import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDevicesPage } from './add-devices';
import { SMS } from '@ionic-native/sms';
// import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';
import { ProgressBarModule } from './progress-bar/progress-bar.module';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
// import { ProgressBarComponent } from './progress-bar/progress-bar';



@NgModule({
  declarations: [
    AddDevicesPage,
    OnCreate,
    // ProgressBarComponent
    
  ],
  imports: [
    IonicPageModule.forChild(AddDevicesPage),
    TranslateModule.forChild(),
    IonBottomDrawerModule,
    ProgressBarModule
  ],
  exports: [
    OnCreate,
    // ProgressBarComponent
  ],
  providers: [
    SMS,
  ]
})
export class AddDevicesPageModule {}
