webpackJsonp([76],{

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(474);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, loadingCtrl, toastCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        this.googleMapsAPIKey = "AIzaSyCmSXnoBBGekErvqBvu5N7dbeqdH1pHp7c";
        // mainUrl: string = "https://www.oneqlik.in/";
        // socketUrl: string = "https://www.oneqlik.in/";
        this.mainUrl = "https://www.oneqlik.in/";
        // usersURL: string = "https://www.oneqlik.in/users/";
        // devicesURL: string = "https://www.oneqlik.in/devices";
        // gpsURL: string = "https://www.oneqlik.in/gps";
        // geofencingURL: string = "https://www.oneqlik.in/geofencing";
        // trackRouteURL: string = "https://www.oneqlik.in/trackRoute";
        // groupURL: string = "https://www.oneqlik.in/groups/";
        // notifsURL: string = "https://www.oneqlik.in/notifs";
        // stoppageURL: string = "https://www.oneqlik.in/stoppage";
        // summaryURL: string = "https://www.oneqlik.in/summary";
        // shareURL: string = "https://www.oneqlik.in/share";
        this.appId = "OneQlikVTS";
        console.log('Hello ApiServiceProvider Provider');
        // debugger
        // if (localStorage.getItem('BASE_URL') != null) {
        //   console.log("check mail urk: ", localStorage.getItem('BASE_URL'));
        //   this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
        //   // this.mainUrl = localStorage.getItem('BASE_URL') + '/';
        //   console.log('main url check =: ', this.mainUrl);
        //   this.usersURL = this.mainUrl + "users/";
        //   this.devicesURL = this.mainUrl + "devices";
        //   this.gpsURL = this.mainUrl + "gps";
        //   this.geofencingURL = this.mainUrl + "geofencing";
        //   this.trackRouteURL = this.mainUrl + "trackRoute";
        //   this.groupURL = this.mainUrl + "groups/";
        //   this.notifsURL = this.mainUrl + "notifs";
        //   this.stoppageURL = this.mainUrl + "stoppage";
        //   this.summaryURL = this.mainUrl + "summary";
        //   this.shareURL = this.mainUrl + "share";
        // }
    }
    ApiServiceProvider.prototype.ionViewDidEnter = function () {
        this.callBaseURL();
        if (localStorage.getItem('BASE_URL') != null) {
            console.log("check mail urk: ", localStorage.getItem('BASE_URL'));
            this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
            // this.mainUrl = localStorage.getItem('BASE_URL') + '/';
            console.log('main url check =: ', this.mainUrl);
            this.usersURL = this.mainUrl + "users/";
            this.devicesURL = this.mainUrl + "devices";
            this.gpsURL = this.mainUrl + "gps";
            this.geofencingURL = this.mainUrl + "geofencing";
            this.trackRouteURL = this.mainUrl + "trackRoute";
            this.groupURL = this.mainUrl + "groups/";
            this.notifsURL = this.mainUrl + "notifs";
            this.stoppageURL = this.mainUrl + "stoppage";
            this.summaryURL = this.mainUrl + "summary";
            this.shareURL = this.mainUrl + "share";
        }
    };
    ApiServiceProvider.prototype.callBaseURL = function () {
        // debugger
        var url = "https://www.oneqlik.in/pullData/getUrlnew";
        this.getSOSReportAPI(url)
            .subscribe(function (data) {
            console.log("base url: ", data);
            if (data.url) {
                localStorage.setItem("BASE_URL", JSON.stringify(data.url));
            }
        });
    };
    ////////////////// LOADING SERVICE /////////////////
    ApiServiceProvider.prototype.toastMsgStarted = function () {
        this.toast = this.toastCtrl.create({
            message: "Loading/Refreshing contents please wait...",
            position: "bottom",
            duration: 2000
        });
        return this.toast.present();
    };
    ApiServiceProvider.prototype.toastMsgDismised = function () {
        return this.toast.dismiss();
    };
    ApiServiceProvider.prototype.startLoadingnew = function (key) {
        var str;
        if (key == 1) {
            str = 'unlocking';
        }
        else {
            str = 'locking';
        }
        return this.loading1 = this.loadingCtrl.create({
            content: "Please wait for some time, as we are " + str + " your vehicle...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoadingnw = function () {
        return this.loading1.dismiss();
    };
    ApiServiceProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ApiServiceProvider.prototype.getCurrency = function () {
        return this.http.get('./assets/json/currency.json')
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCountryCode = function () {
        return this.http.get('./assets/json/countryCode.json')
            .map(function (res) { return res.json(); });
    };
    ////////////////// END LOADING SERVICE /////////////
    ApiServiceProvider.prototype.updatePOIAPI = function (pay) {
        return this.http.post(this.mainUrl + "vehtra/poi/updatePOI", pay, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addPOIAPI = function (payload) {
        return this.http.post(this.mainUrl + "poi/addpoi", payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deletePOIAPI = function (_id) {
        return this.http.get(this.mainUrl + "poi/deletePoi?_id=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.siginupverifyCall = function (usersignup) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + "/signUpZogo", usersignup, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.resendOtp = function (phnum) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + "/sendOtpZRides", phnum)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getpoireportAPI = function (_id, pageNo, fromT, toT, poiId, devid) {
        return this.http.get(this.mainUrl + "poi/poiReport?user=" + _id + "&s=" + pageNo + "&l=9&from=" + fromT + "&to=" + toT + "&poi=" + poiId + "&device=" + devid, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getPoisAPI = function (id) {
        return this.http.get(this.mainUrl + "poi/getPois?user=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateDL = function (updateDL) {
        return this.http.post(this.mainUrl + 'users' + "/zogoUserUpdate", updateDL, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getsingledevice = function (id) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.get(this.devicesURL + "/getDevicebyId?deviceId=" + id, { headers: this.headers })
            // return this.http.get("http://192.168.1.18:3000/devices/getDevicebyId?deviceId=" + id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSOSReportAPI = function (url) {
        // return this.http.get("http://192.168.1.20:3000/notifs/SOSReport?from_date=" + starttime + '&to_date=' + endtime + '&dev_id=' + sos_id + '&_u=' + _id, { headers: this.headers })
        return this.http.get(url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.saveGoogleAddressAPI = function (data) {
        var url = this.mainUrl + "googleAddress/addGoogleAddress";
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.urlpasseswithdata = function (url, data) {
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.DealerSearchService = function (_id, pageno, limit, key) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteDealerCall = function (deletePayload) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + 'deleteUser', deletePayload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDealersCall = function (_id, pageno, limit, searchKey) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        if (searchKey != undefined)
            return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + searchKey, { headers: this.headers })
                .map(function (res) { return res.json(); });
        else
            return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
                .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.callSearchService = function (baseURL) {
        return this.http.get(baseURL, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.callResponse = function (_id) {
        return this.http.get(this.mainUrl + "trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
            // return this.http.get("http://192.168.1.13:3000/trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.serverLevelonoff = function (data) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.post(this.devicesURL + "/addCommandQueue", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePassword = function (data) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + "updatePassword", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateprofile = function (data) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + 'Account_Edit', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeofenceCall = function (_id) {
        if (this.geofencingURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.geofencingURL = this.mainUrl + "geofencing/";
            }
        }
        return this.http.get(this.geofencingURL + '/getgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.get7daysData = function (a, t) {
        if (this.gpsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.gpsURL = this.mainUrl + "gps/";
            }
        }
        return this.http.get(this.gpsURL + '/getDashGraph?imei=' + a + '&t=' + t, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.dataRemoveFuncCall = function (_id, did) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.get(this.devicesURL + '/RemoveShareDevice?did=' + did + '&uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.tripReviewCall = function (device_id, stime, etime) {
        if (this.gpsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.gpsURL = this.mainUrl + "gps/";
            }
        }
        return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + stime + '&to=' + etime, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.sendTokenCall = function (payLoad) {
        if (this.shareURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.shareURL = this.mainUrl + "share/";
            }
        }
        return this.http.post(this.shareURL + "/propagate", payLoad, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.shareLivetrackCall = function (data) {
        if (this.shareURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.shareURL = this.mainUrl + "share/";
            }
        }
        return this.http.post(this.shareURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDriverList = function (_id) {
        return this.http.get(this.mainUrl + "driver/getDrivers?userid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByDateCall = function (_id, skip, limit, dates) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        var from = new Date(dates.fromDate).toISOString();
        var to = new Date(dates.toDate).toISOString();
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByType = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFilteredcall = function (_id, skip, limit, key) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDataOnScroll = function (_id, skip, limit) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        // https://www.oneqlik.in/notifs/getNotifByFilters?from_date=2019-06-13T18:30:00.769Z&to_date=2019-06-14T09:40:16.769Z&user=5cde59324e4d600905f4e690&sortOrder=-1
        // return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getACReportAPI = function (fdate, tdate, userid, imei) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        return this.http.get(this.notifsURL + '/ACSwitchReport?from_date=' + fdate + '&to_date=' + tdate + '&user=' + userid + '&device=' + imei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDetailACReportAPI = function (fdate, tdate, userid, imei) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        return this.http.get(this.notifsURL + "/acReport?from_date=" + fdate + "&to_date=" + tdate + "&_u=" + userid + "&vname=" + imei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleListCall = function (_url) {
        return this.http.get(_url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_detailCall = function (_id, starttime, endtime, did) {
        if (did == undefined) {
            return this.http.get(this.mainUrl + 'user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
        else {
            return this.http.get(this.mainUrl + 'user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + did, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
    };
    ApiServiceProvider.prototype.trackRouteDataCall = function (data) {
        if (this.trackRouteURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.trackRouteURL = this.mainUrl + "trackRoute/";
            }
        }
        return this.http.post(this.trackRouteURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gettrackRouteCall = function (_id, data) {
        if (this.trackRouteURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.trackRouteURL = this.mainUrl + "trackRoute/";
            }
        }
        return this.http.post(this.trackRouteURL + '/' + _id, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteCall = function (_id) {
        if (this.trackRouteURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.trackRouteURL = this.mainUrl + "trackRoute/";
            }
        }
        return this.http.delete(this.trackRouteURL + '/' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRoutesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getStoppageApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        if (this.stoppageURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.stoppageURL = this.mainUrl + "stoppage/";
            }
        }
        return this.http.get(this.stoppageURL + "/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getIgiApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        return this.http.get(this.notifsURL + "/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getOverSpeedApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeogenceReportApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        return this.http.get(this.notifsURL + "/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFuelApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        if (this.notifsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.notifsURL = this.mainUrl + "notifs/";
            }
        }
        return this.http.get(this.notifsURL + "/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceReportApi = function (starttime, endtime, _id, Ignitiondevice_id) {
        if (this.summaryURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.summaryURL = this.mainUrl + "summary/";
            }
        }
        return this.http.get(this.summaryURL + "/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAddressApi = function (data) {
        return this.http.post(this.mainUrl + "gps/getaddress", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport1 = function (url, payload) {
        return this.http.post(url, payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.contactusApi = function (contactdata) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + "contactous", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.createTicketApi = function (contactdata) {
        return this.http.post(this.mainUrl + "customer_support/post_inquiry", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllNotificationCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addgeofenceCall = function (data) {
        if (this.geofencingURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.geofencingURL = this.mainUrl + "geofencing/";
            }
        }
        return this.http.post(this.geofencingURL + '/addgeofence', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getdevicegeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofencestatusCall = function (_id, status, entering, exiting) {
        if (this.geofencingURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.geofencingURL = this.mainUrl + "geofencing/";
            }
        }
        return this.http.get(this.geofencingURL + '/geofencestatus?gid=' + _id + '&status=' + status + '&entering=' + entering + '&exiting=' + exiting, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGeoCall = function (_id) {
        if (this.geofencingURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.geofencingURL = this.mainUrl + "geofencing/";
            }
        }
        return this.http.get(this.geofencingURL + '/deletegeofence?id=' + _id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getallgeofenceCall = function (_id) {
        if (this.geofencingURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.geofencingURL = this.mainUrl + "geofencing/";
            }
        }
        return this.http.get(this.geofencingURL + '/getallgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.user_statusCall = function (data) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + 'user_status', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.editUserDetailsCall = function (devicedetails) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + 'editUserDetails', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerVehiclesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addGroupCall = function (devicedetails) {
        if (this.groupURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.groupURL = this.mainUrl + "groups/";
            }
        }
        return this.http.post(this.groupURL + 'addGroup', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleTypesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllUsersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDeviceModelCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.groupsCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addDeviceCall = function (devicedetails) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.post(this.devicesURL + '/addDevice', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAddress = function (cord) {
        return this.http.post(this.mainUrl + "googleAddress/getGoogleAddress", cord, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCustomersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofenceCall = function (_id) {
        if (this.geofencingURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.geofencingURL = this.mainUrl + "geofencing/";
            }
        }
        return this.http.get(this.geofencingURL + "/getgeofence?uid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassApi = function (mobno) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.get(this.usersURL + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassMobApi = function (Passwordset) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.get(this.usersURL + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.loginApi = function (userdata) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + "LoginWithOtp", userdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.signupApi = function (usersignupdata) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + "signUp", usersignupdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // dashboardcall(email, from, to, _id) {
    //   return this.http.get(this.gpsURL + '/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.dashboardcall = function (_baseUrl) {
        return this.http.get(_baseUrl, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppedDevices = function (_id, email, off_ids) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.livedatacall = function (_id, email) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.get(this.devicesURL + "/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesApi = function (_id, email) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesForAllVehiclesApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .timeout(500000000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.ignitionoffCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceupdateCall = function (devicedetail) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices";
            }
        }
        return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
            // return this.http.post("http://192.168.1.18:3000/devices" + "/deviceupdate", devicedetail, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceSpeedCall = function (device_id, from, to) {
        if (this.gpsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.gpsURL = this.mainUrl + "gps";
            }
        }
        return this.http.get(this.gpsURL + '/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppage_detail = function (_id, from, to, device_id) {
        if (this.stoppageURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.stoppageURL = this.mainUrl + "stoppage/";
            }
        }
        return this.http.get(this.stoppageURL + '/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gpsCall = function (device_id, from, to) {
        if (this.gpsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.gpsURL = this.mainUrl + "gps";
            }
        }
        // return this.http.get(this.gpsURL + '/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
        return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getcustToken = function (id) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.get(this.usersURL + "getCustumerDetail?uid=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSummaryReportApi = function (starttime, endtime, _id, device_id) {
        if (this.summaryURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.summaryURL = this.mainUrl + "summary/";
            }
        }
        return this.http.get(this.summaryURL + "/summaryReport?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDaywiseReportApi = function (starttime, endtime, _id, device_id) {
        if (this.summaryURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.summaryURL = this.mainUrl + "summary/";
            }
        }
        return this.http.get(this.summaryURL + "/getDayWiseReport?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getallrouteCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSpeedReport = function (_id, time) {
        if (this.gpsURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.gpsURL = this.mainUrl + "gps/";
            }
        }
        return this.http.get(this.gpsURL + '/getGpsSpeedReport?imei=' + _id + '&time=' + time, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // deviceupdateInCall(devicedetail) {
    //   return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.deleteDeviceCall = function (d_id) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.get(this.devicesURL + "/deleteDevice?did=" + d_id, { headers: this.headers })
            // .map(res => res.json());
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.deviceShareCall = function (data) {
        if (this.devicesURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.devicesURL = this.mainUrl + "devices/";
            }
        }
        return this.http.get(this.devicesURL + "/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pushnotifyCall = function (pushdata) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + "PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pullnotifyCall = function (pushdata) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + "PullNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGroupCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGroupCall = function (d_id) {
        if (this.groupURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.groupURL = this.mainUrl + "groups/";
            }
        }
        return this.http.get(this.groupURL + "deleteGroup?_id=" + d_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteCustomerCall = function (data) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.post(this.usersURL + 'deleteUser', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.route_details = function (_id, user_id) {
        if (this.trackRouteURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.trackRouteURL = this.mainUrl + "trackRoute/";
            }
        }
        return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + _id + '&user=' + user_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.callcustomerSearchService = function (uid, pageno, limit, seachKey) {
        if (this.usersURL == undefined) {
            this.callBaseURL();
            if (localStorage.getItem('BASE_URL') != null) {
                this.mainUrl = JSON.parse(localStorage.getItem('BASE_URL')) + '/';
                this.usersURL = this.mainUrl + "users/";
            }
        }
        return this.http.get(this.usersURL + 'getCust?uid=' + uid + '&pageNo=' + pageno + '&size=' + limit + '&search=' + seachKey)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["ToastController"]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ }),

/***/ 154:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 154;

/***/ }),

/***/ 195:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/ac-report/ac-report.module": [
		577,
		75
	],
	"../pages/add-devices/add-devices.module": [
		578,
		16
	],
	"../pages/add-devices/immobilize/immobilize.module": [
		579,
		67
	],
	"../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module": [
		580,
		66
	],
	"../pages/add-devices/update-device/update-device.module": [
		581,
		65
	],
	"../pages/add-devices/upload-doc/add-doc/add-doc.module": [
		582,
		64
	],
	"../pages/add-devices/upload-doc/upload-doc.module": [
		583,
		74
	],
	"../pages/add-devices/vehicle-details/vehicle-details.module": [
		584,
		23
	],
	"../pages/all-notifications/all-notifications.module": [
		585,
		63
	],
	"../pages/all-notifications/notif-map/notif-map.module": [
		586,
		62
	],
	"../pages/chat/chat.module": [
		587,
		22
	],
	"../pages/contact-us/contact-us.module": [
		588,
		61
	],
	"../pages/create-trip/create-trip.module": [
		589,
		60
	],
	"../pages/customers/customers.module": [
		590,
		59
	],
	"../pages/customers/modals/add-customer-modal/add-customer-modal.module": [
		591,
		58
	],
	"../pages/customers/modals/add-device-modal.module": [
		592,
		57
	],
	"../pages/customers/modals/group-modal/group-modal.module": [
		593,
		56
	],
	"../pages/customers/modals/update-cust/update-cust.module": [
		594,
		55
	],
	"../pages/daily-report-new/daily-report-new.module": [
		595,
		54
	],
	"../pages/daily-report/daily-report.module": [
		596,
		0
	],
	"../pages/dashboard/dashboard.module": [
		599,
		12
	],
	"../pages/daywise-report/daywise-report.module": [
		597,
		11
	],
	"../pages/dealers/add-dealer/add-dealer.module": [
		598,
		53
	],
	"../pages/dealers/dealers.module": [
		600,
		52
	],
	"../pages/dealers/edit-dealer/edit-dealer.module": [
		603,
		51
	],
	"../pages/device-summary-repo/device-summary-repo.module": [
		601,
		7
	],
	"../pages/distance-report/distance-report.module": [
		602,
		6
	],
	"../pages/expenses/add-expense/add-expense.module": [
		604,
		50
	],
	"../pages/expenses/expense-type-detail/expense-type-detail.module": [
		605,
		49
	],
	"../pages/expenses/expenses.module": [
		606,
		48
	],
	"../pages/fastag-list/fastag-list.module": [
		607,
		47
	],
	"../pages/fastag-list/fastag/fastag.module": [
		608,
		73
	],
	"../pages/feedback/feedback.module": [
		609,
		13
	],
	"../pages/fuel/fuel-chart/fuel-chart.module": [
		610,
		21
	],
	"../pages/fuel/fuel-consumption-report/fuel-consumption-report.module": [
		611,
		20
	],
	"../pages/fuel/fuel-consumption/fuel-consumption.module": [
		612,
		72
	],
	"../pages/fuel/fuel-events/fuel-events.module": [
		613,
		15
	],
	"../pages/geofence-report/geofence-report.module": [
		614,
		10
	],
	"../pages/geofence/add-geofence/add-geofence.module": [
		615,
		46
	],
	"../pages/geofence/geofence-show/geofence-show.module": [
		616,
		45
	],
	"../pages/geofence/geofence.module": [
		617,
		44
	],
	"../pages/groups/groups.module": [
		618,
		43
	],
	"../pages/history-device/history-device.module": [
		622,
		42
	],
	"../pages/idle-report/idle-report.module": [
		619,
		41
	],
	"../pages/ignition-report/ignition-report.module": [
		620,
		5
	],
	"../pages/live-single-device/device-settings/device-settings.module": [
		621,
		40
	],
	"../pages/live-single-device/live-single-device.module": [
		624,
		71
	],
	"../pages/live/expired/expired.module": [
		623,
		39
	],
	"../pages/live/live.module": [
		626,
		38
	],
	"../pages/loading-unloading-trip/loading-unloading-trip.module": [
		625,
		9
	],
	"../pages/login/login.module": [
		627,
		70
	],
	"../pages/maintenance-reminder/add-reminder/add-reminder.module": [
		628,
		37
	],
	"../pages/maintenance-reminder/maintenance-reminder.module": [
		629,
		36
	],
	"../pages/my-bookings/booking-detail/booking-detail.module": [
		630,
		14
	],
	"../pages/my-bookings/my-bookings.module": [
		631,
		69
	],
	"../pages/over-speed-repo/over-speed-repo.module": [
		632,
		4
	],
	"../pages/poi-list/add-poi/add-poi.module": [
		633,
		35
	],
	"../pages/poi-list/poi-list.module": [
		634,
		19
	],
	"../pages/poi-report/poi-report.module": [
		635,
		34
	],
	"../pages/profile/profile.module": [
		637,
		68
	],
	"../pages/profile/settings/notif-setting/notif-setting.module": [
		636,
		33
	],
	"../pages/profile/settings/settings.module": [
		638,
		32
	],
	"../pages/route-map-show/route-map-show.module": [
		639,
		31
	],
	"../pages/route-voilations/route-voilations.module": [
		640,
		30
	],
	"../pages/route/route.module": [
		641,
		29
	],
	"../pages/signup/signup-otp/signup-otp.module": [
		642,
		28
	],
	"../pages/signup/signup.module": [
		643,
		27
	],
	"../pages/siren-alert/siren-alert.module": [
		644,
		26
	],
	"../pages/sos-report/sos-report.module": [
		645,
		8
	],
	"../pages/speed-repo/speed-repo.module": [
		646,
		18
	],
	"../pages/stoppages-repo/stoppages-repo.module": [
		647,
		3
	],
	"../pages/supported-devices/supported-devices.module": [
		648,
		25
	],
	"../pages/trip-report/trip-report.module": [
		649,
		2
	],
	"../pages/trip-report/trip-review/trip-review.module": [
		650,
		24
	],
	"../pages/working-hours-report/working-hours-report.module": [
		651,
		1
	],
	"../pages/your-trips/your-trips.module": [
		652,
		17
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 195;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImmobilizeModelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_sms__ = __webpack_require__(138);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ImmobilizeModelPage = /** @class */ (function () {
    function ImmobilizeModelPage(navParams, viewCtrl, speechRecognition, translate, toastCtrl, apiCall, alertCtrl, events, sms, plt, cd) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.speechRecognition = speechRecognition;
        this.translate = translate;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.sms = sms;
        this.plt = plt;
        this.cd = cd;
        this.min_time = "10";
        this.timeoutSeconds = 60000;
        this.isRecording = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        if (navParams.get("param") != null) {
            this.obj = this.navParams.get("param");
        }
        if (localStorage.getItem("AlreadyDimissed") !== null) {
            localStorage.removeItem("AlreadyDimissed");
        }
    }
    ImmobilizeModelPage.prototype.isIos = function () {
        return this.plt.is('ios');
    };
    ImmobilizeModelPage.prototype.stopListening = function () {
        var _this = this;
        this.speechRecognition.stopListening().then(function () {
            _this.isRecording = false;
        });
    };
    ImmobilizeModelPage.prototype.getPermission = function () {
        var _this = this;
        this.speechRecognition.hasPermission()
            .then(function (hasPermission) {
            if (!hasPermission) {
                _this.speechRecognition.requestPermission();
            }
        });
    };
    ImmobilizeModelPage.prototype.startListening = function () {
        var _this = this;
        this.getPermission();
        var options = {
            language: 'en-US'
        };
        this.speechRecognition.startListening(options).subscribe(function (matches) {
            _this.matches = matches;
            _this.cd.detectChanges();
            console.log("matech length: ", _this.matches.length);
            console.log("match found: ", _this.matches[0]);
            for (var i = 0; i < _this.matches.length; i++) {
                if (_this.matches[i] === 'Lock engine' || _this.matches[i] === 'Unlock Engine' || _this.matches[i] === 'lock engine' || _this.matches[i] === 'unlock engine') {
                    _this.IgnitionOnOff(_this.obj);
                }
            }
        });
        this.isRecording = true;
    };
    ImmobilizeModelPage.prototype.dismiss = function () {
        if (localStorage.getItem("AlreadyDimissed") !== null) {
            this.events.publish("Released:Dismiss");
            localStorage.removeItem("AlreadyDimissed");
        }
        console.log("Inside the function");
        this.viewCtrl.dismiss();
        localStorage.setItem("AlreadyDimissed", "true");
    };
    ImmobilizeModelPage.prototype.voiceToText = function () {
        var _this = this;
        // debugger
        // Check feature available
        this.speechRecognition.isRecognitionAvailable()
            .then(function (available) { return console.log(available); });
        // Check permission
        this.speechRecognition.hasPermission()
            .then(function (hasPermission) {
            console.log(hasPermission);
            if (!hasPermission) {
                // Request permissions
                _this.speechRecognition.requestPermission()
                    .then(function () {
                    console.log('Granted');
                    _this.startListning();
                }, function () { return console.log('Denied'); });
            }
            else {
                var options = {
                    // Android only
                    showPartial: true
                };
                _this.startListning();
            }
        });
    };
    ImmobilizeModelPage.prototype.startListning = function () {
        var _this = this;
        // Start the recognition process
        this.speechRecognition.startListening()
            .subscribe(function (matches) {
            console.log(matches);
            for (var i = 0; i < matches.length; i++) {
                if (matches[i] === 'Lock Engine' || matches[i] === 'Unlock Engine' || matches[i] === 'lock engine' || matches[i] === 'unlock engine') {
                    _this.IgnitionOnOff(_this.obj);
                }
            }
        }, function (onerror) { return console.log('error:', onerror); });
    };
    ImmobilizeModelPage.prototype.unlockEngine = function () {
        var _this = this;
        // debugger
        var that = this;
        if (this.obj.last_ACC != null || this.obj.last_ACC != undefined) {
            if (localStorage.getItem('AlreadyClicked') !== null) {
                var toast = this.toastCtrl.create({
                    message: this.translate.instant('Process ongoing..'),
                    duration: 1800,
                    position: 'middle'
                });
                toast.present();
            }
            else {
                this.checkImmobilizePassword();
                this.messages = undefined;
                this.dataEngine = this.obj;
                var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
                this.apiCall.startLoading().present();
                this.apiCall.ignitionoffCall(baseURLp)
                    .subscribe(function (data) {
                    _this.apiCall.stopLoading();
                    _this.DeviceConfigStatus = data;
                    _this.immobType = data[0].imobliser_type;
                    var key;
                    console.log('ignition lock key: ', _this.dataEngine.ignitionLock);
                    if (_this.dataEngine.ignitionLock == '1') {
                        _this.messages = _this.translate.instant('Do you want to unlock the engine?');
                        key = 1;
                    }
                    else {
                        if (_this.dataEngine.ignitionLock == '0') {
                            // this.messages = this.translate.instant('Do you want to lock the engine?')
                            _this.messages = _this.translate.instant('The engine is already unlocked...!');
                            key = 0;
                        }
                    }
                    if (key === 1) {
                        var alert_1 = _this.alertCtrl.create({
                            message: _this.messages,
                            buttons: [{
                                    text: 'YES',
                                    handler: function () {
                                        debugger;
                                        if (_this.immobType == 0 || _this.immobType == undefined) {
                                            // that.clicked = true;
                                            localStorage.setItem('AlreadyClicked', 'true');
                                            var devicedetail = {
                                                "_id": _this.dataEngine._id,
                                                "engine_status": !_this.dataEngine.engine_status
                                            };
                                            // this.apiCall.startLoading().present();
                                            _this.apiCall.deviceupdateCall(devicedetail)
                                                .subscribe(function (response) {
                                                // this.apiCall.stopLoading();
                                                // this.editdata = response;
                                                var toast = _this.toastCtrl.create({
                                                    message: response.message,
                                                    duration: 2000,
                                                    position: 'top'
                                                });
                                                toast.present();
                                                // this.responseMessage = "Edit successfully";
                                                // this.getdevices();
                                                var msg;
                                                if (!_this.dataEngine.engine_status) {
                                                    msg = _this.DeviceConfigStatus[0].resume_command;
                                                }
                                                else {
                                                    msg = _this.DeviceConfigStatus[0].immoblizer_command;
                                                }
                                                _this.sms.send(_this.obj.sim_number, msg);
                                                var toast1 = _this.toastCtrl.create({
                                                    message: _this.translate.instant('SMS sent successfully'),
                                                    duration: 2000,
                                                    position: 'bottom'
                                                });
                                                toast1.present();
                                                // that.clicked = false;
                                                localStorage.removeItem("AlreadyClicked");
                                            }, function (error) {
                                                // that.clicked = false;
                                                localStorage.removeItem("AlreadyClicked");
                                                // this.apiCall.stopLoading();
                                                console.log(error);
                                            });
                                        }
                                        else {
                                            console.log("Call server code here!!");
                                            if (that.checkedPass === 'PASSWORD_SET') {
                                                _this.askForPassword(_this.obj);
                                                return;
                                            }
                                            that.serverLevelOnOff(_this.obj);
                                        }
                                    }
                                },
                                {
                                    text: _this.translate.instant('NO')
                                }]
                        });
                        alert_1.present();
                    }
                    else {
                        var alert_2 = _this.alertCtrl.create({
                            message: _this.messages,
                            buttons: ['Okay']
                        });
                        alert_2.present();
                    }
                }, function (error) {
                    _this.apiCall.stopLoading();
                    console.log("some error: ", error._body.message);
                });
            }
        }
    };
    ImmobilizeModelPage.prototype.lockEngine = function () {
        var _this = this;
        // debugger
        var that = this;
        if (this.obj.last_ACC != null || this.obj.last_ACC != undefined) {
            if (localStorage.getItem('AlreadyClicked') !== null) {
                var toast = this.toastCtrl.create({
                    message: this.translate.instant('Process ongoing..'),
                    duration: 1800,
                    position: 'middle'
                });
                toast.present();
            }
            else {
                this.checkImmobilizePassword();
                this.messages = undefined;
                this.dataEngine = this.obj;
                var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
                this.apiCall.startLoading().present();
                this.apiCall.ignitionoffCall(baseURLp)
                    .subscribe(function (data) {
                    _this.apiCall.stopLoading();
                    _this.DeviceConfigStatus = data;
                    _this.immobType = data[0].imobliser_type;
                    var key;
                    console.log('ignition lock key: ', _this.dataEngine.ignitionLock);
                    if (_this.dataEngine.ignitionLock == '1') {
                        // this.messages = this.translate.instant('Do you want to unlock the engine?')
                        _this.messages = _this.translate.instant('The engine is already locked...!');
                        key = 0;
                    }
                    else {
                        if (_this.dataEngine.ignitionLock == '0') {
                            _this.messages = _this.translate.instant('Do you want to lock the engine?');
                            // this.messages = this.translate.instant('The engine is already unlocked...!')
                            key = 1;
                        }
                    }
                    if (key === 1) {
                        var alert_3 = _this.alertCtrl.create({
                            message: _this.messages,
                            buttons: [{
                                    text: 'YES',
                                    handler: function () {
                                        if (_this.immobType == 0 || _this.immobType == undefined) {
                                            // that.clicked = true;
                                            localStorage.setItem('AlreadyClicked', 'true');
                                            var devicedetail = {
                                                "_id": _this.dataEngine._id,
                                                "engine_status": !_this.dataEngine.engine_status
                                            };
                                            // this.apiCall.startLoading().present();
                                            _this.apiCall.deviceupdateCall(devicedetail)
                                                .subscribe(function (response) {
                                                // this.apiCall.stopLoading();
                                                // this.editdata = response;
                                                var toast = _this.toastCtrl.create({
                                                    message: response.message,
                                                    duration: 2000,
                                                    position: 'top'
                                                });
                                                toast.present();
                                                // this.responseMessage = "Edit successfully";
                                                // this.getdevices();
                                                var msg;
                                                if (!_this.dataEngine.engine_status) {
                                                    msg = _this.DeviceConfigStatus[0].resume_command;
                                                }
                                                else {
                                                    msg = _this.DeviceConfigStatus[0].immoblizer_command;
                                                }
                                                _this.sms.send(_this.obj.sim_number, msg);
                                                var toast1 = _this.toastCtrl.create({
                                                    message: _this.translate.instant('SMS sent successfully'),
                                                    duration: 2000,
                                                    position: 'bottom'
                                                });
                                                toast1.present();
                                                // that.clicked = false;
                                                localStorage.removeItem("AlreadyClicked");
                                            }, function (error) {
                                                // that.clicked = false;
                                                localStorage.removeItem("AlreadyClicked");
                                                // this.apiCall.stopLoading();
                                                console.log(error);
                                            });
                                        }
                                        else {
                                            console.log("Call server code here!!");
                                            if (that.checkedPass === 'PASSWORD_SET') {
                                                _this.askForPassword(_this.obj);
                                                return;
                                            }
                                            that.serverLevelOnOff(_this.obj);
                                        }
                                    }
                                },
                                {
                                    text: _this.translate.instant('NO')
                                }]
                        });
                        alert_3.present();
                    }
                    else {
                        var alert_4 = _this.alertCtrl.create({
                            message: _this.messages,
                            buttons: ['Okay']
                        });
                        alert_4.present();
                    }
                }, function (error) {
                    _this.apiCall.stopLoading();
                    console.log("some error: ", error._body.message);
                });
            }
        }
    };
    ImmobilizeModelPage.prototype.IgnitionOnOff = function (d) {
        var _this = this;
        var that = this;
        if (d.last_ACC != null || d.last_ACC != undefined) {
            if (localStorage.getItem('AlreadyClicked') !== null) {
                var toast = this.toastCtrl.create({
                    message: this.translate.instant('Process ongoing..'),
                    duration: 1800,
                    position: 'middle'
                });
                toast.present();
            }
            else {
                this.checkImmobilizePassword();
                this.messages = undefined;
                this.dataEngine = d;
                var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
                this.apiCall.startLoading().present();
                this.apiCall.ignitionoffCall(baseURLp)
                    .subscribe(function (data) {
                    _this.apiCall.stopLoading();
                    _this.DeviceConfigStatus = data;
                    _this.immobType = data[0].imobliser_type;
                    if (_this.dataEngine.ignitionLock == '1') {
                        _this.messages = _this.translate.instant('Do you want to unlock the engine?');
                    }
                    else {
                        if (_this.dataEngine.ignitionLock == '0') {
                            _this.messages = _this.translate.instant('Do you want to lock the engine?');
                        }
                    }
                    var alert = _this.alertCtrl.create({
                        message: _this.messages,
                        buttons: [{
                                text: 'YES',
                                handler: function () {
                                    if (_this.immobType == 0 || _this.immobType == undefined) {
                                        // that.clicked = true;
                                        localStorage.setItem('AlreadyClicked', 'true');
                                        var devicedetail = {
                                            "_id": _this.dataEngine._id,
                                            "engine_status": !_this.dataEngine.engine_status
                                        };
                                        _this.apiCall.deviceupdateCall(devicedetail)
                                            .subscribe(function (response) {
                                            var toast = _this.toastCtrl.create({
                                                message: response.message,
                                                duration: 2000,
                                                position: 'top'
                                            });
                                            toast.present();
                                            var msg;
                                            if (!_this.dataEngine.engine_status) {
                                                msg = _this.DeviceConfigStatus[0].resume_command;
                                            }
                                            else {
                                                msg = _this.DeviceConfigStatus[0].immoblizer_command;
                                            }
                                            _this.sms.send(d.sim_number, msg);
                                            var toast1 = _this.toastCtrl.create({
                                                message: _this.translate.instant('SMS sent successfully'),
                                                duration: 2000,
                                                position: 'bottom'
                                            });
                                            toast1.present();
                                            // that.clicked = false;
                                            localStorage.removeItem("AlreadyClicked");
                                        }, function (error) {
                                            // that.clicked = false;
                                            localStorage.removeItem("AlreadyClicked");
                                            // this.apiCall.stopLoading();
                                            console.log(error);
                                        });
                                    }
                                    else {
                                        console.log("Call server code here!!");
                                        if (that.checkedPass === 'PASSWORD_SET') {
                                            _this.askForPassword(d);
                                            return;
                                        }
                                        that.serverLevelOnOff(d);
                                    }
                                }
                            },
                            {
                                text: _this.translate.instant('NO')
                            }]
                    });
                    alert.present();
                }, function (error) {
                    _this.apiCall.stopLoading();
                    console.log("some error: ", error._body.message);
                });
            }
        }
    };
    ;
    ImmobilizeModelPage.prototype.checkImmobilizePassword = function () {
        var _this = this;
        var rurl = this.apiCall.mainUrl + 'users/get_user_setting';
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(rurl, Var)
            .subscribe(function (data) {
            if (!data.engine_cut_psd) {
                _this.checkedPass = 'PASSWORD_NOT_SET';
            }
            else {
                _this.checkedPass = 'PASSWORD_SET';
            }
        });
    };
    ImmobilizeModelPage.prototype.askForPassword = function (d) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Enter Password',
            message: "Enter password for engine cut",
            inputs: [
                {
                    name: 'password',
                    placeholder: 'Password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Proceed',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("data: ", data);
                        _this.verifyPassword(data, d);
                    }
                }
            ]
        });
        prompt.present();
    };
    ImmobilizeModelPage.prototype.verifyPassword = function (pass, d) {
        var _this = this;
        var ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
        var payLd = {
            "uid": this.islogin._id,
            "psd": pass.password
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(ryurl, payLd)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log(resp);
            if (resp.message === 'password not matched') {
                _this.toastmsg(resp.message);
                return;
            }
            _this.serverLevelOnOff(d);
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    ImmobilizeModelPage.prototype.toastmsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        }).present();
    };
    ImmobilizeModelPage.prototype.serverLevelOnOff = function (d) {
        var _this = this;
        // let that = this;
        // that.clicked = true;
        localStorage.setItem("AlreadyClicked", "true");
        var data = {
            "imei": d.Device_ID,
            "_id": this.dataEngine._id,
            "engine_status": d.ignitionLock,
            "protocol_type": d.device_model.device_type
        };
        // this.apiCall.startLoading().present();
        this.apiCall.serverLevelonoff(data)
            .subscribe(function (resp) {
            console.log("ignition on off=> ", resp);
            _this.respMsg = resp;
            _this.intervalID = setInterval(function () {
                _this.apiCall.callResponse(_this.respMsg._id)
                    .subscribe(function (data) {
                    console.log("interval=> " + data);
                    _this.commandStatus = data.status;
                    if (_this.commandStatus == 'SUCCESS') {
                        clearTimeout(_this.timeoutId);
                        clearInterval(_this.intervalID);
                        // that.clicked = false;
                        localStorage.removeItem("AlreadyClicked");
                        // this.apiCall.stopLoadingnw();
                        var toast1 = _this.toastCtrl.create({
                            message: _this.translate.instant('process has been completed successfully!'),
                            duration: 2000,
                            position: 'middle'
                        });
                        toast1.present();
                        _this.dismiss();
                    }
                });
            }, 5000);
            var that = _this;
            that.timeoutId = setTimeout(function () {
                debugger;
                // if (that.commandStatus !== 'SUCCESS') {
                clearTimeout(that.timeoutId);
                clearInterval(that.intervalID);
                console.log("after removinf interval: ", that.intervalID);
                localStorage.removeItem("AlreadyClicked");
                // }
                // if (that.timeoutId) {
                // clearTimeout(that.timeoutId);
                console.log("after removinf timeout: ", that.timeoutId);
                // }
            }, that.timeoutSeconds);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error in onoff=>", err);
            localStorage.removeItem("AlreadyClicked");
            // that.clicked = false;
        });
    };
    ImmobilizeModelPage.prototype.onInfo = function () {
        var toast = this.toastCtrl.create({
            message: 'Now you can use voice control to use immobilize. Say "Lock Engine" to lock vehicle engine and say "Unlock Engine" to unlock vehicle engine.',
            showCloseButton: true,
            closeButtonText: 'Dismiss'
        });
        toast.present();
    };
    ImmobilizeModelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-immobilize',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/add-devices/immobilize-modal.html"*/'<ion-icon name="close" style="font-size: 3em; color: white;float: right; padding-right: 11px;" (click)="dismiss()">\n</ion-icon>\n\n<!-- <button ion-button full (click)="getPermission()">Get Permission</button>\n<button ion-button full (click)="startListening()">Start Listening</button>\n<button ion-button full (click)="stopListening()" *ngIf="isIos()">Stop Listening</button>\n\n -->\n\n<div class="mainDiv">\n    <!-- <ion-buttons end> -->\n    <!-- <button ion-button icon-only (click)="dismiss()">\n            <ion-icon name="close-circle"></ion-icon>\n        </button> -->\n\n    <!-- </ion-buttons> -->\n\n    <div class="secondDiv">\n        <ion-grid>\n            <ion-row no-padding>\n                <ion-col width-50>\n                    <div class="design">\n                        <span class="cont">\n                            <p>ARM</p>\n                        </span>\n                        <div style="height: 60px; width: 70px; margin: auto; text-align: center;">\n                            <img src="assets/imgs/lock.png" />\n                        </div>\n                    </div>\n                </ion-col>\n                <ion-col width-50>\n                    <div class="design">\n                        <span class="cont">\n                            <p>DISARM</p>\n                        </span>\n                        <div style="height: 60px; width: 70px; margin: auto; text-align: center;">\n                            <img src="assets/imgs/unlock.png" />\n                        </div>\n                    </div>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col width-50 (click)="unlockEngine()">\n                    <div class="design">\n                        <div style="height: 60px; width: 70px; margin: auto; text-align: center; padding-top: 38px">\n                            <img src="assets/imgs/play.png" />\n                        </div>\n                        <span class="cont">\n                            <p>RESUME CAR</p>\n                        </span>\n                    </div>\n                </ion-col>\n                <ion-col width-50 (click)="lockEngine()">\n                    <div class="design">\n                        <div style="height: 60px; width: 70px; margin: auto; text-align: center; padding-top: 38px">\n                            <img src="assets/imgs/stop.png" />\n                        </div>\n                        <span class="cont">\n                            <p>STOP CAR</p>\n                        </span>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n        <div class="final" (click)="voiceToText()" *ngIf="!isIos()">\n            <img src="assets/imgs/support.png" class="manual" />\n            <p>LISTEN</p>\n        </div>\n        <div class="final" (click)="startListening()" *ngIf="isIos()">\n            <img src="assets/imgs/support.png" class="manual" />\n            <p>LISTEN</p>\n        </div>\n        <div style="padding: 3px 11px;">\n            <button ion-button block color="light" (click)="stopListening()" *ngIf="isRecording">Stop Listening</button>\n\n            <button ion-button (click)="dismiss()" color="gpsc" block>\n                Submit\n            </button>\n\n        </div>\n    </div>\n</div>\n<ion-card>\n    <ion-card-header>This is what I understood...</ion-card-header>\n    <ion-card-content>\n        <ion-list>\n            <ion-item *ngFor="let match of matches">\n                {{ match }}\n            </ion-item>\n        </ion-list>\n    </ion-card-content>\n</ion-card>\n<ion-fab bottom right>\n    <button ion-fab (click)="onInfo()" color="gpsc">\n        <!-- <ion-icon name="information-outline"></ion-icon> -->\n        <img class="imgSty" src="assets/imgs/info.png" />\n    </button>\n</ion-fab>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/add-devices/immobilize-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__["a" /* SpeechRecognition */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], ImmobilizeModelPage);
    return ImmobilizeModelPage;
}());

//# sourceMappingURL=immobilize-modal.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimePickerModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { DatePicker } from '@ionic-native/date-picker';


// import * as moment from 'moment';
var TimePickerModal = /** @class */ (function () {
    function TimePickerModal(navParams, apiCall, viewCtrl, alertCtrl, translate, toastCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.toastCtrl = toastCtrl;
        this.start_ampm = "AM";
        this.end_ampm = "AM";
        this.schedule = false;
        this.showScheduler = false;
        this.showUpdateBtn = false;
        this.showSubmitBtn = true;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.data = navParams.get('data');
        this.screenKey = navParams.get('key');
        debugger;
        var startHr, startMin, endHr, endMin;
        if (this.screenKey === 'tow') {
            if (this.data.towAlert || this.data.towTime) {
                this.schedule = this.data.towAlert;
                this.showScheduler = true;
                this.showUpdateBtn = true;
                this.showSubmitBtn = false;
            }
            if (this.data.towTime !== undefined) {
                // this.schedule = "true";
                startHr = new Date(this.data.towTime.start).getHours();
                startMin = new Date(this.data.towTime.start).getMinutes();
                this.startHourNumber = this.pad(startHr, 2);
                this.startMinutesNumber = this.pad(startMin, 2);
                endHr = new Date(this.data.towTime.end).getHours();
                endMin = new Date(this.data.towTime.end).getMinutes();
                this.endHourNumber = this.pad(endHr, 2);
                this.endMinutesNumber = this.pad(endMin, 2);
            }
            else {
                this.startHourNumber = this.pad(1, 2);
                this.startMinutesNumber = this.pad(0, 2);
                this.endHourNumber = this.pad(1, 2);
                this.endMinutesNumber = this.pad(0, 2);
            }
        }
        else if (this.screenKey === 'parking') {
            if (this.data.theftAlert || this.data.theftTime) {
                this.schedule = this.data.theftAlert;
                this.showScheduler = true;
                this.showUpdateBtn = true;
                this.showSubmitBtn = false;
            }
            if (this.data.theftTime) {
            }
            if (this.data.theftTime !== undefined) {
                // this.schedule = "true";
                startHr = new Date(this.data.theftTime.start).getHours();
                startMin = new Date(this.data.theftTime.start).getMinutes();
                this.startHourNumber = this.pad(startHr, 2);
                this.startMinutesNumber = this.pad(startMin, 2);
                endHr = new Date(this.data.theftTime.end).getHours();
                endMin = new Date(this.data.theftTime.end).getMinutes();
                this.endHourNumber = this.pad(endHr, 2);
                this.endMinutesNumber = this.pad(endMin, 2);
            }
            else {
                this.startHourNumber = this.pad(1, 2);
                this.startMinutesNumber = this.pad(0, 2);
                this.endHourNumber = this.pad(1, 2);
                this.endMinutesNumber = this.pad(0, 2);
            }
        }
        console.log("data parameters: ", this.data);
    }
    TimePickerModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    TimePickerModal.prototype.pad = function (number, length) {
        var str = '' + number;
        while (str.length < length) {
            str = '0' + str;
        }
        return str;
    };
    TimePickerModal.prototype.setScheduler = function (sch) {
        // console.log('schedluler: ', sch)
        if (this.screenKey === 'tow') {
            this.towAlertCall(sch);
        }
        else if (this.screenKey === 'parking') {
            this.fonctionTest(sch);
        }
    };
    TimePickerModal.prototype.fonctionTest = function (d) {
        var _this = this;
        var theft;
        var that = this;
        theft = !(that.data.theftAlert);
        if (theft) {
            var alert_1 = this.alertCtrl.create({
                title: this.translate.instant('Confirm'),
                message: this.translate.instant('Are you sure you want to activate parking alarm? On activating this alert you will get receive notification if vehicle moves.'),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: function () {
                            var payload = {
                                "_id": that.data._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                var toast = _this.toastCtrl.create({
                                    message: _this.translate.instant('Parking alarm Activated!'),
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                // this.getdevices();
                            }, function (err) {
                                _this.apiCall.stopLoading();
                            });
                        }
                    },
                    {
                        text: 'BACK',
                        handler: function () {
                            that.schedule = false;
                            // that.showScheduler = false;
                            // that.data.theftAlert = !(that.data.theftAlert);
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: this.translate.instant('Confirm'),
                message: this.translate.instant('Are you sure you want to deactivate parking alarm?'),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: function () {
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: _this.translate.instant('Parking alarm Deactivated!'),
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                // this.getdevices();
                            });
                        }
                    },
                    {
                        text: 'BACK',
                        handler: function () {
                            that.schedule = that.data.theftAlert;
                            // that.data.theftAlert = !(that.data.theftAlert);
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    TimePickerModal.prototype.towAlertCall = function (d) {
        var _this = this;
        var tow;
        var that = this;
        tow = !(that.data.towAlert);
        if (tow) {
            var alert_3 = this.alertCtrl.create({
                title: "Confirm",
                message: "Are you sure you want to activate Tow Alert alarm? On activating this alert you will get receive notification if vehicle has been towed.",
                buttons: [
                    {
                        text: 'YES PROCEED',
                        handler: function () {
                            // theft = !(d.theftAlert);
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "towAlert": tow
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: "Tow Alert alarm Activated!",
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                // this.callObjFunc(d);
                                // this.getdevices();
                            }, function (err) {
                                _this.apiCall.stopLoading();
                            });
                        }
                    },
                    {
                        text: 'BACK',
                        handler: function () {
                            debugger;
                            that.schedule = false;
                            // that.showScheduler = false;
                            // d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            });
            alert_3.present();
        }
        else {
            var alert_4 = this.alertCtrl.create({
                title: "Confirm",
                message: "Are you sure you want to deactivate Tow Alert alarm?",
                buttons: [
                    {
                        text: 'YES PROCEED',
                        handler: function () {
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": that.data._id,
                                "towAlert": tow
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: "Tow Alert alarm Deactivated!",
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                // this.callObjFunc(d);
                                // this.getdevices();
                            });
                        }
                    },
                    {
                        text: 'BACK',
                        handler: function () {
                            that.schedule = that.data.towAlert;
                            // d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            });
            alert_4.present();
        }
    };
    TimePickerModal.prototype.checkSettings = function () {
        this.showScheduler = true;
    };
    TimePickerModal.prototype.reset = function () {
        this.startHourNumber = this.pad(1, 2);
        this.startMinutesNumber = this.pad(0, 2);
        this.endHourNumber = this.pad(1, 2);
        this.endMinutesNumber = this.pad(0, 2);
    };
    TimePickerModal.prototype.onHourClicked = function (key1, key) {
        // debugger
        if (key1 === 'start') {
            if (key === 'up') {
                if (Number(this.startHourNumber) < 24) {
                    this.startHourNumber = Number(this.startHourNumber) + 1;
                    this.startHourNumber = this.pad(this.startHourNumber, 2);
                }
                else {
                    this.startHourNumber = this.pad(1, 2);
                }
            }
            else if (key === 'down') {
                if (Number(this.startHourNumber) > 1) {
                    this.startHourNumber = Number(this.startHourNumber) - 1;
                    this.startHourNumber = this.pad(this.startHourNumber, 2);
                }
                else {
                    this.startHourNumber = this.pad(24, 2);
                }
            }
        }
        else if (key1 === 'end') {
            if (key === 'up') {
                if (Number(this.endHourNumber) < 24) {
                    this.endHourNumber = Number(this.endHourNumber) + 1;
                    this.endHourNumber = this.pad(this.endHourNumber, 2);
                }
                else {
                    this.endHourNumber = this.pad(1, 2);
                }
            }
            else if (key === 'down') {
                if (Number(this.endHourNumber) > 1) {
                    this.endHourNumber = Number(this.endHourNumber) - 1;
                    this.endHourNumber = this.pad(this.endHourNumber, 2);
                }
                else {
                    this.endHourNumber = this.pad(24, 2);
                }
            }
        }
    };
    TimePickerModal.prototype.onMinuteClicked = function (key1, key) {
        if (key1 === 'start') {
            if (key === 'up') {
                if (Number(this.startMinutesNumber) < 59) {
                    this.startMinutesNumber = Number(this.startMinutesNumber) + 1;
                    this.startMinutesNumber = this.pad(this.startMinutesNumber, 2);
                }
                else {
                    this.startMinutesNumber = this.pad(1, 2);
                }
            }
            else if (key === 'down') {
                if (Number(this.startMinutesNumber) >= 1) {
                    this.startMinutesNumber = Number(this.startMinutesNumber) - 1;
                    this.startMinutesNumber = this.pad(this.startMinutesNumber, 2);
                }
                else {
                    this.startMinutesNumber = this.pad(59, 2);
                }
            }
        }
        else if (key1 === 'end') {
            if (key === 'up') {
                if (Number(this.endMinutesNumber) < 59) {
                    this.endMinutesNumber = Number(this.endMinutesNumber) + 1;
                    this.endMinutesNumber = this.pad(this.endMinutesNumber, 2);
                }
                else {
                    this.endMinutesNumber = this.pad(1, 2);
                }
            }
            else if (key === 'down') {
                if (Number(this.endMinutesNumber) >= 1) {
                    this.endMinutesNumber = Number(this.endMinutesNumber) - 1;
                    this.endMinutesNumber = this.pad(this.endMinutesNumber, 2);
                }
                else {
                    this.endMinutesNumber = this.pad(59, 2);
                }
            }
        }
    };
    TimePickerModal.prototype.ampmClicked = function (key) {
        if (key === 'start') {
            if (this.start_ampm === "AM") {
                this.start_ampm = "PM";
            }
            else if (this.start_ampm === "PM") {
                this.start_ampm = "AM";
            }
        }
        else if (key === 'end') {
            if (this.end_ampm === "AM") {
                this.end_ampm = "PM";
            }
            else if (this.end_ampm === "PM") {
                this.end_ampm = "AM";
            }
        }
    };
    TimePickerModal.prototype.submit = function () {
        var _this = this;
        debugger;
        var startTime, endTime;
        // var st = this.startHourNumber + ":" + this.startMinutesNumber + (this.start_ampm).toLowerCase();
        // var dt = moment(st.toString(), 'h:mm a').format('DD-MM-YYYY h:mm a');
        // console.log(dt);
        // startTime = new Date(dt).toISOString();
        // console.log("start time:", startTime);
        // var et = this.endHourNumber + ":" + this.endMinutesNumber + (this.end_ampm).toLowerCase();
        // var dt1 = moment(et.toString(), 'h:mm a').format('DD-MM-YYYY h:mm a');
        // console.log(dt1);
        // endTime = new Date(dt1).toISOString();
        // console.log("end time: ", endTime);
        var d = new Date();
        d.setHours(Number(this.startHourNumber));
        // var d = new Date();
        d.setMinutes(Number(this.startMinutesNumber));
        startTime = d;
        var d1 = new Date();
        d1.setHours(Number(this.endHourNumber));
        // var d = new Date();
        d1.setMinutes(Number(this.endMinutesNumber));
        endTime = d1;
        console.log("start time:", startTime);
        console.log("end time:", endTime);
        // console.log(d);
        var that = this;
        var payload;
        if (this.screenKey === 'parking') {
            payload = {
                "_id": that.data._id,
                "deviceid": that.data.Device_ID,
                "theftTime": {
                    "start": startTime,
                    "end": endTime
                }
            };
        }
        else if (this.screenKey === 'tow') {
            payload = {
                "_id": that.data._id,
                "deviceid": that.data.Device_ID,
                "towTime": {
                    "start": startTime,
                    "end": endTime
                }
            };
        }
        this.apiCall.startLoading().present();
        this.apiCall.deviceupdateCall(payload)
            .subscribe(function (resp) {
            console.log('response language code ' + resp);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: 'Alert is scheduled sucessfully.',
                duration: 1500,
                position: 'bottom'
            });
            toast.onDidDismiss(function () {
                _this.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    TimePickerModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/settings/notif-setting/time-picker/time-picker.html"*/'<ion-content padding style="background: #a60033;">\n    <ion-row>\n        <ion-col col-10>\n            <!-- <p style="font-size: 1.5em; font-weight: 350;">{{screenKey | titlecase}} Scheduler</p> -->\n        </ion-col>\n        <ion-col col-2 (click)="dismiss()" style="padding-right: 16px;text-align: right;">\n            <ion-icon name="close-circle" style="color: white; font-size: 1.8em;"></ion-icon>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-12 style="text-align: center;">\n            <p style="font-size: 1.5em; font-weight: 350;">{{screenKey | titlecase}} Scheduler</p>\n        </ion-col>\n    </ion-row>\n    <ion-item class="itemStyle">\n        <ion-label style="font-size: 1.5em;">{{data.Device_Name}}</ion-label>\n        <div item-right><img src="assets/imgs/schedule.png" style="height: 30px;" (click)="checkSettings()" /></div>\n        <ion-toggle color="oceanBlue" [(ngModel)]="schedule" (ngModelChange)="setScheduler(schedule)"></ion-toggle>\n        <!-- <ion-toggle readonly color="oceanBlue" [(ngModel)]="schedule"></ion-toggle> -->\n    </ion-item>\n</ion-content>\n<ion-footer no-header *ngIf="showScheduler">\n    <ion-toolbar>\n        <ion-row>\n            <ion-col col-2 style="text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">S</p>\n                </div>\n            </ion-col>\n            <ion-col col-2 style="margin-left: -11px;text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">M</p>\n                </div>\n            </ion-col>\n            <ion-col col-1 style="padding-left: 2px;text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">T</p>\n                </div>\n            </ion-col>\n            <ion-col col-2 style="padding-left: 18px;text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">W</p>\n                </div>\n            </ion-col>\n            <ion-col col-1 style="text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">T</p>\n                </div>\n            </ion-col>\n            <ion-col col-2 style="padding-left: 22px;text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">F</p>\n                </div>\n            </ion-col>\n            <ion-col col-2 style="text-align: center;">\n                <div class="dot">\n                    <p style="margin: 0px; font-weight: 600; color: white;">S</p>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row style="padding-bottom: 16px;">\n            <ion-col col-6 style="text-align: center;">\n                <p style="color: #ffffff; font-size: 1.5em; font-weight: 400;">Start</p>\n            </ion-col>\n            <ion-col col-6 style="text-align: center;">\n                <p style="color: #ffffff; font-size: 1.5em; font-weight: 400;">End</p>\n            </ion-col>\n        </ion-row>\n        <ion-row style="padding-bottom: 80px;">\n            <ion-col col-6 style="padding-right: 16px; color: white; border-right: 1px solid white;">\n                <ion-row>\n                    <ion-col col-1></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;" (click)="onHourClicked(\'start\',\'up\')">\n                        <ion-icon name="arrow-dropup"></ion-icon>\n                    </ion-col>\n                    <ion-col col-3></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;"\n                        (click)="onMinuteClicked(\'start\',\'up\')">\n                        <ion-icon name="arrow-dropup"></ion-icon>\n                    </ion-col>\n                    <ion-col col-2></ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-1></ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.3em; font-weight: 600;">{{startHourNumber}}</p>\n                    </ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.5em; font-weight: 600;">:</p>\n                    </ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.3em;font-weight: 600;">{{startMinutesNumber}}</p>\n                    </ion-col>\n                    <ion-col col-2></ion-col>\n                    <!-- <ion-col col-3 style="text-align: center;">\n                        <ion-badge style="font-size: 1em;background: transparent;" (click)="ampmClicked(\'start\')">{{start_ampm}}</ion-badge>\n                    </ion-col> -->\n                </ion-row>\n                <ion-row>\n                    <ion-col col-1></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;"\n                        (click)="onHourClicked(\'start\',\'down\')">\n                        <ion-icon name="arrow-dropdown"></ion-icon>\n                    </ion-col>\n                    <ion-col col-3></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;"\n                        (click)="onMinuteClicked(\'start\',\'down\')">\n                        <ion-icon name="arrow-dropdown"></ion-icon>\n                    </ion-col>\n                    <ion-col col-2></ion-col>\n                </ion-row>\n            </ion-col>\n            <ion-col col-6 style="padding-left: 16px; color: white;">\n                <ion-row>\n                    <ion-col col-2></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;" (click)="onHourClicked(\'end\',\'up\')">\n                        <ion-icon name="arrow-dropup"></ion-icon>\n                    </ion-col>\n                    <ion-col col-3></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;" (click)="onMinuteClicked(\'end\',\'up\')">\n                        <ion-icon name="arrow-dropup"></ion-icon>\n                    </ion-col>\n                    <ion-col col-1></ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col col-2></ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.3em;font-weight: 600;">{{endHourNumber}}</p>\n                    </ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.5em;font-weight: 600;">:</p>\n                    </ion-col>\n                    <ion-col col-3 style="text-align: center;">\n                        <p style="margin: 0px; font-size: 1.3em;font-weight: 600;">{{endMinutesNumber}}</p>\n                    </ion-col>\n                    <ion-col col-1></ion-col>\n                    <!-- <ion-col col-3 style="text-align: center;">\n                        <ion-badge style="font-size: 1em; background: transparent;" (click)="ampmClicked(\'end\')">{{end_ampm}}</ion-badge>\n                    </ion-col> -->\n                </ion-row>\n                <ion-row>\n                    <ion-col col-2></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;" (click)="onHourClicked(\'end\',\'down\')">\n                        <ion-icon name="arrow-dropdown"></ion-icon>\n                    </ion-col>\n                    <ion-col col-3></ion-col>\n                    <ion-col col-3 style="text-align: center; font-size: 1.5em;"\n                        (click)="onMinuteClicked(\'end\',\'down\')">\n                        <ion-icon name="arrow-dropdown"></ion-icon>\n                    </ion-col>\n                    <ion-col col-1></ion-col>\n                </ion-row>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col col-6 style="text-align: center;"><button ion-button round (click)="reset()">Reset</button>\n            </ion-col>\n            <ion-col col-6 style="text-align: center;" *ngIf="showSubmitBtn"><button ion-button round (click)="submit()">Submit</button>\n            </ion-col>\n            <ion-col col-6 style="text-align: center;" *ngIf="showUpdateBtn"><button ion-button round (click)="submit()">Update</button>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/settings/notif-setting/time-picker/time-picker.html"*/,
            selector: 'app-time-picker-modal'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], TimePickerModal);
    return TimePickerModal;
}());

//# sourceMappingURL=time-picker.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewDoc; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ViewDoc = /** @class */ (function () {
    function ViewDoc(navparams, viewCtrl, apiCall) {
        this.navparams = navparams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("param1: ", this.navparams.get("param1"));
        this._instData = this.navparams.get("param1");
        var str = this._instData.imageURL;
        var str1 = str.split('public/');
        this.imgUrl = this.apiCall.mainUrl + str1[1];
        console.log("img url: ", this._instData);
    }
    ViewDoc.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ViewDoc = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'page-view-doc',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/add-devices/upload-doc/view-doc/view-doc.html"*/'<div>\n    <ion-row>\n        <ion-col col-11>\n\n        </ion-col>\n        <ion-col col-1>\n            <ion-icon style="font-size:1.5em" name="close-circle" (tap)="dismiss()"></ion-icon>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col style="text-align: center;">\n            <img src="{{imgUrl}}" width="300" height="450" />\n        </ion-col>\n    </ion-row>\n    <ion-row padding-left>\n        <ion-col col-3>\n            <b>{{ "Expiry Date:" | translate }}</b>\n        </ion-col>\n        <ion-col col-9>{{_instData.expDate | date: \'medium\'}}</ion-col>\n    </ion-row>\n</div>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/add-devices/upload-doc/view-doc/view-doc.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], ViewDoc);
    return ViewDoc;
}());

//# sourceMappingURL=view-doc.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatus */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(564);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ConnectionStatus;
(function (ConnectionStatus) {
    ConnectionStatus[ConnectionStatus["Online"] = 0] = "Online";
    ConnectionStatus[ConnectionStatus["Offline"] = 1] = "Offline";
})(ConnectionStatus || (ConnectionStatus = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(network, toastController, plt) {
        var _this = this;
        this.network = network;
        this.toastController = toastController;
        this.plt = plt;
        this.status = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](ConnectionStatus.Offline);
        this.plt.ready().then(function () {
            _this.initializeNetworkEvents();
            var status = _this.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;
            _this.status.next(status);
        });
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.status.getValue() === ConnectionStatus.Online) {
                console.log('WE ARE OFFLINE');
                _this.updateNetworkStatus(ConnectionStatus.Offline);
            }
        });
        this.network.onConnect().subscribe(function () {
            if (_this.status.getValue() === ConnectionStatus.Offline) {
                console.log('WE ARE ONLINE');
                _this.updateNetworkStatus(ConnectionStatus.Online);
            }
        });
    };
    // showToastMsg(msg) {
    //   let toast = this.toastCtrl.create({
    //     message: msg,
    //     duration: 3000,
    //     position: 'middle'
    //   });
    //   toast.present();
    // }
    NetworkProvider.prototype.updateNetworkStatus = function (status) {
        return __awaiter(this, void 0, void 0, function () {
            var connection, toast;
            return __generator(this, function (_a) {
                this.status.next(status);
                connection = status == ConnectionStatus.Offline ? 'Offline' : 'Online';
                toast = this.toastController.create({
                    message: "You are now " + connection,
                    duration: 3000,
                    position: 'middle'
                });
                toast.present();
                return [2 /*return*/];
            });
        });
    };
    NetworkProvider.prototype.onNetworkChange = function () {
        return this.status.asObservable();
    };
    NetworkProvider.prototype.getCurrentNetworkStatus = function () {
        return this.status.getValue();
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__ = __webpack_require__(565);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular
 // tslint:disable-line
// Ionic


// This class is defined in this file because
// we don't want to make it exportable
var InnerMenuOptionModel = /** @class */ (function () {
    function InnerMenuOptionModel() {
    }
    InnerMenuOptionModel.fromMenuOptionModel = function (option, parent) {
        var innerMenuOptionModel = new InnerMenuOptionModel();
        innerMenuOptionModel.id = this.counter++;
        innerMenuOptionModel.iconName = option.iconName;
        innerMenuOptionModel.displayText = option.displayText;
        innerMenuOptionModel.badge = option.badge;
        innerMenuOptionModel.targetOption = option;
        innerMenuOptionModel.parent = parent || null;
        innerMenuOptionModel.selected = option.selected;
        if (option.suboptions) {
            innerMenuOptionModel.expanded = false;
            innerMenuOptionModel.suboptionsCount = option.suboptions.length;
            innerMenuOptionModel.subOptions = [];
            option.suboptions.forEach(function (subItem) {
                var innerSubItem = InnerMenuOptionModel.fromMenuOptionModel(subItem, innerMenuOptionModel);
                innerMenuOptionModel.subOptions.push(innerSubItem);
                // Select the parent if any
                // child option is selected
                if (subItem.selected) {
                    innerSubItem.parent.selected = true;
                    innerSubItem.parent.expanded = true;
                }
            });
        }
        return innerMenuOptionModel;
    };
    InnerMenuOptionModel.counter = 1;
    return InnerMenuOptionModel;
}());
var SideMenuContentComponent = /** @class */ (function () {
    function SideMenuContentComponent(platform, eventsCtrl, cdRef) {
        var _this = this;
        this.platform = platform;
        this.eventsCtrl = eventsCtrl;
        this.cdRef = cdRef;
        this.collapsableItems = [];
        // Outputs: return the selected option to the caller
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // Handle the redirect event
        this.eventsCtrl.subscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */], function (data) {
            _this.updateSelectedOption(data);
        });
    }
    Object.defineProperty(SideMenuContentComponent.prototype, "options", {
        set: function (value) {
            var _this = this;
            if (value) {
                // Keep a reference to the options
                // sent to this component
                this.menuOptions = value;
                this.collapsableItems = new Array();
                // Map the options to our internal models
                this.menuOptions.forEach(function (option) {
                    var innerMenuOption = InnerMenuOptionModel.fromMenuOptionModel(option);
                    _this.collapsableItems.push(innerMenuOption);
                    // Check if there's any option marked as selected
                    if (option.selected) {
                        _this.selectedOption = innerMenuOption;
                    }
                    else if (innerMenuOption.suboptionsCount) {
                        innerMenuOption.subOptions.forEach(function (subItem) {
                            if (subItem.selected) {
                                _this.selectedOption = subItem;
                            }
                        });
                    }
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this.menuSettings = value;
                this.mergeSettings();
            }
        },
        enumerable: true,
        configurable: true
    });
    SideMenuContentComponent.prototype.ngOnDestroy = function () {
        this.eventsCtrl.unsubscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */]);
    };
    // ---------------------------------------------------
    // PUBLIC methods
    // ---------------------------------------------------
    // Send the selected option to the caller component
    SideMenuContentComponent.prototype.select = function (option) {
        if (this.menuSettings.showSelectedOption) {
            this.setSelectedOption(option);
        }
        // Return the selected option (not our inner option)
        this.change.emit(option.targetOption);
    };
    // Toggle the sub options of the selected item
    SideMenuContentComponent.prototype.toggleItemOptions = function (targetOption) {
        if (!targetOption)
            return;
        // If the accordion mode is set to true, we need
        // to collapse all the other menu options
        if (this.menuSettings.accordionMode) {
            this.collapsableItems.forEach(function (option) {
                if (option.id !== targetOption.id) {
                    option.expanded = false;
                }
            });
        }
        // Toggle the selected option
        targetOption.expanded = !targetOption.expanded;
    };
    // Reset the entire menu
    SideMenuContentComponent.prototype.collapseAllOptions = function () {
        this.collapsableItems.forEach(function (option) {
            if (!option.selected) {
                option.expanded = false;
            }
            if (option.suboptionsCount) {
                option.subOptions.forEach(function (subItem) {
                    if (subItem.selected) {
                        // Expand the parent if any of
                        // its childs is selected
                        subItem.parent.expanded = true;
                    }
                });
            }
        });
        // Update the view since there wasn't
        // any user interaction with it
        this.cdRef.detectChanges();
    };
    Object.defineProperty(SideMenuContentComponent.prototype, "subOptionIndentation", {
        // Get the proper indentation of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.subOptionIndentation.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.subOptionIndentation.wp;
            return this.menuSettings.subOptionIndentation.md;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "optionHeight", {
        // Get the proper height of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.optionHeight.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.optionHeight.wp;
            return this.menuSettings.optionHeight.md;
        },
        enumerable: true,
        configurable: true
    });
    // ---------------------------------------------------
    // PRIVATE methods
    // ---------------------------------------------------
    // Method that set the selected option and its parent
    SideMenuContentComponent.prototype.setSelectedOption = function (option) {
        if (!option.targetOption.component)
            return;
        // Clean the current selected option if any
        if (this.selectedOption) {
            this.selectedOption.selected = false;
            this.selectedOption.targetOption.selected = false;
            if (this.selectedOption.parent) {
                this.selectedOption.parent.selected = false;
                this.selectedOption.parent.expanded = false;
            }
            this.selectedOption = null;
        }
        // Set this option to be the selected
        option.selected = true;
        option.targetOption.selected = true;
        if (option.parent) {
            option.parent.selected = true;
            option.parent.expanded = true;
        }
        // Keep a reference to the selected option
        this.selectedOption = option;
        // Update the view if needed since we may have
        // expanded or collapsed some options
        this.cdRef.detectChanges();
    };
    // Update the selected option
    SideMenuContentComponent.prototype.updateSelectedOption = function (data) {
        if (!data.displayText)
            return;
        var targetOption;
        this.collapsableItems.forEach(function (option) {
            if (option.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                targetOption = option;
            }
            else if (option.suboptionsCount) {
                option.subOptions.forEach(function (subOption) {
                    if (subOption.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                        targetOption = subOption;
                    }
                });
            }
        });
        if (targetOption) {
            this.setSelectedOption(targetOption);
        }
    };
    // Merge the settings received with the default settings
    SideMenuContentComponent.prototype.mergeSettings = function () {
        var defaultSettings = {
            accordionMode: false,
            optionHeight: {
                ios: 50,
                md: 50,
                wp: 50
            },
            arrowIcon: 'ios-arrow-down',
            showSelectedOption: false,
            selectedOptionClass: 'selected-option',
            indentSubOptionsWithoutIcons: false,
            subOptionIndentation: {
                ios: 16,
                md: 16,
                wp: 16
            }
        };
        if (!this.menuSettings) {
            // Use the default values
            this.menuSettings = defaultSettings;
            return;
        }
        if (!this.menuSettings.optionHeight) {
            this.menuSettings.optionHeight = defaultSettings.optionHeight;
        }
        else {
            this.menuSettings.optionHeight.ios = this.isDefinedAndPositive(this.menuSettings.optionHeight.ios) ? this.menuSettings.optionHeight.ios : defaultSettings.optionHeight.ios;
            this.menuSettings.optionHeight.md = this.isDefinedAndPositive(this.menuSettings.optionHeight.md) ? this.menuSettings.optionHeight.md : defaultSettings.optionHeight.md;
            this.menuSettings.optionHeight.wp = this.isDefinedAndPositive(this.menuSettings.optionHeight.wp) ? this.menuSettings.optionHeight.wp : defaultSettings.optionHeight.wp;
        }
        this.menuSettings.showSelectedOption = this.isDefined(this.menuSettings.showSelectedOption) ? this.menuSettings.showSelectedOption : defaultSettings.showSelectedOption;
        this.menuSettings.accordionMode = this.isDefined(this.menuSettings.accordionMode) ? this.menuSettings.accordionMode : defaultSettings.accordionMode;
        this.menuSettings.arrowIcon = this.isDefined(this.menuSettings.arrowIcon) ? this.menuSettings.arrowIcon : defaultSettings.arrowIcon;
        this.menuSettings.selectedOptionClass = this.isDefined(this.menuSettings.selectedOptionClass) ? this.menuSettings.selectedOptionClass : defaultSettings.selectedOptionClass;
        this.menuSettings.indentSubOptionsWithoutIcons = this.isDefined(this.menuSettings.indentSubOptionsWithoutIcons) ? this.menuSettings.indentSubOptionsWithoutIcons : defaultSettings.indentSubOptionsWithoutIcons;
        if (!this.menuSettings.subOptionIndentation) {
            this.menuSettings.subOptionIndentation = defaultSettings.subOptionIndentation;
        }
        else {
            this.menuSettings.subOptionIndentation.ios = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.ios) ? this.menuSettings.subOptionIndentation.ios : defaultSettings.subOptionIndentation.ios;
            this.menuSettings.subOptionIndentation.md = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.md) ? this.menuSettings.subOptionIndentation.md : defaultSettings.subOptionIndentation.md;
            this.menuSettings.subOptionIndentation.wp = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.wp) ? this.menuSettings.subOptionIndentation.wp : defaultSettings.subOptionIndentation.wp;
        }
    };
    SideMenuContentComponent.prototype.isDefined = function (property) {
        return property !== null && property !== undefined;
    };
    SideMenuContentComponent.prototype.isDefinedAndPositive = function (property) {
        return this.isDefined(property) && !isNaN(property) && property > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('options'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], SideMenuContentComponent.prototype, "options", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('settings'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideMenuContentComponent.prototype, "settings", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], SideMenuContentComponent.prototype, "change", void 0);
    SideMenuContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'side-menu-content',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/shared/side-menu-content/side-menu-content.component.html"*/'<ion-list no-margin no-lines>\n    <ng-template ngFor let-option [ngForOf]="collapsableItems" let-i="index">\n\n        <!-- It is a simple option -->\n        <ng-template [ngIf]="!option.suboptionsCount">\n            <ion-item class="option" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                (tap)="select(option)" tappable>\n                <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n                {{ option.displayText }}\n                <ion-badge item-right *ngIf="option.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n            </ion-item>\n        </ng-template>\n\n        <!-- It has nested options -->\n        <ng-template [ngIf]="option.suboptionsCount">\n\n            <ion-list no-margin class="accordion-menu">\n\n                <!-- Header -->\n                <ion-item class="header" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                    (tap)="toggleItemOptions(option)" tappable>\n                    <ion-icon [class.rotate]="option.expanded" class="header-icon" [name]="option.iconName || menuSettings.arrowIcon"\n                        item-left></ion-icon>\n                    <!-- <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon> -->\n                    {{ option.displayText }}\n                </ion-item>\n\n                <!-- Sub items -->\n                <div [style.height]="option.expanded ? ((optionHeight + 1) * option.suboptionsCount) + \'px\' : \'0px\'" class="options">\n                    <ng-template ngFor let-item [ngForOf]="option.subOptions">\n                        <ion-item class="sub-option" [style.padding-left]="subOptionIndentation + \'px\'" [class.no-icon]="menuSettings?.indentSubOptionsWithoutIcons && !item.iconName"\n                            [ngClass]="menuSettings?.showSelectedOption && item.selected ? menuSettings.selectedOptionClass : null"\n                            tappable (tap)="select(item)">\n                            <ion-icon *ngIf="item.iconName" [name]="item.iconName" item-left></ion-icon>\n                            {{ item.displayText }}\n                            <ion-badge item-right *ngIf="item.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n                        </ion-item>\n                    </ng-template>\n                </div>\n            </ion-list>\n\n        </ng-template>\n\n    </ng-template>\n</ion-list>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/shared/side-menu-content/side-menu-content.component.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SideMenuContentComponent);
    return SideMenuContentComponent;
}());

//# sourceMappingURL=side-menu-content.component.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LoginPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, formBuilder, alertCtrl, apiservice, toastCtrl, popoverCtrl, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.apiservice = apiservice;
        this.toastCtrl = toastCtrl;
        this.popoverCtrl = popoverCtrl;
        this.translate = translate;
        this.showPassword = false;
        // this.callBaseURL();
        this.loginForm = formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    LoginPage_1 = LoginPage;
    // callBaseURL() {
    //   let url = "https://www.oneqlik.in/pullData/getUrl";
    //   this.apiservice.getSOSReportAPI(url)
    //     .subscribe((data) => {
    //       console.log("base url: ", data);
    //       if (data.url) {
    //         localStorage.setItem("BASE_URL", JSON.stringify(data.url));
    //       }
    //     });
    // }
    LoginPage.prototype.ionViewWillEnter = function () {
        console.log("ionViewWillEnter");
        // if (localStorage.getItem("LANG_KEY") == null) {
        //   this.presentPopover();
        // }
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        console.log("ionViewDidEnter");
    };
    LoginPage.prototype.toggleShowPassword = function () {
        this.showPassword = !this.showPassword;
    };
    LoginPage.prototype.userlogin = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.loginForm.value.username == "") {
            /*alert("invalid");*/
            return false;
        }
        else if (this.loginForm.value.password == "") {
            /*alert("invalid");*/
            return false;
        }
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        var isEmail = validateEmail(this.loginForm.value.username);
        var isName = isNaN(this.loginForm.value.username);
        var strNum;
        if (isName == false) {
            strNum = this.loginForm.value.username.trim();
        }
        if (isEmail == false && isName == false && strNum.length == 10) {
            this.data = {
                "psd": this.loginForm.value.password,
                "ph_num": this.loginForm.value.username
            };
        }
        else if (isEmail) {
            this.data = {
                "psd": this.loginForm.value.password,
                "emailid": this.loginForm.value.username
            };
        }
        else {
            this.data = {
                "psd": this.loginForm.value.password,
                "user_id": this.loginForm.value.username
            };
        }
        this.apiservice.startLoading();
        this.apiservice.loginApi(this.data)
            .subscribe(function (response) {
            _this.logindata = response;
            _this.logindata = JSON.stringify(response);
            var logindetails = JSON.parse(_this.logindata);
            _this.userDetails = window.atob(logindetails.token.split('.')[1]);
            _this.details = JSON.parse(_this.userDetails);
            console.log(_this.details.email);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(_this.details));
            localStorage.setItem('condition_chk', _this.details.isDealer);
            localStorage.setItem('VeryFirstLoginUser', _this.details._id);
            localStorage.setItem("INTRO", "INTRO");
            _this.apiservice.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant("Welcome! You're logged In successfully."),
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.navCtrl.setRoot('DashboardPage');
            });
            toast.present();
        }, function (error) {
            // console.log("login error => "+error)
            var body = error._body;
            var msg = JSON.parse(body);
            if (msg.message == "Mobile Phone Not Verified") {
                var confirmPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: _this.translate.instant('Okay'),
                            handler: function () {
                                // this.navCtrl.push(MobileVerify);
                            }
                        }
                    ]
                });
                confirmPopup.present();
            }
            else {
                // Do something on error
                var alertPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message,
                    buttons: [_this.translate.instant('Okay')]
                });
                alertPopup.present();
            }
            _this.apiservice.stopLoading();
        });
    };
    LoginPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2500
        });
        toast.present();
    };
    LoginPage.prototype.gotosignuppage = function () {
        this.navCtrl.push('SignupPage');
    };
    LoginPage.prototype.forgotPassFunc = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Forgot Password'),
            message: this.translate.instant('Enter your Registered Mobile Number and we will send you a confirmation code.'),
            inputs: [
                {
                    name: 'mobno',
                    placeholder: this.translate.instant('Mobile Number')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('SEND CONFIRMATION CODE'),
                    handler: function (data) {
                        var forgotdata = {
                            "cred": data.mobno
                        };
                        _this.apiservice.startLoading();
                        _this.apiservice.forgotPassApi(forgotdata)
                            .subscribe(function (data) {
                            _this.apiservice.stopLoading();
                            _this.presentToast(data.message);
                            _this.otpMess = data;
                            _this.PassWordConfirmPopup();
                        }, function (error) {
                            _this.apiservice.stopLoading();
                            var body = error._body;
                            var msg = JSON.parse(body);
                            var alert = _this.alertCtrl.create({
                                title: _this.translate.instant('Forgot Password Failed!'),
                                message: msg.message,
                                buttons: [_this.translate.instant('Okay')]
                            });
                            alert.present();
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.PassWordConfirmPopup = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Reset Password'),
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'newpass',
                    placeholder: this.translate.instant('Password')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('Back'),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.translate.instant('SAVE'),
                    handler: function (data) {
                        if (!data.newpass || !data.mobilenum || !data.confCode) {
                            var alertPopup = _this.alertCtrl.create({
                                title: _this.translate.instant('Warning'),
                                message: _this.translate.instant('Fill all mandatory fields!'),
                                buttons: [_this.translate.instant('Okay')]
                            });
                            alertPopup.present();
                        }
                        else {
                            if (data.newpass == data.mobilenum && data.newpass && data.mobilenum) {
                                if (data.newpass.length < 6 || data.newpass.length > 12) {
                                    var Popup = _this.alertCtrl.create({
                                        title: _this.translate.instant('Warning'),
                                        message: _this.translate.instant('Password length should be 6 - 12'),
                                        buttons: [_this.translate.instant('Okay')]
                                    });
                                    Popup.present();
                                }
                                else {
                                    var Passwordset = {
                                        "newpwd": data.newpass,
                                        "otp": data.confCode,
                                        "phone": _this.otpMess,
                                        "cred": _this.otpMess
                                    };
                                    _this.apiservice.startLoading();
                                    _this.apiservice.forgotPassMobApi(Passwordset)
                                        .subscribe(function (data) {
                                        _this.apiservice.stopLoading();
                                        _this.presentToast(data.message);
                                        _this.navCtrl.setRoot(LoginPage_1);
                                    }, function (error) {
                                        _this.apiservice.stopLoading();
                                        var body = error._body;
                                        var msg = JSON.parse(body);
                                        var alert = _this.alertCtrl.create({
                                            title: _this.translate.instant('Forgot Password Failed!'),
                                            message: msg.message,
                                            buttons: [_this.translate.instant('Okay')]
                                        });
                                        alert.present();
                                    });
                                }
                            }
                            else {
                                var alertPopup = _this.alertCtrl.create({
                                    title: _this.translate.instant('Warning'),
                                    message: _this.translate.instant('New Password and Confirm Password Not Matched'),
                                    buttons: [_this.translate.instant('Okay')]
                                });
                                alertPopup.present();
                            }
                            if (!data.newpass || !data.mobilenum || !data.confCode) {
                                //don't allow the user to close unless he enters model...
                                return false;
                            }
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/login/login.html"*/'<ion-content id=outer>\n  <div class="warning">\n    <!-- <div id=inner> -->\n    <div class="logo">\n      <img src="assets/imgs/icon.png">\n    </div>\n    <form [formGroup]="loginForm">\n      <div class="temp">\n        <!-- <ion-item class="logitem"> -->\n        <ion-item style="background-color: transparent; padding-left: 0px;" class="logitem">\n          <!-- <ion-icon item-start name="person"></ion-icon> -->\n          <ion-input formControlName="username" type="text" placeholder="{{\'Email/Mobile/User Id*\' | translate}}"\n            class="text-input">\n          </ion-input>\n        </ion-item>\n      </div>\n      <ion-item class="logitem1"\n        *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)">\n        <p style="padding-left: 8px;">{{\'username required!\' | translate}}</p>\n      </ion-item>\n      <div class="temp">\n        <!-- <ion-item class="logitem"> -->\n        <ion-item style="background-color: transparent; padding-left: 0px;" class="logitem">\n          <!-- <ion-icon name="lock"></ion-icon> -->\n          <ion-input formControlName="password" *ngIf="!showPassword" type="password"\n            placeholder="{{\'Password*\' | translate}}" class="text-input"></ion-input>\n          <ion-input formControlName="password" *ngIf="showPassword" type="text"\n            placeholder="{{\'Password*\' | translate}}" class="text-input"></ion-input>\n\n          <button ion-button clear item-end (click)="toggleShowPassword()">\n            <ion-icon style="font-size: 1.6em;" *ngIf="showPassword" name="eye" color="light"></ion-icon>\n            <ion-icon style="font-size: 1.6em;" *ngIf="!showPassword" name="eye-off" color="light"></ion-icon>\n          </button>\n        </ion-item>\n      </div>\n      <ion-item class="logitem1"\n        *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)">\n        <p style="padding-left: 8px;">{{\'Password required!\' | translate}}</p>\n      </ion-item>\n    </form>\n    <div class="btnDiv">\n      <button ion-button class="btnLog" color="gpsc" (tap)="userlogin()">{{\'SIGN IN\' | translate}}</button>\n      <ion-row>\n        <ion-col width-50 ion-text color="light" style="font-size: 1.1em;" (tap)="forgotPassFunc()">\n          {{\'Forgot Password\' | translate}} ?</ion-col>\n        <ion-col width-50 ion-text color="light" style="font-size: 1.1em;">{{\'New user?\' | translate}}&nbsp;&nbsp;\n          <span style="color:#6fa9cd;" (tap)="gotosignuppage()">{{\'SIGN UP\' | translate}}</span>\n        </ion-col>\n      </ion-row>\n    </div>\n    <!-- </div> -->\n    <ion-row style="margin-top: 50%;">\n      <ion-col col-12 style="text-align: center;">\n        <p style="margin: 0px; color: #fff; font-weight: 500; font-size: 10px;">\n          Call Support Time : 11 AM to 8 PM\n        </p>\n      </ion-col>\n      <ion-col col-12>\n        <p style="margin: 0px; color: #fff; font-weight: 500;font-size: 10px;">\n          <ion-icon name="mail"></ion-icon>&nbsp;&nbsp;\n          support@oneqlik.in &nbsp;&nbsp; <ion-icon name="call"></ion-icon>&nbsp;&nbsp;\n          +91 96737 77264\n        </p>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1;
}());

var LanguagesPage = /** @class */ (function () {
    function LanguagesPage(viewCtrl, translate, apiCall, toastCtrl, events) {
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    LanguagesPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    LanguagesPage.prototype.setLanguage = function (key) {
        var _this = this;
        var payload = {
            uid: this.islogin._id,
            lang: key
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe(function (resp) {
            console.log('response language code ' + resp);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: resp.message,
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
        });
        this.events.publish('lang:key', key);
        // this language will be used as a fallback when a translation isn't found in the current language
        // this.translate.use(key);
        // localStorage.setItem("LANG_KEY", key);
        this.viewCtrl.dismiss();
    };
    LanguagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-list-header style=\"text-align: center;\">{{'Select Language' | translate}}</ion-list-header>\n      <button ion-item (click)=\"setLanguage('en')\">{{'English' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('hi')\">{{'Hindi' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('bn')\">{{'Bangali' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('te')\">{{'Telugu' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ta')\">{{'Tamil' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('gu')\">{{'Gujarati' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('kn')\">{{'Kannada' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('mr')\">{{'Marathi' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ml')\">{{'Malayalam' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('sp')\">{{'Spanish' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('fa')\">{{'Persian' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ar')\">{{'Arabic' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ne')\">{{'Nepali' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('fr')\">{{'French' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('pn')\">{{'Punjabi' | translate}}</button>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], LanguagesPage);
    return LanguagesPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiveSingleDevice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PoiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_geocoder_geocoder__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_Subscription__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__live_map_style_model__ = __webpack_require__(543);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_tinyurl__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_tinyurl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_tinyurl__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










// import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';






// declare var plugin: any;
// declare var Window;
var LiveSingleDevice = /** @class */ (function () {
    function LiveSingleDevice(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, platform, event, modalCtrl, socialSharing, alertCtrl, toastCtrl, storage, plt, 
    // private launchNavigator: LaunchNavigator,
    // private nativePageTransitions: NativePageTransitions,
    // private viewCtrl: ViewController,
    translate, geocoderApi) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.platform = platform;
        this.event = event;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.plt = plt;
        this.translate = translate;
        this.geocoderApi = geocoderApi;
        this.showDistDuration = false;
        this.streetviewButtonColor = '#6FA9CD';
        this.streetColor = '#fff';
        this.navigateButtonColor = '#6FA9CD'; //Default Color
        this.navColor = '#fff';
        this.policeButtonColor = '#6FA9CD';
        this.policeColor = '#fff';
        this.eyeBtnColor = "#6FA9CD";
        this.eyeColor = "#fff";
        this.petrolButtonColor = "#6FA9CD";
        this.petrolColor = '#fff';
        this.shouldBounce = true;
        this.dockedHeight = 80;
        this.distanceTop = 200;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 50;
        this.transition = '0.85s ease-in-out';
        this.drawerState1 = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.dockedHeight1 = 150;
        this.distanceTop1 = 378;
        this.minimumHeight1 = 0;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.selectedFlag = { _id: '', flag: false };
        this.allData = {};
        this.isEnabled = false;
        this.showShareBtn = false;
        this.mapHideTraffic = false;
        this.locateme = false;
        this.deviceDeatils = {};
        this.tempArray = [];
        this.mapTrail = false;
        this.condition = 'gpsc';
        this.condition1 = 'light';
        this.condition2 = 'light';
        this.showaddpoibtn = false;
        this.btnString = "Create Trip";
        this.showIcon = false;
        this.expectation = {};
        this.polyLines = [];
        this.marksArray = [];
        this.zoomLevel = 15;
        this.markersArray = [];
        this.devices = [];
        this.latLngArray = [];
        this.fraction = 0;
        this.direction = 1;
        this.navigateBtn = false;
        this.singleVehData = {};
        this.nearbyPolicesArray = [];
        this.nearbyPetrolArray = [];
        this.takeAction = "live";
        this.socketurl = "https://www.oneqlik.in";
        this.showStreetMap = false;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z";
        this.truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [10, 20],
        };
        this.busIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.bikeIcon = {
            path: this.bike,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.truckIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [10, 20],
        };
        this.userIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.ambulance = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [10, 20],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.bikeIcon,
            "truck": this.truckIcon,
            "bus": this.busIcon,
            "user": this.userIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon,
            "ambulance": this.carIcon,
            "auto": this.carIcon
        };
        this.hideMe = false;
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_12_rxjs_Subscription__["Subscription"]();
        // Environment.setBackgroundColor("black");
        this.callBaseURL();
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.event.subscribe("tripstatUpdated", function (data) {
            if (data) {
                _this.checktripstat();
            }
        });
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        if (navParams.get("device") != null) {
            this.deviceDeatils = navParams.get("device");
        }
        this.motionActivity = 'Activity';
        this.menuActive = false;
        // console.log("check @Input() activity: ", this.deviceData);
    }
    LiveSingleDevice.prototype.callBaseURL = function () {
        var _this = this;
        // debugger
        var url = "https://www.oneqlik.in/pullData/getUrlnew";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (data) {
            console.log("base url: ", data);
            if (data.url) {
                localStorage.setItem("BASE_URL", JSON.stringify(data.url));
            }
            if (data.socket) {
                localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
            }
            _this.getSocketUrl();
        });
    };
    LiveSingleDevice.prototype.ngOnInit = function () {
        var that = this;
        that.tempArray = [];
        this.checktripstat();
    };
    LiveSingleDevice.prototype.getSocketUrl = function () {
        if (localStorage.getItem('SOCKET_URL') !== null) {
            this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
        }
    };
    LiveSingleDevice.prototype.ionViewDidEnter = function () {
        var _this = this;
        localStorage.removeItem("liveTrackingTarget");
        localStorage.removeItem("liveTrackingLat");
        localStorage.removeItem("liveTrackingLng");
        localStorage.removeItem('some_last_speed');
        this.navBar.backButtonClick = function (ev) {
            console.log('this will work in Ionic 3 +');
            if (_this.allData.map) {
                _this.allData.map.remove();
            }
            _this.hideMe = true;
            _this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'back'
            });
        };
        // this.allData.map.setDiv(this.mapsProvider.map.mapElement.nativeElement);
        // this.allData.map.setVisible(true);
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.socketurl + "/gps", { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        this.showShareBtn = true;
        var paramData = this.navParams.get("device");
        this.singleVehData = this.navParams.get("device");
        this.titleText = paramData.Device_Name;
        this.temp(paramData);
        this.showActionSheet = true;
    };
    LiveSingleDevice.prototype.ionViewWillEnter = function () {
        // if (this.plt.is('ios')) {
        //   this.shwBckBtn = true;
        //   this.viewCtrl.showBackButton(false);
        // }
        var _this = this;
        this.platform.ready().then(function () {
            _this.resumeListener = _this.platform.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: _this.translate.instant('Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?'),
                        buttons: [
                            {
                                text: _this.translate.instant('YES PROCEED'),
                                handler: function () {
                                    _this.refreshMe();
                                }
                            },
                            {
                                text: _this.translate.instant('Back'),
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    LiveSingleDevice.prototype.ionViewWillLeave = function () {
        var _this = this;
        // this.allData.map.remove();
        this.platform.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
    };
    LiveSingleDevice.prototype.ionViewDidLeave = function () {
        var that = this;
        that.takeAction = "live";
        if (that.circleTimer) {
            clearInterval(that.circleTimer);
        }
        if (that.intevalId) {
            clearInterval(that.intevalId);
        }
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that._io.on('disconnect', function () {
            that._io.open();
        });
        if (localStorage.getItem("livepagetravelDetailsObject") != null) {
            localStorage.removeItem("livepagetravelDetailsObject");
        }
        localStorage.removeItem("gotPing");
        //////
        // that.allData.map.setVisible(false);
        // that.allData.map.setDiv(null);
    };
    LiveSingleDevice.prototype.ngOnDestroy = function () {
        // let that = this;
        // if (that.circleTimer) {
        //   clearInterval(that.circleTimer);
        // }
        // if (that.intevalId) {
        //   clearInterval(that.intevalId);
        // }
        // for (var i = 0; i < that.socketChnl.length; i++)
        //   that._io.removeAllListeners(that.socketChnl[i]);
        // that._io.on('disconnect', () => {
        //   that._io.open();
        // })
        // if (localStorage.getItem("livepagetravelDetailsObject") != null) {
        //   localStorage.removeItem("livepagetravelDetailsObject");
        // }
        // localStorage.removeItem("gotPing");
    };
    LiveSingleDevice.prototype.setDocHeight = function () {
        var that = this;
        that.distanceTop = 200;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LiveSingleDevice.prototype.setDocHeightAtFirst = function () {
        var that = this;
        that.distanceTop = 80;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LiveSingleDevice.prototype.segmentChanged = function (ev) {
        console.log(ev);
        this.takeAction = ev._value;
        if (this.takeAction === 'history') {
            this.navCtrl.push('HistoryDevicePage', {
                device: this.singleVehData
            });
        }
        else if (this.takeAction === 'trip') {
            this.navCtrl.push('TripReportPage', {
                param: this.singleVehData
            });
        }
        else if (this.takeAction === 'notif') {
            this.navCtrl.push('AllNotificationsPage', {
                param: this.singleVehData
            });
        }
        else if (this.takeAction === 'car_service') {
            this.navCtrl.push('MaintenanceReminderPage', {
                param: this.singleVehData
            });
        }
    };
    LiveSingleDevice.prototype.streetView = function () {
        if (this.streetviewButtonColor == '#6FA9CD') {
            this.streetviewButtonColor = '#fff';
            this.streetColor = '#6FA9CD';
        }
        else if (this.streetviewButtonColor == '#fff') {
            this.streetviewButtonColor = '#6FA9CD';
            this.streetColor = '#fff';
        }
        var that = this;
        this.showStreetMap = !this.showStreetMap;
        if (this.showStreetMap) {
            document.getElementById("map_canvas_single_device").style.height = "50%";
            // debugger
            var fenway = {};
            if (this.singleVehData.last_location !== undefined) {
                fenway = { lat: this.singleVehData.last_location.lat, lng: this.singleVehData.last_location.long };
            }
            else if (this.singleVehData.last_lat !== undefined && this.singleVehData.last_lng !== undefined) {
                fenway = { lat: this.singleVehData.last_lat, lng: this.singleVehData.last_lng };
            }
            console.log("fenway: ", fenway);
            var panorama = new google.maps.StreetViewPanorama(document.getElementById("viewParanoma"), {
                position: fenway,
                pov: {
                    heading: 34,
                    pitch: 10
                }
            });
            that.allData.map.setStreetView(panorama);
        }
        else {
            // document.getElementById('map_canvas_single_device').setAttribute("style", "height:99%");
            document.getElementById("map_canvas_single_device").style.height = "99%";
        }
    };
    LiveSingleDevice.prototype.showNearby = function (key) {
        // for police stations
        var url, icurl;
        if (key === 'police') {
            url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=police&key=AIzaSyCmSXnoBBGekErvqBvu5N7dbeqdH1pHp7c";
            if (this.plt.is('android')) {
                icurl = './assets/imgs/police-station.png';
            }
            else if (this.plt.is('ios')) {
                icurl = 'www/assets/imgs/police-station.png';
            }
            if (this.policeButtonColor == '#6FA9CD') {
                this.policeButtonColor = '#f4f4f4';
                this.policeColor = '#6FA9CD';
                this.addNearbyMarkers(key, url, icurl);
            }
            else {
                if (this.policeButtonColor == '#f4f4f4') {
                    this.policeButtonColor = '#6FA9CD';
                    this.policeColor = '#fff';
                    console.log("all nearby police markers: ", this.nearbyPolicesArray.length);
                    if (this.nearbyPolicesArray.length > 0) {
                        // this.allData.map
                        for (var g = 0; g < this.nearbyPolicesArray.length; g++) {
                            this.nearbyPolicesArray[g].remove();
                            this.reCenterMe();
                        }
                    }
                }
            }
        }
        else if (key === 'petrol') {
            if (this.plt.is('android')) {
                icurl = './assets/imgs/gas-pump.png';
            }
            else if (this.plt.is('ios')) {
                icurl = 'www/assets/imgs/gas-pump.png';
            }
            url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=gas_station&key=AIzaSyCmSXnoBBGekErvqBvu5N7dbeqdH1pHp7c";
            if (this.petrolButtonColor == '#6FA9CD') {
                this.petrolButtonColor = '#f4f4f4';
                this.petrolColor = '#6FA9CD';
                this.addNearbyMarkers(key, url, icurl);
            }
            else {
                if (this.petrolButtonColor == '#f4f4f4') {
                    this.petrolButtonColor = '#6FA9CD';
                    this.petrolColor = '#fff';
                    console.log("all nearby petrol markers: ", this.nearbyPetrolArray.length);
                    if (this.nearbyPetrolArray.length > 0) {
                        // this.allData.map
                        for (var g = 0; g < this.nearbyPetrolArray.length; g++) {
                            this.nearbyPetrolArray[g].remove();
                            this.reCenterMe();
                            // this.nearbyPetrolArray[g].clear();
                        }
                    }
                }
            }
        }
    };
    LiveSingleDevice.prototype.addNearbyMarkers = function (key, url, icurl) {
        var _this = this;
        console.log("google maps activity: ", url);
        this.apiCall.getSOSReportAPI(url).subscribe(function (resp) {
            console.log(resp.status);
            if (resp.status === 'OK') {
                console.log(resp.results);
                console.log(resp);
                var mapData = resp.results.map(function (d) {
                    return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
                });
                var bounds = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* LatLngBounds */](mapData);
                var that_1 = _this;
                for (var t = 0; t < resp.results.length; t++) {
                    _this.allData.map.addMarker({
                        title: resp.results[t].name,
                        position: {
                            lat: resp.results[t].geometry.location.lat,
                            lng: resp.results[t].geometry.location.lng
                        },
                        icon: {
                            url: icurl,
                            size: {
                                height: 35,
                                width: 35
                            }
                        }
                    }).then(function (mark) {
                        if (key === 'police') {
                            that_1.nearbyPolicesArray.push(mark);
                        }
                        else if (key === 'petrol') {
                            that_1.nearbyPetrolArray.push(mark);
                        }
                    });
                }
                _this.allData.map.moveCamera({
                    target: bounds
                });
                _this.allData.map.setPadding(20, 20, 20, 20);
                _this.zoomLevel = 15;
            }
            else if (resp.status === 'ZERO_RESULTS') {
                if (key === 'police') {
                    _this.showToastMsg('No nearby police stations found.');
                }
                else if (key === 'petrol') {
                    _this.showToastMsg('No nearby petrol pump found.');
                }
            }
            else if (resp.status === 'OVER_QUERY_LIMIT') {
                _this.showToastMsg('Query limit exeed. Please try after some time.');
            }
            else if (resp.status === 'REQUEST_DENIED') {
                _this.showToastMsg('Please check your API key.');
            }
            else if (resp.status === 'INVALID_REQUEST') {
                _this.showToastMsg('Invalid request. Please try again.');
            }
            else if (resp.status === 'UNKNOWN_ERROR') {
                _this.showToastMsg('An unknown error occured. Please try again.');
            }
        });
    };
    LiveSingleDevice.prototype.showToastMsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        }).present();
    };
    LiveSingleDevice.prototype.goBack = function () {
        // debugger
        if (this.navCtrl.canGoBack()) {
            this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'forward'
            });
        }
    };
    // goBack() {
    //   if (this.navCtrl.canGoBack()) {
    //     let options: NativeTransitionOptions = {
    //       direction: 'left',
    //       duration: 500,
    //       slowdownfactor: 3,
    //       slidePixels: 20,
    //       iosdelay: 100,
    //       androiddelay: 150,
    //       fixedPixelsTop: 0,
    //       fixedPixelsBottom: 60
    //     };
    //     this.nativePageTransitions.slide(options);
    //     this.navCtrl.pop();
    //   } else {
    //     let options: NativeTransitionOptions = {
    //       duration: 700
    //     };
    //     this.nativePageTransitions.fade(options);
    //     this.navCtrl.setRoot('DashboardPage');
    //   }
    // }
    LiveSingleDevice.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ngOnInit();
        this.ionViewDidEnter();
    };
    LiveSingleDevice.prototype.refreshMe1 = function () {
        this.ionViewDidLeave();
        // this.ngOnDestroy();
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        var paramData = this.navParams.get("device");
        this.socketInit(paramData);
    };
    LiveSingleDevice.prototype.reCenterMe = function () {
        this.allData.map.animateCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            zoom: 15,
            tilt: 30,
            // bearing: this.allData.map.getCameraBearing(),
            duration: 1800,
        }).then(function () {
        });
    };
    LiveSingleDevice.prototype.newMap = function () {
        var mapOptions = {
            camera: { zoom: 10 },
            gestures: {
                'rotate': false,
                'tilt': true,
                'scroll': true
            },
            gestureHandling: 'cooperative'
        };
        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
        // Environment.setBackgroundColor("black");
        return map;
    };
    LiveSingleDevice.prototype.isNight = function () {
        //Returns true if the time is between
        //7pm to 5am
        var time = new Date().getHours();
        return (time > 5 && time < 19) ? false : true;
    };
    LiveSingleDevice.prototype.checktripstat = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "user_trip/getLastTrip?device=" + this.deviceDeatils._id + "&status=Started&tripInfo=last_trip";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (data) {
            // debugger
            if (!data.message) {
                _this.allData.tripStat = data[0].trip_status;
                if (_this.allData.tripStat == 'Started') {
                    var sources = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](parseFloat(data[0].start_lat), parseFloat(data[0].start_long));
                    var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](parseFloat(data[0].end_lat), parseFloat(data[0].end_long));
                    _this.calcRoute(sources, dest);
                }
            }
        });
    };
    // old code using direction path
    // navigateFromCurrentLoc() {
    //   if (this.navigateButtonColor == '#6FA9CD') {
    //     this.navigateButtonColor = '#f4f4f4';
    //     this.navColor = '#6FA9CD';
    //     // this.apiCall.startLoading().present();
    //     this.navigateBtn = true;
    //     this.allData.map.getMyLocation().then((location) => {
    //       var myLocCoords = new LatLng(location.latLng.lat, location.latLng.lng);
    //       var vehLocCoords = new LatLng(this.recenterMeLat, this.recenterMeLng);
    //       this.calcRoute(myLocCoords, vehLocCoords);
    //     });
    //   } else {
    //     if (this.navigateButtonColor == '#f4f4f4') {
    //       this.navigateButtonColor = '#6FA9CD';
    //       this.navColor = '#fff';
    //       if (this.polyLines.length !== 0) {
    //         // this.polyLines.remove();
    //         for (var w = 0; w < this.polyLines.length; w++) {
    //           this.polyLines[w].remove();
    //         }
    //       }
    //       let that = this;
    //       localStorage.removeItem("livepagetravelDetailsObject");
    //       that.showDistDuration = false;
    //     }
    //   }
    // }
    // new code using launch navigator plugin
    LiveSingleDevice.prototype.navigateFromCurrentLoc = function () {
        window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_blank");
    };
    LiveSingleDevice.prototype.calcRoute = function (start, end) {
        this.allData.AIR_PORTS = [];
        var directionsService = new google.maps.DirectionsService();
        var that = this;
        var request = {
            origin: start,
            destination: end,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var path = new google.maps.MVCArray();
                for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
                    path.push(response.routes[0].overview_path[i]);
                    that.allData.AIR_PORTS.push({
                        lat: path.g[i].lat(), lng: path.g[i].lng()
                    });
                    // debugger
                    if (that.allData.AIR_PORTS.length > 1) {
                        if (!that.navigateBtn) {
                            that.allData.map.addMarker({
                                title: 'My Location',
                                position: start,
                                icon: 'green'
                            }).then(function (mark) {
                                that.marksArray.push(mark);
                            });
                            that.allData.map.addMarker({
                                title: 'Destination',
                                position: end,
                                icon: 'red'
                            }).then(function (mark) {
                                that.marksArray.push(mark);
                            });
                        }
                        // that.allData.map.setPadding(0, 30, 0, 55)
                        that.allData.map.addPolyline({
                            'points': that.allData.AIR_PORTS,
                            'color': '#3b3a3a',
                            'width': 4,
                            'geodesic': true,
                        }).then(function (poly) {
                            that.polyLines.push(poly);
                            that.getTravelDetails(start, end);
                            //   that.showBtn = true;
                            // let boundss = new LatLngBounds(that.allData.AIR_PORTS);
                            // that.allData.map.animateCamera({
                            //   target: boundss,
                            //   duration: 3000,
                            //   // padding: 5
                            // });
                            // that.zoomLevel = 16;
                        });
                    }
                }
            }
        });
    };
    LiveSingleDevice.prototype.someFunc = function () {
        // var runningDevices = [];
        var that = this;
        that._io = undefined;
        that._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.socketurl + '/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        that._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        if (that.userdetails.isSuperAdmin || that.userdetails.isDealer) {
        }
        else {
            that.socketInit(that.singleVehData);
        }
    };
    LiveSingleDevice.prototype.getTravelDetails = function (source, dest) {
        var _this = this;
        this.service = new google.maps.DistanceMatrixService();
        var that = this;
        this._id = setInterval(function () {
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
                    that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
                    console.log("expectation: ", that.expectation);
                    that.showDistDuration = true;
                }
                else {
                    clearInterval(_this._id);
                }
            }
        }, 3000);
        that.service.getDistanceMatrix({
            origins: [source],
            destinations: [dest],
            travelMode: 'DRIVING'
        }, that.callback);
        // that.apiCall.stopLoading();
    };
    LiveSingleDevice.prototype.callback = function (response, status) {
        var travelDetailsObject;
        if (status == 'OK') {
            var origins = response.originAddresses;
            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                for (var j = 0; j < results.length; j++) {
                    var element = results[j];
                    var distance = element.distance.text;
                    var duration = element.duration.text;
                    travelDetailsObject = {
                        distance: distance,
                        duration: duration
                    };
                }
            }
            localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
        }
    };
    LiveSingleDevice.prototype.diff = function (start, end) {
        start = start.split(":");
        end = end.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var endDate = new Date(0, 0, 0, end[0], end[1], 0);
        var diff = endDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        // If using time pickers with 24 hours format, add the below line get exact hours
        if (hours < 0)
            hours = hours + 24;
        return (hours <= 9 ? "0" : "") + hours + " hours " + (minutes <= 9 ? "0" : "") + minutes + " mins";
    };
    LiveSingleDevice.prototype.callendtripfunc = function (res) {
        var _this = this;
        var endtime = __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(), 'DD/MM/YYY').format('hh:mm');
        var starttime = __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(res.doc.start_time), 'DD/MM/YYYY').format('hh:mm');
        var duration = this.diff(starttime, endtime);
        var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](res.doc.start_lat, res.doc.start_long);
        var dest2 = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](res.doc.end_lat, res.doc.end_long);
        var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, dest2); // in meters
        var bUrl = this.apiCall.mainUrl + "user_trip/updatePlantrip";
        var payload = {
            "user": res.doc.user,
            "device": res.doc.device,
            "start_loc": {
                "lat": res.doc.start_lat,
                "long": res.doc.start_long
            },
            "end_loc": {
                "lat": res.doc.end_lat,
                "long": res.doc.end_long,
            },
            "duration": duration,
            "distance": distance,
            "trip_status": "completed",
            "trip_id": res.doc._id,
            "end_time": new Date().toISOString()
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(bUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("tri end data: ", data);
            // alert("trip ended succesfully");
            var toast = _this.toastCtrl.create({
                message: 'Trip has ended succesfully.',
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
            var that = _this;
            // debugger
            that.allData.tripStat = 'completed';
            if (that.polyLines.length > 0) {
                for (var t = 0; t < that.polyLines.length; t++) {
                    that.polyLines[t].remove();
                }
            }
            if (that.marksArray.length > 0) {
                for (var e = 0; e < that.marksArray.length; e++) {
                    that.marksArray[e].remove();
                }
            }
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                localStorage.removeItem("livepagetravelDetailsObject");
            }
            _this.checktripstat();
        });
    };
    LiveSingleDevice.prototype.endTrip = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.translate.instant('Confirm'),
            message: this.translate.instant('Do you want to end the trip?'),
            buttons: [
                {
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        _this.storage.get("TRIPDATA").then(function (res) {
                            if (res) {
                                _this.callendtripfunc(res);
                            }
                        });
                    }
                },
                {
                    text: this.translate.instant('Back')
                }
            ]
        });
        alert.present();
    };
    // onClickMap(maptype) {
    //   let that = this;
    //   if (maptype == 'SATELLITE') {
    //     that.allData.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
    //   } else {
    //     if (maptype == 'TERRAIN') {
    //       that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
    //     } else {
    //       if (maptype == 'NORMAL') {
    //         that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
    //       } else {
    //         if (maptype == 'HYBRID') {
    //           that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
    //         }
    //       }
    //     }
    //   }
    // }
    LiveSingleDevice.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
            // this.ngOnDestroy();
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
            // this.someFunc();
        }
        else {
            if (maptype == 'TERRAIN') {
                // this.ngOnDestroy();
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
                // this.someFunc();
            }
            else {
                if (maptype == 'NORMAL') {
                    // this.ngOnDestroy();
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                    // this.someFunc();
                }
            }
        }
    };
    LiveSingleDevice.prototype.createTrip = function () {
        console.log("create trip data: ", this.deviceDeatils);
        this.navCtrl.push('CreateTripPage', {
            'paramData': this.deviceDeatils,
        });
    };
    LiveSingleDevice.prototype.trafficFunc = function () {
        var that = this;
        that.isEnabled = !that.isEnabled;
        if (that.isEnabled == true) {
            that.allData.map.setTrafficEnabled(true);
        }
        else {
            that.allData.map.setTrafficEnabled(false);
        }
    };
    LiveSingleDevice.prototype.shareLive = function () {
        var that = this;
        that.showActionSheet = false;
        // that.drawerHidden = false;
        that.drawerState1 = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        that.showFooter = true;
    };
    LiveSingleDevice.prototype.sharedevices = function (param) {
        var that = this;
        if (param == '15mins') {
            that.condition = 'gpsc';
            that.condition1 = 'light';
            that.condition2 = 'light';
            that.tttime = 15;
            // that.tttime  = (15 * 60000); //for miliseconds
        }
        else {
            if (param == '1hour') {
                that.condition1 = 'gpsc';
                that.condition = 'light';
                that.condition2 = 'light';
                that.tttime = 60;
                // that.tttime  = (1 * 3600000); //for miliseconds
            }
            else {
                if (param == '8hours') {
                    that.condition2 = 'gpsc';
                    that.condition = 'light';
                    that.condition1 = 'light';
                    that.tttime = (8 * 60);
                    // that.tttime  = (8 * 3600000);
                }
            }
        }
    };
    LiveSingleDevice.prototype.shareLivetemp = function () {
        var _this = this;
        var that = this;
        if (that.tttime == undefined) {
            that.tttime = 15;
        }
        var data = {
            id: that.liveDataShare._id,
            imei: that.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: that.tttime // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LiveSingleDevice.prototype.liveShare = function () {
        var that = this;
        var link = 'https://www.oneqlik.in/' + "share/liveShare?t=" + that.resToken;
        __WEBPACK_IMPORTED_MODULE_14_tinyurl__["shorten"](link).then(function (res) {
            // alert("tinyurl: " + res);
            that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);
        });
        // that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
        that.showActionSheet = true;
        that.showFooter = false;
        that.tttime = undefined;
    };
    LiveSingleDevice.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LiveSingleDevice.prototype.callObjFunc = function (d) {
        var _this = this;
        var that = this;
        var _bUrl = that.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_bUrl)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log("updated device object=> " + resp);
            if (!resp) {
                return;
            }
            else {
                that.deviceDeatils = resp;
            }
        });
    };
    LiveSingleDevice.prototype.settings = function () {
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            that.callObjFunc(that.deviceDeatils);
        });
    };
    LiveSingleDevice.prototype.getAddress = function (coordinates) {
        var _this = this;
        var that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        var tempcord = {
            "lat": coordinates.lat,
            "long": coordinates.long
        };
        this.apiCall.getAddress(tempcord)
            .subscribe(function (res) {
            // console.log("test");
            // console.log("address result", res);
            if (res.message == "Address not found in databse") {
                _this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
                    .then(function (res) {
                    var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                    that.saveAddressToServer(str, coordinates.lat, coordinates.long);
                    that.address = str;
                    console.log("inside", that.address);
                });
            }
            else {
                that.address = res.address;
            }
            // console.log(that.address);
        });
    };
    LiveSingleDevice.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            // console.log("check if address is stored in db or not? ", respData)
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    LiveSingleDevice.prototype.weDidntGetPing = function (data) {
        console.log("I came here in weDidntGetPing function");
        var that = this;
        if (data._id != undefined && data.last_location != undefined) {
            that.otherValues(data);
            if (data.last_location != undefined) {
                console.log("we did not get pung: ", that.getIconUrl(data));
                // debugger
                // if (localStorage.getItem('gotPing') === null) {
                that.allData.map.addMarker({
                    title: data.Device_Name,
                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                    icon: {
                        url: that.getIconUrl(data),
                        size: {
                            height: 40,
                            width: 20
                        }
                    },
                }).then(function (marker) {
                    // debugger
                    that.temporaryMarker = marker;
                    // that.allData[key].mark = marker;
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        that.liveVehicleName = data.Device_Name;
                        that.showActionSheet = true;
                        that.showaddpoibtn = true;
                        that.getAddressTitle(marker);
                    });
                    that.getAddress(data.last_location);
                    that.socketInit(data);
                });
                // }
            }
        }
        else if (data._id != undefined && (data.last_lat !== undefined && data.last_lng !== undefined)) {
            that.otherValues(data);
            // if (data.last_location != undefined) {
            console.log("we did not get pung: ", that.getIconUrl(data));
            // debugger
            // if (localStorage.getItem('gotPing') === null) {
            that.allData.map.addMarker({
                title: data.Device_Name,
                position: { lat: data.last_lat, lng: data.last_lng },
                icon: {
                    url: that.getIconUrl(data),
                    size: {
                        height: 40,
                        width: 20
                    }
                },
            }).then(function (marker) {
                // debugger
                that.temporaryMarker = marker;
                // that.allData[key].mark = marker;
                marker.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                    .subscribe(function (e) {
                    that.liveVehicleName = data.Device_Name;
                    that.showActionSheet = true;
                    that.showaddpoibtn = true;
                    that.getAddressTitle(marker);
                });
                var last_location = {
                    lat: data.last_lat,
                    long: data.last_lng
                };
                that.getAddress(last_location);
                that.socketInit(data);
            });
            // }
            // }
        }
    };
    // socketInit(pdata, center = false) {
    //   this._io.emit('acc', pdata.Device_ID);
    //   this.socketChnl.push(pdata.Device_ID + 'acc');
    //   let that = this;
    //   this._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
    //     if (d4 != undefined)
    //       console.log("ping data: => ", d4);
    //     (function (data) {
    //       if (data == undefined) {
    //         return;
    //       }
    //       localStorage.setItem("gotPing", "true");
    //       // that.reCenterMe();
    //       localStorage.setItem("pdata", JSON.stringify(data))
    //       if (data._id != undefined && data.last_location != undefined) {
    //         var key = data._id;
    //         that.impkey = data._id;
    //         that.deviceDeatils = data;
    //         that.otherValues(data);
    //         var strStr;
    //         // debugger
    //         if (that.allData[key]) {
    //           strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
    //           that.socketSwitch[key] = data;
    //           console.log("check mark is avalible or not? ", that.allData[key].mark)
    //           if (that.allData[key].mark !== undefined) {
    //             that.allData[key].mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
    //               // alert('clicked');
    //               setTimeout(() => {
    //                 that.allData[key].mark.hideInfoWindow();
    //               }, 2000)
    //             });
    //             // that.allData[key].mark.setIcon(that.getIconUrl(data));
    //             var temp = _.cloneDeep(that.allData[key].poly[1]);
    //             that.allData[key].poly[0] = _.cloneDeep(temp);
    //             that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
    //             //////////////////////////////////////// test code
    //             let delay;
    //             let speed;
    //             let localTarget = localStorage.getItem('liveTrackingTarget');
    //             if (localTarget !== null) {
    //               let markLat = JSON.parse(localStorage.getItem("liveTrackingLat"));
    //               let markLng = JSON.parse(localStorage.getItem("liveTrackingLng"));
    //               if ((markLat !== that.allData[key].poly[0].lat) && (markLng !== that.allData[key].poly[0].lng)) {
    //                 console.log("marker is not reached at destination")
    //                 delay = 8;
    //                 if (data.last_speed === '0') {
    //                   speed = 30;
    //                   let sls = localStorage.getItem("some_last_speed");;
    //                   that.some_last_speed = Number(sls);
    //                   that.vehicle_speed = (that.some_last_speed ? that.some_last_speed : 0);
    //                 } else {
    //                   speed = Number(data.last_speed) + 10;
    //                   that.some_last_speed = data.last_speed;
    //                   localStorage.setItem("some_last_speed", data.last_speed);
    //                   that.vehicle_speed = (that.some_last_speed ? that.some_last_speed : 0);
    //                 }
    //                 let iconUrl;
    //                 if (that.plt.is('ios')) {
    //                   iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
    //                 } else if (that.plt.is('android')) {
    //                   iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
    //                 }
    //                 that.allData[key].mark.setIcon(iconUrl);
    //                 console.log("data status: 1 abnormal", data.status)
    //                 console.log("data last speed: ", data.last_speed)
    //                 that.allData.map.animateCamera({
    //                   target: { lat: markLat, lng: markLng },
    //                   tilt: 30,
    //                   duration: 1800,
    //                 })
    //                 console.log("speed check: ", speed)
    //               } else {
    //                 delay = 10;
    //                 that.allData[key].mark.setSnippet(strStr);
    //                 // that.allData[key].mark.setIcon(that.getIconUrl(data));
    //                 let iconUrl;
    //                 if (that.plt.is('ios')) {
    //                   iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
    //                 } else if (that.plt.is('android')) {
    //                   iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
    //                 }
    //                 that.allData[key].mark.setIcon(iconUrl);
    //                 that.some_last_speed = data.last_speed;
    //                 that.vehicle_speed = (that.some_last_speed ? that.some_last_speed : 0);
    //                 localStorage.setItem("some_last_speed", data.last_speed);
    //                 speed = data.status == "RUNNING" ? data.last_speed : 0;
    //                 console.log("data status: 2 abnormal but normal", data.status)
    //                 console.log("data last speed: ", data.last_speed)
    //                 // that.allData.map.animateCamera({
    //                 //   target: { lat: markLat, lng: markLng },
    //                 //   tilt: 30,
    //                 //   duration: 1800
    //                 // })
    //                 that.reCenterMe();
    //               }
    //             } else {
    //               delay = 10;
    //               that.allData[key].mark.setSnippet(strStr);
    //               that.allData[key].mark.setIcon(that.getIconUrl(data));
    //               speed = data.status == "RUNNING" ? data.last_speed : 0;
    //               that.some_last_speed = data.last_speed;
    //               that.vehicle_speed = (that.some_last_speed ? that.some_last_speed : 0);
    //               that.reCenterMe();
    //               console.log("data status: 2 normal", data.status);
    //               console.log("data last speed: ", data.last_speed)
    //             }
    //             console.log("check delay :", delay)
    //             //////////////////////////////////////// test code
    //             // var speed = data.status == "RUNNING" ? data.last_speed : 0;
    //             that.getAddress(data.last_location);
    //             that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), delay, center, data._id, that);
    //           } else {
    //             return;
    //           }
    //         }
    //         else {
    //           that.allData[key] = {};
    //           that.socketSwitch[key] = data;
    //           that.allData[key].poly = [];
    //           that.vehicle_speed = (data.last_speed ? data.last_speed : 0);
    //           if (data.sec_last_location.lat !== null && data.sec_last_location.long !== null) {
    //             that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
    //           } else {
    //             that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
    //           }
    //           that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
    //           if (data.last_location != undefined) {
    //             console.log("we got pung: ", that.getIconUrl(data));
    //             // let position: ILatLng = {
    //             //   lat: that.allData[key].poly[0].lat,
    //             //   lng: that.allData[key].poly[0].lng
    //             // };
    //             // debugger
    //             if (that.temporaryMarker !== undefined) {
    //               that.temporaryMarker.setTitle(data.Device_Name);
    //               that.temporaryMarker.setPosition(that.allData[key].poly[0]);
    //               that.allData.map.animateCamera({
    //                 target: {
    //                   lat: that.allData[key].poly[0].lat,
    //                   lng: that.allData[key].poly[0].lng
    //                 },
    //                 duration: 1800
    //               })
    //               that.allData[key].mark = that.temporaryMarker;
    //               // that.temporaryMarker.remove();
    //               that.allData[key].mark.addEventListener(GoogleMapsEvent.MARKER_CLICK)
    //                 .subscribe(e => {
    //                   that.liveVehicleName = data.Device_Name;
    //                   that.showActionSheet = true;
    //                   that.showaddpoibtn = true;
    //                   that.getAddressTitle(that.allData[key].mark);
    //                 });
    //               that.getAddress(data.last_location);
    //               var speed = data.status == "RUNNING" ? data.last_speed : 0;
    //               that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
    //             } else {
    //               that.allData.map.addMarker({
    //                 title: data.Device_Name,
    //                 position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
    //                 icon: {
    //                   url: that.getIconUrl(data),
    //                   size: {
    //                     height: 40,
    //                     width: 20
    //                   }
    //                 },
    //               }).then((marker: Marker) => {
    //                 that.allData[key].mark = marker;
    //                 marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
    //                   .subscribe(e => {
    //                     that.liveVehicleName = data.Device_Name;
    //                     that.showActionSheet = true;
    //                     that.showaddpoibtn = true;
    //                     that.getAddressTitle(marker);
    //                   });
    //                 that.getAddress(data.last_location);
    //                 var speed = data.status == "RUNNING" ? data.last_speed : 0;
    //                 that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
    //               });
    //             }
    //             ///////////////
    //             // that.addCircleWithAnimation(that.allData[data._id].poly[0].lat, that.allData[data._id].poly[0].lng);
    //             //////////////
    //           }
    //         }
    //       }
    //     })(d4)
    //   })
    // }
    LiveSingleDevice.prototype.socketInit = function (pdata, center) {
        var _this = this;
        if (center === void 0) { center = false; }
        this.allData.allKey = [];
        // let that = this;
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            console.log("live data", d1, d2, d3, d4);
            var that = _this;
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    //  console.log( that.socketSwitch);
                    if (data._id != undefined && data.last_location != undefined) {
                        var key = data._id;
                        var ic_1 = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.icons[data.iconType]);
                        if (!ic_1) {
                            console.warn(data.Device_ID + " has no iconType", data.iconType);
                            return;
                        }
                        ic_1.path = null;
                        // ic.url = 'https://www.oneqlik.in/images/' + data.status.toLowerCase() + data.iconType + '.png#' + data._id;
                        ic_1.url = that.getIconUrl(data);
                        that.otherValues(data);
                        that.reCenterMe();
                        if (that.allData[key]) {
                            that.socketSwitch[key] = data;
                            if (that.allData[key].mark !== undefined) {
                                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                                that.allData[key].mark.setIcon(that.getIconUrl(data));
                                var temp = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.allData[key].poly[1]);
                                that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](temp);
                                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                                that.getAddress(data.last_location);
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                that.liveTrack(that.allData.map, that.allData[key].mark, ic_1.url, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                            }
                        }
                        else {
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData.allKey.push(key);
                            that.allData[key].poly = [];
                            if (data.sec_last_location) {
                                that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                            }
                            else {
                                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            }
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                if (that.temporaryMarker !== undefined) {
                                    that.temporaryMarker.setTitle(data.Device_Name);
                                    that.temporaryMarker.setPosition(that.allData[key].poly[0]);
                                    that.allData.map.animateCamera({
                                        target: {
                                            lat: that.allData[key].poly[0].lat,
                                            lng: that.allData[key].poly[0].lng
                                        },
                                        duration: 1800
                                    });
                                    that.allData[key].mark = that.temporaryMarker;
                                    // that.temporaryMarker.remove();
                                    that.allData[key].mark.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                        .subscribe(function (e) {
                                        that.liveVehicleName = data.Device_Name;
                                        that.showActionSheet = true;
                                        that.showaddpoibtn = true;
                                        that.getAddressTitle(that.allData[key].mark);
                                    });
                                    that.getAddress(data.last_location);
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrack(that.allData.map, that.allData[key].mark, ic_1.url, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                }
                                else {
                                    that.allData.map.addMarker({
                                        title: data.Device_Name,
                                        position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                        icon: {
                                            url: that.getIconUrl(data),
                                            size: {
                                                height: 40,
                                                width: 20
                                            }
                                        },
                                    }).then(function (marker) {
                                        that.allData[key].mark = marker;
                                        marker.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                            .subscribe(function (e) {
                                            that.liveVehicleName = data.Device_Name;
                                            that.showActionSheet = true;
                                            that.showaddpoibtn = true;
                                            that.getAddressTitle(marker);
                                        });
                                        that.getAddress(data.last_location);
                                        var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                        that.liveTrack(that.allData.map, that.allData[key].mark, ic_1.url, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                    });
                                }
                                // var infobox = new InfoBox({
                                //   content: "<div class='infobox' style='padding:2px'><small><b style='color:" + that.getClr(data.status) + "'>" + data.Device_Name + "</b></small></div>",
                                //   disableAutoPan: false,
                                //   maxWidth: 150,
                                //   alignBottom: true,
                                //   pixelOffset: new google.maps.Size(-25, -20),
                                //   zIndex: null,
                                //   boxStyle: {
                                //     opacity: 1,
                                //     zIndex: 999,
                                //     width: "auto",
                                //     padding: "2px"
                                //   },
                                //   //closeBoxMargin: "12px 4px 2px 2px",
                                //   closeBoxURL: "",
                                //   infoBoxClearance: new google.maps.Size(1, 1)
                                // });
                                // that.data[key].infobox = infobox;
                                // infobox.open(that.data.map, that.data[key].mark)
                                // that.info(that.data[key].mark, function (e) {
                                //   that.data.map.setCenter(e.latLng);
                                //   that.socketSwitch.selected = data._id;
                                // })
                            }
                            // var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            // that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                        }
                    }
                })(d4);
        });
    };
    LiveSingleDevice.prototype.addCircleWithAnimation = function (lat, lng) {
        var that = this;
        var _radius = 500;
        var rMin = _radius * 1 / 9;
        var rMax = _radius;
        var direction = 1;
        var GOOGLE = {
            "lat": lat,
            "lng": lng
        };
        var circleOption = {
            'center': GOOGLE,
            'radius': 500,
            'fillColor': 'rgb(216, 6, 34)',
            'fillOpacity': 0.6,
            'strokeColor': '#950417',
            'strokeOpacity': 1,
            'strokeWidth': 0.5
        };
        // Add circle
        var circle = that.allData.map.addCircleSync(circleOption);
        that.circleObj = circle;
        that.circleTimer = setInterval(function () {
            var radius = circle.getRadius();
            if ((radius > rMax) || (radius < rMin)) {
                direction *= -1;
            }
            var _par = (radius / _radius) - 0.1;
            var opacity = 0.4 * _par;
            var colorString = that.RGBAToHexA(216, 6, 34, opacity);
            circle.setFillColor(colorString);
            circle.setRadius(radius + direction * 10);
        }, 30);
    };
    LiveSingleDevice.prototype.RGBAToHexA = function (r, g, b, a) {
        r = r.toString(16);
        g = g.toString(16);
        b = b.toString(16);
        a = Math.round(a * 255).toString(16);
        if (r.length == 1)
            r = "0" + r;
        if (g.length == 1)
            g = "0" + g;
        if (b.length == 1)
            b = "0" + b;
        if (a.length == 1)
            a = "0" + a;
        return "#" + r + g + b + a;
    };
    LiveSingleDevice.prototype.getAddressTitle = function (marker) {
        this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            marker.setSnippet(str);
        });
    };
    LiveSingleDevice.prototype.addPOI = function () {
        var _this = this;
        var that = this;
        var modal = this.modalCtrl.create(PoiPage, {
            param1: that.allData[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            var that = _this;
            that.showaddpoibtn = false;
        });
        modal.present();
    };
    /////////// Anjali code ////////
    LiveSingleDevice.prototype.getIconUrl = function (data) {
        var that = this;
        var iconUrl;
        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0)) {
            if (that.plt.is('ios')) {
                iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            }
            else if (that.plt.is('android')) {
                iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
        }
        else {
            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0)) {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
            }
            else {
                if ((data.status.toLowerCase() === 'stopped') && Number(data.last_speed) > 0) {
                    if (that.plt.is('ios')) {
                        iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                    }
                    else if (that.plt.is('android')) {
                        iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
                    }
                }
                else {
                    var stricon;
                    if (data.status.toLowerCase() === 'out of reach') {
                        stricon = "outofreach";
                        if (that.plt.is('ios')) {
                            iconUrl = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                        }
                        else if (that.plt.is('android')) {
                            iconUrl = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                        }
                    }
                    else {
                        if (that.plt.is('ios')) {
                            iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                        }
                        else if (that.plt.is('android')) {
                            iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                        }
                    }
                }
            }
        }
        console.log("icon url: ", iconUrl);
        return iconUrl;
    };
    LiveSingleDevice.prototype.otherValues = function (data) {
        var that = this;
        that.vehicle_speed = (data.last_speed ? data.last_speed : 0);
        // that.vehicle_speed = (that.some_last_speed ? that.some_last_speed : 0);
        that.battery_percent = (data.battery_percent ? data.battery_percent : 0);
        that.todays_odo = (data.today_odo ? data.today_odo : 0);
        that.total_odo = (data.total_odo ? data.total_odo : 0);
        if (that.userdetails.fuel_unit == 'LITRE') {
            that.fuel = (data.currentFuel ? data.currentFuel : null);
        }
        else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
            that.fuel = (data.fuel_percent ? data.fuel_percent : null);
        }
        else {
            that.fuel = (data.currentFuel ? data.currentFuel : null);
        }
        that.voltage = (data.battery ? data.battery : 0);
        if (data.last_location) {
            that.recenterMeLat = data.last_location.lat;
            that.recenterMeLng = data.last_location.long;
            that.getAddress(data.last_location);
        }
        else {
            if (data.last_lat !== undefined && data.last_lng !== undefined) {
                var location_1 = {
                    lat: data.last_lat,
                    long: data.last_lng
                };
                that.recenterMeLat = data.last_lat;
                that.recenterMeLng = data.last_lng;
                that.getAddress(location_1);
            }
        }
        that.last_ping_on = (data.last_ping_on ? data.last_ping_on : 'N/A');
        var tempvar = new Date(data.lastStoppedAt);
        if (data.lastStoppedAt != null) {
            var fd = tempvar.getTime();
            var td = new Date().getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            that.lastStoppedAt = rhours + ':' + rminutes;
        }
        else {
            that.lastStoppedAt = '00' + ':' + '00';
        }
        that.distFromLastStop = data.distFromLastStop;
        if (!isNaN(data.timeAtLastStop)) {
            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
        }
        else {
            that.timeAtLastStop = '00:00:00';
        }
        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
        that.last_ACC = data.last_ACC;
        that.acModel = data.ac;
        that.currentFuel = data.currentFuel;
        that.power = data.power;
        that.gpsTracking = data.gpsTracking;
        that.tempreture = data.temp;
        that.door = data.door;
    };
    LiveSingleDevice.prototype.ioReceiveLocation = function (pdata) {
        var that = this;
        if (that.fraction >= 1) {
            if (that.intevalId) {
                clearInterval(that.intevalId);
            }
            that.setPositionAnimate(0);
        }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            console.log("ioReceiveLocation ping: ", d4);
            var m = __WEBPACK_IMPORTED_MODULE_5_moment__();
            var ddate1 = m.milliseconds() + 1000 * (m.seconds() + 60 * (m.minutes() + 60 * m.hours()));
            console.log("date in miliseconds: ", ddate1);
            that.zoomLevel = 16;
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    if (data.last_location) {
                        that.latLngArray.push(data.last_location);
                    }
                    that.otherValues(data);
                    // that.zoomLevel = that.allData.map.getCameraZoom();
                    if (that.devices.indexOf(data._id) === -1) {
                        that.devices.push(data._id);
                        var indice = that.devices.indexOf(data._id);
                        that.createMarker(data, indice, that.getIconUrl(data));
                    }
                    else {
                        var indice = that.devices.indexOf(data._id);
                        that.markersArray[indice].setIcon(that.getIconUrl(data));
                        that.setPositionAnimate(indice);
                    }
                })(d4);
        });
    };
    LiveSingleDevice.prototype.createMarker = function (location, indice, iconUrl) {
        var _this = this;
        var latlng = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](location.last_location.lat, location.last_location.long);
        var markerOptions = {
            title: 'Ionic',
            animation: 'BOUNCE',
            position: latlng,
            icon: iconUrl
        };
        this.allData.map.addMarker(markerOptions).then(function (marker) {
            _this.markersArray[indice] = marker;
            console.log('merkers array list: ', _this.markersArray);
        });
    };
    LiveSingleDevice.prototype.setPositionAnimate = function (indice) {
        var that = this;
        if (that.latLngArray.length >= 2) {
            if (that.fraction >= 1) {
                console.log("when fraction 1: ", that.latLngArray.length);
                clearInterval(that.intevalId);
                if (that.latLngArray.length === 2) {
                    // this.speed = location.last_location.last_speed * 3.6;
                    var nyc_1 = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
                    var london_1 = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
                    var bearing_1 = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(nyc_1, london_1);
                    var points_1 = [nyc_1, london_1];
                    that.latLngArray.shift();
                    this.allData.map.addPolyline({
                        "points": points_1,
                        "geodesic": true,
                        "width": 3
                    });
                    that.fraction = 0;
                    var GOOGLE = london_1;
                    this.allData.map.animateCamera({
                        target: GOOGLE,
                        bearing: bearing_1,
                        tilt: 60,
                        duration: 2500,
                        zoom: that.zoomLevel
                    });
                }
                else if (that.latLngArray.length > 2) {
                    var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
                    var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
                    var bearing = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(nyc, london);
                    var points = [nyc, london];
                    that.latLngArray.shift();
                    console.log("After shift from 1: ", that.latLngArray.length);
                    this.allData.map.addPolyline({
                        "points": points,
                        "geodesic": true,
                        "width": 3
                    });
                    that.fraction = 0;
                    var GOOGLE = london;
                    this.allData.map.animateCamera({
                        target: GOOGLE,
                        bearing: bearing,
                        tilt: 60,
                        duration: 2500,
                        zoom: that.zoomLevel
                    });
                }
            }
            else if (that.fraction === 0) {
                console.log("when fraction 0: ", that.latLngArray.length);
                clearInterval(that.intevalId);
                if (that.latLngArray.length === 2) {
                    var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
                    var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
                    var bearing = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(nyc, london);
                    var points = [nyc, london];
                    that.latLngArray.shift();
                    this.allData.map.addPolyline({
                        "points": points,
                        "geodesic": true,
                        "width": 3
                    });
                    var GOOGLE = london;
                    this.allData.map.animateCamera({
                        target: GOOGLE,
                        bearing: bearing,
                        tilt: 60,
                        duration: 2500,
                        zoom: that.zoomLevel
                    });
                    console.log("After shift from 0: ", that.latLngArray.length);
                }
                else if (that.latLngArray.length > 2) {
                    var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
                    var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
                    var bearing = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(nyc, london);
                    var points = [nyc, london];
                    that.latLngArray.shift();
                    this.allData.map.addPolyline({
                        "points": points,
                        "geodesic": true,
                        "width": 3
                    });
                    var GOOGLE = london;
                    this.allData.map.animateCamera({
                        target: GOOGLE,
                        bearing: bearing,
                        tilt: 60,
                        duration: 2500,
                        zoom: that.zoomLevel
                    });
                }
            }
            if (that.fraction <= 0) {
                that.intevalId = setInterval(function () {
                    if (that.fraction <= 1) {
                        that.fraction += 0.01 * that.direction;
                    }
                    that.markersArray[indice].setPosition(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].interpolate(nyc, london, that.fraction));
                }, that.vehicle_speed);
            }
        }
        else {
            return;
        }
    };
    /////////// Anjali code ends ///////
    // target: any = 0;
    // liveTrack(map, mark, coords, speed, delay, center, id, that) {
    //   // let localTarget = localStorage.getItem('liveTrackingTarget');
    //   let target;
    //   // if (localTarget !== null) {
    //   //   target = JSON.parse(localTarget);
    //   // } else {
    //   target = 0;
    //   // }
    //   debugger
    //   console.log('on livetrack function entered')
    //   clearTimeout(that.ongoingGoToPoint[id]);
    //   clearTimeout(that.ongoingMoveMarker[id]);
    //   // if (center) {
    //   //   map.setCameraTarget(coords[0]);
    //   // }
    //   function _goToPoint() {
    //     console.log("coords length: ", coords.length);
    //     // console.log("coords length: ", JSON.stringify(coords))
    //     console.log("target length :" + target)
    //     if (target > coords.length) {
    //       // localStorage.setItem("markerLatestPosition", coords[0])
    //       return;
    //     }
    //     //////////////////////////////////////////////// for testing
    //     if (target !== coords.length) {
    //       localStorage.setItem("liveTrackingTarget", JSON.stringify(target));
    //       localStorage.setItem("liveTrackingLat", JSON.stringify(mark.getPosition().lat))
    //       localStorage.setItem("liveTrackingLng", JSON.stringify(mark.getPosition().lng))
    //       // var lat = mark.getPosition().lat;
    //       // var lng = mark.getPosition().lng;
    //     } else {
    //       localStorage.removeItem("liveTrackingTarget");
    //       localStorage.removeItem("liveTrackingLat");
    //       localStorage.removeItem("liveTrackingLng");
    //       // localStorage.setItem("markerLatestPosition", coords[0])
    //     }
    //     // map.addMarker({
    //     //   title: 'a',
    //     //   position: {
    //     //     lat: coords[0].lat,
    //     //     lng: coords[0].lng
    //     //   },
    //     //   icon: 'green'
    //     // })
    //     // map.addMarker({
    //     //   title: 'b',
    //     //   position: {
    //     //     lat: coords[1].lat,
    //     //     lng: coords[1].lng
    //     //   },
    //     //   icon: 'red'
    //     // })
    //     //////////////////////////////////////////////// for testing
    //     var lat = mark.getPosition().lat;
    //     var lng = mark.getPosition().lng;
    //     // if(coords[target + 1] !== undefined) {
    //     //   delay = 8;
    //     // } else {
    //     //   delay = 10;
    //     // }
    //     var step = (speed * 1000 * delay) / 3600000; // in meters
    //     if (coords[target] == undefined)
    //       return;
    //     var dest = new LatLng(coords[target].lat, coords[target].lng);
    //     var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
    //     var numStep = distance / step;
    //     var i = 0;
    //     var deltaLat = (coords[target].lat - lat) / numStep;
    //     var deltaLng = (coords[target].lng - lng) / numStep;
    //     function changeMarker(mark, deg) {
    //       mark.setRotation(deg);
    //     }
    //     function _moveMarker() {
    //       // debugger
    //       lat += deltaLat;
    //       lng += deltaLng;
    //       i += step;
    //       var head;
    //       if (i < distance) {
    //         head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
    //         head = head < 0 ? (360 + head) : head;
    //         if ((head != 0) || (head == NaN)) {
    //           changeMarker(mark, head);
    //         }
    //         if (that.mapTrail) {
    //           that.showTrail(lat, lng, map);
    //         } else {
    //           // console.log(that.allData.polylineID)
    //           // console.log(that.allData.polylineID.length)
    //           if (that.allData.polylineID) {
    //             that.tempArray = [];
    //             that.allData.polylineID.remove();
    //           }
    //         }
    //         // map.moveCamera({
    //         //   target: new LatLng(lat, lng)
    //         // });
    //         // map.setCameraTarget(new LatLng(lat, lng));
    //         that.latlngCenter = new LatLng(lat, lng);
    //         mark.setPosition(new LatLng(lat, lng));
    //         /////////
    //         // that.circleObj.setCenter(new LatLng(lat, lng))
    //         ////////
    //         that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
    //       }
    //       else {
    //         head = Spherical.computeHeading(mark.getPosition(), dest);
    //         head = head < 0 ? (360 + head) : head;
    //         if ((head != 0) || (head == NaN)) {
    //           changeMarker(mark, head);
    //         }
    //         if (that.mapTrail) {
    //           that.showTrail(dest.lat, dest.lng, map);
    //         }
    //         else {
    //           if (that.allData.polylineID) {
    //             that.tempArray = [];
    //             that.allData.polylineID.remove();
    //           }
    //         }
    //         // map.moveCamera({
    //         //   target: {
    //         //     lat: dest.lat,
    //         //     lng: dest.lng
    //         //   }
    //         // });
    //         // map.setCameraTarget(dest);
    //         that.latlngCenter = dest;
    //         mark.setPosition(dest);
    //         /////////
    //         // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
    //         ////////
    //         // that.target++;
    //         target++;
    //         that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
    //       }
    //     }
    //     _moveMarker();
    //   }
    //   _goToPoint();
    // }
    LiveSingleDevice.prototype.liveTrack = function (map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCenter(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    var head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        mark.setRotation(head);
                    }
                    // mark.setIcon(icons);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    var head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        mark.setRotation(head);
                    }
                    // mark.setIcon(icons);
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LiveSingleDevice.prototype.zoomin = function () {
        var that = this;
        // that.ngOnDestroy();
        that.allData.map.moveCameraZoomIn();
        // setTimeout(() => {
        //   that.someFunc();
        // }, 1000);
    };
    LiveSingleDevice.prototype.zoomout = function () {
        var that = this;
        // that.ngOnDestroy();
        that.allData.map.moveCameraZoomOut();
        // setTimeout(() => {
        //   that.someFunc();
        // }, 1000);
    };
    LiveSingleDevice.prototype.temp = function (data) {
        var that = this;
        that.showShareBtn = true;
        that.liveDataShare = data;
        that.onClickShow = false;
        if (that.allData.map != undefined) {
            that.allData.map.remove();
        }
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that.allData = {};
        that.socketChnl = [];
        that.socketSwitch = {};
        var style = [];
        if (localStorage.getItem('NightMode') != null) {
            if (localStorage.getItem('NightMode') === 'ON') {
                //Change Style to night between 7pm to 5am
                if (this.isNight()) {
                    style = __WEBPACK_IMPORTED_MODULE_13__live_map_style_model__["a" /* mapStyle */];
                }
            }
        }
        var mapOptions = {
            backgroundColor: 'white',
            controls: {
                compass: false,
                zoom: false,
                myLocation: true,
                myLocationButton: false,
            },
            gestures: {
                'rotate': false,
                'tilt': true,
                'scroll': true
            },
            gestureHandling: 'cooperative',
            camera: {
                target: { lat: 20.5937, lng: 78.9629 },
                zoom: this.zoomLevel,
            },
            mapType: that.mapKey,
            styles: style
        };
        if (data) {
            if (data.last_location) {
                if (that.plt.is('android')) {
                    // let mapOptions = {
                    //   backgroundColor: 'white',
                    //   controls: {
                    //     compass: false,
                    //     zoom: false,
                    //     myLocation: true,
                    //     myLocationButton: false,
                    //   },
                    //   gestures: {
                    //     'rotate': false,
                    //     'tilt': true,
                    //     'scroll': true
                    //   },
                    //   gestureHandling: 'cooperative',
                    //   mapType: that.mapKey,
                    //   styles: style
                    // }
                    var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                    // Environment.setBackgroundColor("black");
                    map.animateCamera({
                        target: { lat: 20.5937, lng: 78.9629 },
                        zoom: this.zoomLevel,
                        duration: 1000,
                        padding: 0 // default = 20px
                    });
                    // let boundssss: LatLngBounds = new LatLngBounds([
                    //   {"lat": data.last_location['lat'], "lng": data.last_location['long']}
                    // ]);
                    // map.moveCamera({
                    //   target: boundssss
                    // });
                    // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                    map.moveCamera({
                        target: {
                            lat: data.last_location['lat'], lng: data.last_location['long']
                        }
                    });
                    that.allData.map = map;
                    // that.onButtonClick();
                }
                else {
                    if (that.plt.is('ios')) {
                        // let mapOptions = {
                        //   backgroundColor: 'white',
                        //   controls: {
                        //     compass: false,
                        //     zoom: false,
                        //     myLocation: true,
                        //     myLocationButton: false,
                        //   },
                        //   gestures: {
                        //     'rotate': false,
                        //     'tilt': true,
                        //     'scroll': true
                        //   },
                        //   gestureHandling: 'cooperative',
                        //   camera: {
                        //     target: { lat: 20.5937, lng: 78.9629 },
                        //     zoom: this.zoomLevel,
                        //   },
                        //   mapType: that.mapKey,
                        //   styles: style
                        // }
                        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                        map.moveCamera({
                            target: {
                                lat: data.last_location['lat'], lng: data.last_location['long']
                            }
                        });
                        // Environment.setBackgroundColor("black");
                        that.allData.map = map;
                        // that.onButtonClick();
                    }
                }
                that.weDidntGetPing(data);
                // that.ioReceiveLocation(data);
                // that.socketInit(data);
            }
            else {
                if (data.last_lat !== undefined && data.last_lng !== undefined) {
                    if (that.plt.is('android')) {
                        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                        // Environment.setBackgroundColor("black");
                        map.animateCamera({
                            target: { lat: 20.5937, lng: 78.9629 },
                            zoom: this.zoomLevel,
                            duration: 3000,
                            padding: 0 // default = 20px
                        });
                        map.moveCamera({
                            target: {
                                lat: data.last_lat, lng: data.last_lng
                            }
                        });
                        that.allData.map = map;
                        // that.onButtonClick();
                    }
                    else {
                        if (that.plt.is('ios')) {
                            var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                            map.moveCamera({
                                target: {
                                    lat: data.last_lat, lng: data.last_lng
                                }
                            });
                            // Environment.setBackgroundColor("black");
                            that.allData.map = map;
                            // that.onButtonClick();
                        }
                    }
                    that.weDidntGetPing(data);
                }
                // that.allData.map = that.newMap();
                // that.socketInit(data);
            }
            //** Check if traffice mode is on or off **//
            if (localStorage.getItem('TrafficMode') !== null) {
                if (localStorage.getItem('TrafficMode') === 'ON') {
                    this.mapHideTraffic = true;
                    this.allData.map.setTrafficEnabled(true);
                }
            }
            //** Check if traffice mode is on or off **//
        }
    };
    LiveSingleDevice.prototype.showTrail = function (lat, lng, map) {
        var that = this;
        that.tempArray.push({ lat: lat, lng: lng });
        if (that.tempArray.length === 2) {
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then(function (polyline) {
                that.allData.polylineID = polyline;
                // that.polylineID.push(polyline);
            });
        }
        else if (that.tempArray.length > 2) {
            that.tempArray.shift();
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then(function (polyline) {
                that.allData.polylineID = polyline;
                // that.allData.polylineID.push(polyline);
            });
        }
    };
    LiveSingleDevice.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LiveSingleDevice.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                this.mapHideTraffic = !this.mapHideTraffic;
                if (this.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                }
                // this.ngOnDestroy();
                // that.mapHideTraffic = !that.mapHideTraffic;
                // if (that.mapHideTraffic) {
                //   that.allData.map.setTrafficEnabled(true);
                //   this.someFunc();
                // } else {
                //   that.allData.map.setTrafficEnabled(false);
                //   this.someFunc();
                // }
            }
            else {
                if (type == 'mapTrail') {
                    this.mapTrail = !this.mapTrail;
                    if (this.mapTrail) {
                        if (this.eyeBtnColor == '#6FA9CD') {
                            this.eyeBtnColor = '#f4f4f4';
                            this.eyeColor = '#6FA9CD';
                        }
                    }
                    else {
                        if (this.eyeBtnColor == '#f4f4f4') {
                            this.eyeBtnColor = '#6FA9CD';
                            this.eyeColor = '#fff';
                        }
                    }
                }
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], LiveSingleDevice.prototype, "navBar", void 0);
    LiveSingleDevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/live-single-device/live-single-device.html"*/'<ion-header>\n  <ion-navbar>\n    <!-- <ion-buttons start *ngIf="shwBckBtn">\n      <button ion-button small (click)="goBack()">\n        <ion-icon name="arrow-back"></ion-icon>{{ "back" | translate }}\n      </button>\n    </ion-buttons> -->\n    <ion-title>{{ "Live For " | translate }}{{ titleText }}</ion-title>\n    <ion-buttons end>\n      <button *ngIf="allData.tripStat != \'Started\'" ion-button icon-only (click)="createTrip()">\n        <ion-icon name="play"></ion-icon>\n      </button>\n      <button *ngIf="allData.tripStat == \'Started\'" ion-button icon-only (click)="endTrip()">\n        <ion-icon name="radio-button-on"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-segment mode="md" color="gpsc" scrollable="true" (ionChange)="segmentChanged($event)" [(ngModel)]="takeAction"\n    style="background-color: white;">\n    <ion-segment-button class="seg" style="padding-top: 3px;" value="live">\n      <img src="assets/imgs/segment/map_active.png" style="width: 30px;" *ngIf="takeAction === \'live\'" />\n      <img src="assets/imgs/segment/map_inactive.png" style="width: 30px;" *ngIf="takeAction !== \'live\'" />\n    </ion-segment-button>\n    <ion-segment-button style="padding-top: 3px;" value="history">\n      <img src="assets/imgs/segment/history_active.png" style="width: 30px;" *ngIf="takeAction === \'history\'" />\n      <img src="assets/imgs/segment/history_inactive.png" style="width: 30px;" *ngIf="takeAction !== \'history\'" />\n    </ion-segment-button>\n    <ion-segment-button style="padding-top: 3px;" value="trip">\n      <img src="assets/imgs/segment/trip_active.png" style="width: 30px;" *ngIf="takeAction === \'trip\'" />\n      <img src="assets/imgs/segment/trip_inactive.png" style="width: 30px;" *ngIf="takeAction !== \'trip\'" />\n    </ion-segment-button>\n    <ion-segment-button style="padding-top: 3px;" value="notif">\n      <img src="assets/imgs/segment/bell_active.png" style="width: 30px;" *ngIf="takeAction === \'notif\'" />\n      <img src="assets/imgs/segment/bell_inactive.png" style="width: 30px;" *ngIf="takeAction !== \'notif\'" />\n    </ion-segment-button>\n    <ion-segment-button style="padding-top: 3px;" value="car_service">\n      <img src="assets/imgs/segment/car_service_active.png" style="width: 30px;" *ngIf="takeAction === \'car_service\'" />\n      <img src="assets/imgs/segment/car_service_inactive.png" style="width: 30px;"\n        *ngIf="takeAction !== \'car_service\'" />\n    </ion-segment-button>\n  </ion-segment>\n</ion-header>\n<ion-content *ngIf="!hideMe">\n\n  <ion-fab right style="margin-top:85%">\n    <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n      <ion-icon name="locate"></ion-icon>\n    </button>\n  </ion-fab>\n  <ion-fab right style="margin-top:98%">\n    <button color="gpsc" ion-fab (click)="zoomin()" mini>\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>\n  <ion-fab right style="margin-top:111%">\n    <button color="gpsc" ion-fab (click)="zoomout()" mini>\n      <ion-icon name="remove"></ion-icon>\n    </button>\n  </ion-fab>\n\n\n  <ion-fab left style="margin-top:59%">\n    <button ion-fab [ngStyle]="{\'background-color\': policeButtonColor}" mini (click)="showNearby(\'police\')">\n      <!-- <img type="image/svg+xml" src="/assets/icon/police-station.svg"> -->\n      <ion-icon name="custom-station" [ngStyle]="{\'color\': policeColor}"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-fab left style="margin-top:72%">\n    <button ion-fab [ngStyle]="{\'background-color\': petrolButtonColor}" mini (click)="showNearby(\'petrol\')">\n      <!-- <img type="image/svg+xml" src="/assets/icon/fuel-service.svg"> -->\n      <ion-icon name="custom-fuel" [ngStyle]="{\'color\': petrolColor}"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-fab left style="margin-top:85%">\n    <button ion-fab color="gpsc" mini (click)="refreshMe1()">\n      <ion-icon name="refresh"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-fab left style="margin-top:98%">\n    <button ion-fab [ngStyle]="{\'background-color\': streetviewButtonColor}" mini (click)="streetView()">\n      <ion-icon name="custom-street" [ngStyle]="{\'color\': streetColor}"></ion-icon>\n    </button>\n  </ion-fab>\n  <ion-fab left style="margin-top:111%">\n    <button ion-fab [ngStyle]="{\'background-color\': navigateButtonColor}" mini (click)="navigateFromCurrentLoc()">\n      <ion-icon name="navigate" [ngStyle]="{\'color\': navColor}"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-row *ngIf="showDistDuration"\n    style="margin-top:72%; background-color: rgb(0, 0, 0, 0.5); font-size: 0.8em; color: white;border-radius: 25px;width: 70%;margin: auto; padding:5px;">\n    <ion-col style="background-color: transparent; text-align: center;" col-6>\n      {{ "Distance" | translate }} {{ expectation.distance }}\n    </ion-col>\n    <ion-col style="background-color: transparent; text-align: center;" col-6>\n      {{ "Time" | translate }} {{ expectation.duration }}\n    </ion-col>\n  </ion-row>\n  <div id="map_canvas_single_device">\n    <ion-row>\n      <ion-col>\n        <ion-fab top left>\n          <button ion-fab color="light" mini (click)="onClickMainMenu()">\n            <ion-icon color="gpsc" name="map"></ion-icon>\n          </button>\n          <ion-fab-list side="bottom">\n            <button style="margin: 0 2px;" ion-fab mini (click)="onClickMap(\'HYBRID\')" color="gpsc">\n              S\n            </button>\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n              T\n            </button>\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'NORMAL\')" color="gpsc">\n              N\n            </button>\n          </ion-fab-list>\n        </ion-fab>\n      </ion-col>\n      <ion-col>\n        <p class="blink" style="text-align: center;font-size:24px;color: green;font-weight: 600;"\n          *ngIf="vehicle_speed <= 60">\n          {{ vehicle_speed }}\n          <span style="font-size:16px;">{{ "Km/hr" | translate }}</span>\n        </p>\n        <p class="blink" style="text-align: center;font-size:24px;color: red;font-weight: 600;"\n          *ngIf="vehicle_speed > 60">\n          {{ vehicle_speed }}\n          <span style="font-size:16px;">{{ "Km/hr" | translate }}</span>\n        </p>\n      </ion-col>\n      <ion-col>\n        <ion-fab top right>\n          <button ion-fab color="gpsc" mini>\n            <ion-icon name="arrow-dropdown"></ion-icon>\n          </button>\n          <ion-fab-list side="bottom">\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab *ngIf="showShareBtn"\n              (click)="shareLive($event)">\n              <ion-icon name="share"></ion-icon>\n            </button>\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapTrail\')"\n              [ngStyle]="{\'background-color\': eyeBtnColor}">\n              <ion-icon name="eye" [ngStyle]="{\'color\': eyeColor}"></ion-icon>\n            </button>\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n              <img src="assets/icon/trafficON.png" *ngIf="mapHideTraffic" />\n              <img src="assets/icon/trafficOFF.png" *ngIf="!mapHideTraffic" />\n            </button>\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="settings()">\n              <ion-icon name="cog"></ion-icon>\n            </button>\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="addPOI()">\n              <ion-icon name="flag"></ion-icon>\n            </button>\n          </ion-fab-list>\n        </ion-fab>\n      </ion-col>\n    </ion-row>\n  </div>\n  <div id="viewParanoma" style="width: 100%;height: 300px;"></div>\n  <!-- <div *ngIf="showStreetMap" id="viewParanoma" style="width: 100%;height: 300px;"></div> -->\n  <!-- </div> -->\n</ion-content>\n<div *ngIf="showActionSheet" class="divPlan">\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop" [minimumHeight]="minimumHeight" [transition]="transition" (click)="setDocHeight()">\n    <div class="drawer-content">\n      <!-- <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">\n        {{ "Last Updated On" | translate }} &mdash;\n        {{ last_ping_on | date: "medium" }}\n\n        <span style="text-align: right;">\n          <img src="assets/imgs/statusIcons/car-battery-active.png" width="20" height="20" />\n        </span>\n      </p> -->\n      <ion-row style="margin-bottom: -10px;">\n        <ion-col col-9 no-padding>\n          <p padding-left style="font-size: 13px; color:cornflowerblue;">\n            {{ "Last Updated On" | translate }} &mdash;\n            {{ last_ping_on | date: "medium" }}\n          </p>\n        </ion-col>\n        <!-- <ion-col col-1 no-padding style="padding-top: 13px;">\n          <img src="assets/imgs/car-battery.png" width="15" height="15" />\n        </ion-col> -->\n        <ion-col col-3 no-padding style="text-align: right; padding-right: 20px;">\n\n          <p style="font-size: 13px;">\n            <!-- <img src="assets/imgs/car-battery.png" width="15" height="15" /> -->\n            <ion-icon name="battery-charging" style="color: #11a46e;"></ion-icon>\n            &nbsp;{{battery_percent}} %\n          </p>\n        </ion-col>\n      </ion-row>\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n      <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p>\n      <hr />\n      <ion-row>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC == \'0\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC == \'1\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC == null" width="20" height="20" />\n          <p>{{ "IGN" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel == \'1\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel == \'0\'" width="20" height="20" />\n          <p>{{ "AC" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20" />\n          <p>{{ "FUEL" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power == \'0\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power == \'1\'" width="20" height="20" />\n          <p>{{ "POWER" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking == \'0\'" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking == \'1\'" width="20" height="20" />\n          <p>{{ "GPS" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <ion-icon name="thermometer" *ngIf="tempreture == null" style="color: gray; font-size: 1.6em;"></ion-icon>\n          <ion-icon name="thermometer" *ngIf="tempreture == \'0\'" color="danger" style="font-size: 1.6em;"></ion-icon>\n          <ion-icon name="thermometer" *ngIf="tempreture == \'1\'" color="secondary" style="font-size: 1.6em;"></ion-icon>\n          <p>{{ "Temp" | translate }}</p>\n        </ion-col>\n        <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/car_no_data.png" *ngIf="door == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/car_door_open.png" *ngIf="door == 0" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/car_door_close.png" *ngIf="door == 1" width="20" height="20" />\n          <p>{{ "Door" | translate }}</p>\n        </ion-col>\n        <!-- <ion-col width-1 style="text-align:center">\n          <img src="assets/imgs/statusIcons/car-battery-na.png" *ngIf="battery_percent == null" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/car-battery-inactive.png" *ngIf="battery_percent == 0" width="20" height="20" />\n          <img src="assets/imgs/statusIcons/car-battery-active.png" *ngIf="battery_percent == 1" width="20" height="20" />\n          <p>{{ "Battery" | translate }}</p>\n        </ion-col> -->\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!total_odo">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="total_odo">\n            {{ total_odo | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 13px">{{ "Odometer" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!vehicle_speed">\n            0 {{ "Km/hr" | translate }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="vehicle_speed">\n            {{ vehicle_speed }} {{ "Km/hr" | translate }}\n          </p>\n          <p style="font-size: 13px">\n            {{ "Speed" | translate }}\n          </p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="fuel">\n            {{ fuel }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!fuel">N/A</p>\n          <p style="font-size: 13px">{{ "Fuel" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="voltage">\n            {{ voltage | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!voltage">N/A</p>\n          <p style="font-size: 13px">{{ "Voltage" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="tempreture">\n            {{ tempreture | number: "1.0-2" }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!tempreture">N/A</p>\n          <p style="font-size: 13px">{{ "Temp" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!distFromLastStop">\n            0 {{ "Kms" | translate }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="distFromLastStop">\n            {{ distFromLastStop | number: "1.0-2" }} {{ "Kms" | translate }}\n          </p>\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%" />\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!todays_odo">\n            0 {{ "Kms" | translate }}\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="todays_odo">\n            {{ todays_odo | number: "1.0-2" }} {{ "Kms" | translate }}\n          </p>\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!timeAtLastStop">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="timeAtLastStop">\n            {{ timeAtLastStop }}\n          </p>\n          <p style="font-size: 13px">{{ "At Last Stop" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%" />\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!today_stopped">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="today_stopped">\n            {{ today_stopped }}\n          </p>\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n        </ion-col>\n      </ion-row>\n      <hr />\n      <ion-row>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!lastStoppedAt">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="lastStoppedAt">\n            {{ lastStoppedAt }}\n          </p>\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%" />\n        </ion-col>\n        <ion-col width-33 style="text-align:center">\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!today_running">\n            N/A\n          </p>\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="today_running">\n            {{ today_running }}\n          </p>\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>\n\n<div *ngIf="showFooter">\n  <ion-bottom-drawer [(state)]="drawerState1" [dockedHeight]="dockedHeight1" [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop1" [minimumHeight]="minimumHeight1">\n    <div class="drawer-content">\n      <p style="font-size:1.2em; color:black;">\n        {{ "Share Live Vehicle" | translate }}\n      </p>\n      <ion-row>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition }}" (click)="sharedevices(\'15mins\')">\n            {{ "15 mins" | translate }}\n          </button>\n        </ion-col>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition1 }}" (click)="sharedevices(\'1hour\')">\n            {{ "1 hour" | translate }}\n          </button>\n        </ion-col>\n        <ion-col col-4 style="padding: 5px">\n          <button ion-button block color="{{ condition2 }}" (click)="sharedevices(\'8hours\')">\n            {{ "8 hours" | translate }}\n          </button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-4></ion-col>\n        <ion-col col-4></ion-col>\n        <ion-col col-4 style="text-align: right;">\n          <ion-fab style="right: calc(10px + env(safe-area-inset-right));">\n            <button ion-fab mini (click)="shareLivetemp()" color="gpsc">\n              <ion-icon name="send" color="black"></ion-icon>\n            </button>\n          </ion-fab>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/live-single-device/live-single-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_10__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], LiveSingleDevice);
    return LiveSingleDevice;
}());

var PoiPage = /** @class */ (function () {
    function PoiPage(apicall, toastCtrl, navparam, viewCtrl) {
        this.apicall = apicall;
        this.toastCtrl = toastCtrl;
        this.navparam = navparam;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.params = this.navparam.get("param1");
        this.lat = this.params.mark.getPosition().lat;
        this.lng = this.params.mark.getPosition().lng;
    }
    PoiPage.prototype.ngOnInit = function () {
        var _this = this;
        var that = this;
        that.address = undefined;
        // debugger
        if (this.lat != undefined || this.lng != undefined) {
            var tempcord = {
                "lat": this.lat,
                "long": this.lng
            };
            this.apicall.getAddress(tempcord)
                .subscribe(function (res) {
                if (res.message == "Address not found in databse") {
                    var geocoder = new google.maps.Geocoder;
                    var latlng = new google.maps.LatLng(_this.lat, _this.lng);
                    var request = { "latLng": latlng };
                    geocoder.geocode(request, function (resp, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (resp[0] != null) {
                                that.address = resp[0].formatted_address;
                            }
                            else {
                                console.log("No address available");
                            }
                        }
                        else {
                            that.address = 'N/A';
                        }
                    });
                }
                else {
                    that.address = res.address;
                }
                // console.log(that.address);
            });
        }
    };
    PoiPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PoiPage.prototype.save = function () {
        var _this = this;
        if (this.poi_name == undefined || this.address == undefined) {
            var toast = this.toastCtrl.create({
                message: "POI name is required!",
                duration: 1500,
                position: "bottom"
            });
            toast.present();
        }
        else {
            var payload = {
                "poi": [{
                        "location": {
                            "type": "Point",
                            "coordinates": [
                                this.lng,
                                this.lat
                            ]
                        },
                        "poiname": this.poi_name,
                        "status": "Active",
                        "address": this.address,
                        "user": this.islogin._id
                    }]
            };
            this.apicall.startLoading().present();
            this.apicall.addPOIAPI(payload)
                .subscribe(function (data) {
                _this.apicall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: "POI added successfully!",
                    duration: 2000,
                    position: 'top'
                });
                toast.present();
                _this.viewCtrl.dismiss();
            }, function (err) {
                _this.apicall.stopLoading();
            });
        }
    };
    PoiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/live-single-device/poi.html"*/'<!-- <ion-content padding> -->\n<div>\n    <ion-row>\n        <ion-col col-12 style="text-align: center; font-size: 2rem;">\n            <b>Add POI</b>&nbsp;&nbsp;&nbsp;\n            <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n        </ion-col>\n        <!-- <ion-col col-2 style="text-align: right">\n                <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n            </ion-col> -->\n    </ion-row><br />\n    <!-- <p style="text-align: center; font-size: 2rem;">\n            <b>Add POI&nbsp;&nbsp;<ion-icon class="close-button" id="close-button" name="close-circle"\n                (tap)="dismiss()"></ion-icon></b>\n        </p><br /> -->\n    <ion-row>\n        <ion-col col-12><b>POI Name:</b></ion-col>\n        <ion-col col-12>\n            <ion-input type="text" [(ngModel)]="poi_name"></ion-input>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-12><b>POI Address:</b></ion-col>\n        <ion-col col-12>\n            <ion-input type="text" [(ngModel)]="address"></ion-input>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col col-6>\n            <button ion-button block (tap)="save()" color="gpsc">SAVE</button>\n        </ion-col>\n        <ion-col col-6>\n            <button ion-button block (tap)="dismiss()" color="gpsc">CANCEL</button>\n        </ion-col>\n    </ion-row>\n</div>\n<!-- </ion-content> -->'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/live-single-device/poi.html"*/,
            selector: 'page-device-settings'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PoiPage);
    return PoiPage;
}());

//# sourceMappingURL=live-single-device.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AcReportPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ACDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AcReportPage = /** @class */ (function () {
    function AcReportPage(navCtrl, navParams, apiCAll) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCAll = apiCAll;
        this.portstemp = [];
        this.reportData = [];
        this.twoMonthsLater = __WEBPACK_IMPORTED_MODULE_3_moment__().subtract(2, 'month').format("YYYY-MM-DD");
        this.today = __WEBPACK_IMPORTED_MODULE_3_moment__().format("YYYY-MM-DD");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    AcReportPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter AcReportPage');
    };
    AcReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    AcReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCAll.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCAll.startLoading().present();
        this.apiCAll.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCAll.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCAll.stopLoading();
            console.log(err);
        });
    };
    AcReportPage.prototype.getAcReportData = function () {
        var _this = this;
        var that = this;
        this.reportData = [];
        if (that.dev_id == undefined) {
            that.dev_id = "";
        }
        this.apiCAll.startLoading().present();
        ;
        this.apiCAll.getACReportAPI(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), that.islogin._id, that.dev_id)
            .subscribe(function (data) {
            _this.apiCAll.stopLoading();
            console.log("ac report data: ", data);
            _this.reportData = data;
            // console.log("1stAction: ", data[0]['1stAction'])
        });
    };
    AcReportPage.prototype.getAcReporID = function (key) {
        console.log("key: ", key.Device_ID);
        this.dev_id = key.Device_ID;
    };
    AcReportPage.prototype.showDetail = function (pdata) {
        console.log("pdata: ", pdata);
        var that = this;
        this.navCtrl.push(ACDetailPage, {
            'param': pdata,
            'fdate': that.datetimeStart,
            'tdate': that.datetimeEnd,
            'uid': that.islogin._id
        });
    };
    AcReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ac-report',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/ac-report/ac-report.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "AC Report" | translate }}</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getAcReporID(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [min]="twoMonthsLater" [max]="today" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getAcReportData();">\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <ion-list *ngIf="reportData.length === 0">\n    <ion-item>\n      <p>Oops.. No record found..!</p>\n    </ion-item>\n  </ion-list>\n  <div *ngIf="reportData.length > 0">\n    <ion-card *ngFor="let rep of reportData;" (click)="showDetail(rep)">\n      <ion-item style="border-bottom: 2px solid #dedede;">\n        <ion-avatar item-start>\n          <img src="assets/imgs/car_red_icon.png">\n        </ion-avatar>\n        <ion-label>{{rep.VehicleName}}</ion-label>\n      </ion-item>\n\n      <ion-card-content>\n        <ion-row style="padding-top:5px;">\n          <ion-col col-6>{{rep[\'1stAction\']}}</ion-col>\n          <ion-col col-6>{{rep[\'1stTime\']}}</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Action</ion-col>\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">ON Time(Min.)</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>{{rep[\'2stAction\']}}</ion-col>\n          <ion-col col-6>{{rep[\'2stTime\']}}</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Action</ion-col>\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">OFF Time(Min.)</ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/ac-report/ac-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], AcReportPage);
    return AcReportPage;
}());

var ACDetailPage = /** @class */ (function () {
    function ACDetailPage(apiCall, navParam) {
        this.apiCall = apiCall;
        this.navParam = navParam;
        this.paramData = {};
        this.ddata = [];
        console.log("param parameters: ", this.navParam.get('param'));
        this.paramData = this.navParam.get('param');
        this.fdate = this.navParam.get('fdate');
        this.tdate = this.navParam.get('tdate');
        this.uid = this.navParam.get('uid');
    }
    ACDetailPage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ACDetailPage.prototype.getDetails = function () {
        var _this = this;
        debugger;
        this.apiCall.startLoading().present();
        this.apiCall.getDetailACReportAPI(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.paramData._id.imei)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("detailed ac report data: ", data[0].s);
            _this.ddata = data[0].s;
        });
    };
    ACDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/ac-report/ac-detail.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{ "Details for" | translate }} {{paramData.VehicleName}}</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-card *ngFor="let d of ddata;">\n        <ion-card-content>\n            <ion-row>\n                <ion-col col-6 style="font-size:13px;text-align:justify;">{{d.switch}}</ion-col>\n                <ion-col col-6 style="font-size:13px;text-align:justify;">\n                    {{d.timestamp | date:\'medium\'}}</ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">{{ "ON/OFF" | translate }}</ion-col>\n                <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">{{ "Time" | translate }}</ion-col>\n            </ion-row>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/ac-report/ac-detail.html"*/,
            styles: ["\n  .col {\n    padding: 0px;\n}"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ACDetailPage);
    return ACDetailPage;
}());

//# sourceMappingURL=ac-report.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDevicesPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__immobilize_modal__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__profile_settings_notif_setting_time_picker_time_picker__ = __webpack_require__(329);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var AddDevicesPage = /** @class */ (function () {
    function AddDevicesPage(navCtrl, navParams, apiCall, alertCtrl, toastCtrl, 
    // private sms: SMS,
    modalCtrl, popoverCtrl, geocoderApi, plt, translate, events, sqlite) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.geocoderApi = geocoderApi;
        this.plt = plt;
        this.translate = translate;
        this.events = events;
        this.sqlite = sqlite;
        this.loadProgress = 0;
        this.allDevices = [];
        this.allDevicesSearch = [];
        this.option_switch = false;
        this.searchCountryString = ''; // initialize your searchCountryString string empty
        this.page = 0;
        this.limit = 8;
        this.searchQuery = '';
        this.clicked = false;
        this.checkIfStat = "ALL";
        this.toggled = false;
        this.all_devices = [];
        this.showEmptyStatus = false;
        this.showDrawer = false;
        this.shouldBounce = true;
        this.dockedHeight = 185;
        this.distanceTop = 556;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 0;
        this.hideMe = false;
        this.AllData = [];
        this.runningData = [];
        this.stoppedData = [];
        this.idlingData = [];
        this.outOfReachData = [];
        this.toggled = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        if (localStorage.getItem('Total_Vech') !== null) {
            this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
        }
        // this.superAdmin = this.islogin.isSuperAdmin;
        // this.adbtn = localStorage.getItem("dlrchk");
        // this.dealer_Permission = this.islogin.device_add_permission;
        this.isDealer = this.islogin.isDealer;
        this.isSuperAdmin = this.islogin.isSuperAdmin;
        this.islogindealer = localStorage.getItem('isDealervalue');
        if (this.isDealer == false && this.isSuperAdmin == false) {
            this.dealer_Permission = false;
        }
        else {
            this.dealer_Permission = this.islogin.device_add_permission;
            console.log("dealer_Permission devices => " + this.dealer_Permission);
        }
        if (navParams.get("label") && navParams.get("value")) {
            this.stausdevice = localStorage.getItem('status');
            this.checkIfStat = this.stausdevice;
            this.singleVehicleCount = navParams.get("value");
        }
        else {
            this.stausdevice = undefined;
        }
        ////////////
        this.events.subscribe("Released:Dismiss", function () {
            _this.getdevices();
            // this.getdevicesTemp();
        });
    }
    AddDevicesPage.prototype.toggle = function () {
        this.toggled = !this.toggled;
    };
    AddDevicesPage.prototype.cancelSearch = function () {
        // this.toggle();
        this.toggled = false;
    };
    AddDevicesPage.prototype.showBottomDrawer = function (data) {
        console.log('drawerData', data);
        this.drawerData = data;
        // if(this.showDrawer) {
        //   this.showDrawer = false;
        // }
        //  setTimeout(() => {
        this.showDrawer = true;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        //  }, 500);
    };
    AddDevicesPage.prototype.fonctionTest = function (d) {
        var _this = this;
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__profile_settings_notif_setting_time_picker_time_picker__["a" /* TimePickerModal */], {
            data: d,
            key: 'parking'
        });
        modal.onDidDismiss(function (data) {
            // console.log(data);
            _this.getdevicesTemp123();
        });
        modal.present();
        // var theft;
        // theft = !(d.theftAlert);
        // if (theft) {
        //   let alert = this.alertCtrl.create({
        //     title: this.translate.instant('Confirm'),
        //     message: this.translate.instant('Are you sure you want to activate parking alarm? On activating this alert you will get receive notification if vehicle moves.'),
        //     buttons: [
        //       {
        //         text: this.translate.instant('YES PROCEED'),
        //         handler: () => {
        //           var payload = {
        //             "_id": d._id,
        //             "theftAlert": theft
        //           }
        //           this.apiCall.startLoading();
        //           this.apiCall.deviceupdateCall(payload)
        //             .subscribe(data => {
        //               this.apiCall.stopLoading();
        //               let toast = this.toastCtrl.create({
        //                 message: this.translate.instant('Parking alarm Activated!'),
        //                 position: "bottom",
        //                 duration: 1000
        //               });
        //               toast.present();
        //               // this.getdevices();
        //             })
        //         }
        //       },
        //       {
        //         text: 'BACK',
        //         handler: () => {
        //           d.theftAlert = !(d.theftAlert);
        //         }
        //       }
        //     ]
        //   })
        //   alert.present();
        // } else {
        //   let alert = this.alertCtrl.create({
        //     title: this.translate.instant('Confirm'),
        //     message: this.translate.instant('Are you sure you want to deactivate parking alarm?'),
        //     buttons: [
        //       {
        //         text: this.translate.instant('YES PROCEED'),
        //         handler: () => {
        //           // theft = d.theftAlert;
        //           var payload = {
        //             "_id": d._id,
        //             "theftAlert": theft
        //           }
        //           this.apiCall.startLoading();
        //           this.apiCall.deviceupdateCall(payload)
        //             .subscribe(data => {
        //               this.apiCall.stopLoading();
        //               console.log("resp: ", data)
        //               let toast = this.toastCtrl.create({
        //                 message: this.translate.instant('Parking alarm Deactivated!'),
        //                 position: "bottom",
        //                 duration: 1000
        //               });
        //               toast.present();
        //               // this.getdevices();
        //             })
        //         }
        //       },
        //       {
        //         text: 'BACK',
        //         handler: () => {
        //           d.theftAlert = !(d.theftAlert);
        //         }
        //       }
        //     ]
        //   })
        //   alert.present();
        // }
    };
    AddDevicesPage.prototype.towAlertCall = function (d) {
        var _this = this;
        debugger;
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__profile_settings_notif_setting_time_picker_time_picker__["a" /* TimePickerModal */], {
            data: d,
            key: 'tow'
        });
        modal.onDidDismiss(function (data) {
            // console.log(data);
            _this.getdevicesTemp123();
        });
        modal.present();
        // var tow;
        // tow = !(d.towAlert);
        // if (tow) {
        //   let alert = this.alertCtrl.create({
        //     title: "Confirm",
        //     message: "Are you sure you want to activate Tow Alert alarm? On activating this alert you will get receive notification if vehicle has been towed.",
        //     buttons: [
        //       {
        //         text: 'YES PROCEED',
        //         handler: () => {
        //           // theft = !(d.theftAlert);
        //           // theft = d.theftAlert;
        //           var payload = {
        //             "_id": d._id,
        //             "towAlert": tow
        //           }
        //           this.apiCall.startLoading();
        //           this.apiCall.deviceupdateCall(payload)
        //             .subscribe(data => {
        //               this.apiCall.stopLoading();
        //               console.log("resp: ", data)
        //               let toast = this.toastCtrl.create({
        //                 message: "Tow Alert alarm Activated!",
        //                 position: "bottom",
        //                 duration: 1000
        //               });
        //               toast.present();
        //               // this.callObjFunc(d);
        //               // this.getdevices();
        //             })
        //         }
        //       },
        //       {
        //         text: 'BACK',
        //         handler: () => {
        //           // d.theftAlert = !(d.theftAlert);
        //         }
        //       }
        //     ]
        //   })
        //   alert.present();
        // } else {
        //   let alert = this.alertCtrl.create({
        //     title: "Confirm",
        //     message: "Are you sure you want to deactivate Tow Alert alarm?",
        //     buttons: [
        //       {
        //         text: 'YES PROCEED',
        //         handler: () => {
        //           // theft = d.theftAlert;
        //           var payload = {
        //             "_id": d._id,
        //             "towAlert": tow
        //           }
        //           this.apiCall.startLoading();
        //           this.apiCall.deviceupdateCall(payload)
        //             .subscribe(data => {
        //               this.apiCall.stopLoading();
        //               console.log("resp: ", data)
        //               let toast = this.toastCtrl.create({
        //                 message: "Tow Alert alarm Deactivated!",
        //                 position: "bottom",
        //                 duration: 1000
        //               });
        //               toast.present();
        //               // this.callObjFunc(d);
        //               // this.getdevices();
        //             })
        //         }
        //       },
        //       {
        //         text: 'BACK',
        //         handler: () => {
        //           // d.theftAlert = !(d.theftAlert);
        //         }
        //       }
        //     ]
        //   })
        //   alert.present();
        // }
    };
    AddDevicesPage.prototype.ngOnInit = function () {
        this.now = new Date().toISOString();
        // this.getdevicesTemp();
        // this.getDataFromSQLiteDB();
        // this.intervalid123 = setInterval(() => {
        //   this.page = 0;
        //   this.getdevicesTemp123();
        // }, 30000);
        // localStorage.setItem('getDevicesInterval_ID', JSON.stringify(this.intervalid123))
    };
    AddDevicesPage.prototype.ngOnDestroy = function () { };
    AddDevicesPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.navBar.backButtonClick = function (ev) {
            console.log('this will work in Ionic 3 +');
            _this.hideMe = true;
            _this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'back'
            });
        };
        this.getdevices();
        this.cancelSearch();
        // this.getdevicesTemp();
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
    };
    AddDevicesPage.prototype.getDataFromSQLiteDB = function () {
        var _this = this;
        var that = this;
        this.sqlite.create({
            name: 'oneqlik_vts.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql("\n      CREATE TABLE IF NOT EXISTS vehicle_list(\n        _id VARCHAR, \n        Device_Name TEXT,\n        Device_ID BIGINT,\n        supAdmin VARCHAR,\n        Dealer VARCHAR,\n        expiration_date VARCHAR, \n        status_updated_at VARCHAR, \n        fuel_percent INT, \n        currentFuel INT,\n        last_speed INT,\n        created_on VARCHAR, \n        today_odo FLOAT, \n        contact_number BIGINT, \n        iconType TEXT,\n        vehicleType TEXT,\n        status TEXT,\n        last_lat FLOAT,\n        last_lng FLOAT)", [])
                .then(function (res) {
                db.executeSql('SELECT * FROM vehicle_list', [])
                    .then(function (res) {
                    that.loadProgress = 100;
                    var expiredDevices = [];
                    var nodataDevices = [];
                    that.runningData = [];
                    that.idlingData = [];
                    that.stoppedData = [];
                    that.outOfReachData = [];
                    that.all_devices = [];
                    debugger;
                    if (res.rows.length > 0) {
                        console.log("total vehicle count: ", that.t_veicle_count);
                        // if(res.rows.length === that.t_veicle_count) {
                        // }
                        for (var i = 0; i < res.rows.length; i++) {
                            console.log('icontype: ', res.rows.item(i).iconType);
                            if (res.rows.item(i).status === 'RUNNING') {
                                that.runningData.push(res.rows.item(i));
                            }
                            else if (res.rows.item(i).status === 'IDLING') {
                                that.idlingData.push(res.rows.item(i));
                            }
                            else if (res.rows.item(i).status === 'STOPPED') {
                                that.stoppedData.push(res.rows.item(i));
                            }
                            else if (res.rows.item(i).status === 'OUT OF REACH') {
                                that.outOfReachData.push(res.rows.item(i));
                            }
                            else if (res.rows.item(i).status === 'Expired') {
                                expiredDevices.push(res.rows.item(i));
                            }
                            else if (res.rows.item(i).status === 'NO DATA') {
                                nodataDevices.push(res.rows.item(i));
                            }
                            that.all_devices.push(res.rows.item(i));
                        }
                        if (_this.checkIfStat === 'ALL') {
                            _this.allDevicesSearch = that.all_devices;
                        }
                        else if (_this.checkIfStat === 'RUNNING') {
                            if (_this.runningData.length !== _this.singleVehicleCount) {
                                _this.getdevicesTemp123();
                            }
                            else {
                                _this.allDevicesSearch = _this.runningData;
                            }
                        }
                        else if (_this.checkIfStat === 'IDLING') {
                            // this.allDevicesSearch = this.idlingData;
                            if (_this.idlingData.length !== _this.singleVehicleCount) {
                                _this.getdevicesTemp123();
                            }
                            else {
                                _this.allDevicesSearch = _this.idlingData;
                            }
                        }
                        else if (_this.checkIfStat === 'OUT OF REACH') {
                            // this.allDevicesSearch = this.outOfReachData;
                            if (_this.outOfReachData.length !== _this.singleVehicleCount) {
                                _this.getdevicesTemp123();
                            }
                            else {
                                _this.allDevicesSearch = _this.outOfReachData;
                            }
                        }
                        else if (_this.checkIfStat === 'STOPPED') {
                            // this.allDevicesSearch = this.stoppedData;
                            if (_this.stoppedData.length !== _this.singleVehicleCount) {
                                _this.getdevicesTemp123();
                            }
                            else {
                                _this.allDevicesSearch = _this.stoppedData;
                            }
                        }
                        else if (_this.checkIfStat === 'Expired') {
                            // this.allDevicesSearch = expiredDevices;
                            if (expiredDevices.length !== _this.singleVehicleCount) {
                                _this.getdevicesTemp123();
                            }
                            else {
                                _this.allDevicesSearch = expiredDevices;
                            }
                        }
                        else if (_this.checkIfStat === 'NO DATA') {
                            // this.allDevicesSearch = nodataDevices;
                            if (nodataDevices.length !== _this.singleVehicleCount) {
                                _this.getdevicesTemp123();
                            }
                            else {
                                _this.allDevicesSearch = nodataDevices;
                            }
                        }
                    }
                    else {
                        _this.getdevicesTemp123();
                    }
                })
                    .catch(function (e) { return console.log(e); });
            })
                .catch(function (e) {
                console.log(e);
            });
        });
    };
    AddDevicesPage.prototype.segmentChanged = function (ev) {
        console.log('Segment changed', ev);
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        this.checkIfStat = ev._value;
        if (this.checkIfStat !== 'ALL') {
            this.stausdevice = ev._value;
        }
        else {
            this.stausdevice = undefined;
        }
        // this.allDevicesSearch = [];
        // if (this.all_devices.length > 0) {
        //   if (this.checkIfStat === 'ALL' || this.checkIfStat === 'Expired' || this.checkIfStat === 'NO DATA') {
        //     // this.allDevicesSearch = this.allDevices;
        //     this.allDevicesSearch = this.all_devices;
        //   } else if (this.checkIfStat === 'RUNNING') {
        //     this.allDevicesSearch = this.runningData;
        //   } else if (this.checkIfStat === 'IDLING') {
        //     this.allDevicesSearch = this.idlingData;
        //   } else if (this.checkIfStat === 'OUT OF REACH') {
        //     this.allDevicesSearch = this.outOfReachData;
        //   } else if (this.checkIfStat === 'STOPPED') {
        //     this.allDevicesSearch = this.stoppedData;
        //   }
        // }
        // if (this.singleVehicleCount !== undefined) {
        // this.getDataFromSQLiteDB();
        // this.getdevicesTemp();
        // }
        // this.stausdevice = ev._value
        // this.scrollEv = undefined;
        // this.laodingEl = undefined;
        this.page = 0;
        // this.allDevicesSearch = [];
        this.loadProgress = 100;
        // this.progressIntervalId = undefined;
        this.getdevices();
    };
    AddDevicesPage.prototype.getdevices = function () {
        var _this = this;
        this.showEmptyStatus = false;
        this.showDrawer = false;
        var baseURLp;
        if (this.stausdevice) {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.progressBar();
        if (this.loadProgress == 100) {
            this.loadProgress = 0;
        }
        // this.apiCall.toastMsgStarted();
        // this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            // this.apiCall.toastMsgDismised();
            var that = _this;
            that.loadProgress = 100;
            clearInterval(that.progressIntervalId);
            _this.allDevicesSearch = [];
            _this.ndata = data.devices;
            _this.allDevices = _this.ndata;
            _this.allDevicesSearch = _this.ndata;
            // this.allDevicesSearch = this.ndata;
            if (_this.allDevicesSearch.length === 0) {
                _this.showEmptyStatus = true;
            }
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
        }, function (err) {
            console.log("error=> ", err);
            var that = _this;
            // that.loadProgress = 100;
            clearInterval(that.progressIntervalId);
            // this.apiCall.stopLoading();
            // this.apiCall.toastMsgDismised();
        });
    };
    // getdevicesTemp() {
    //   let baseURLp;
    //   this.page = 0;
    //   this.showEmptyStatus = false;
    //   this.showDrawer = false;
    //   debugger
    //   // if (this.stausdevice === "Expired") {
    //   //   baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
    //   // }
    //   // else {
    //   baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    //   // }
    //   if (this.islogin.isSuperAdmin == true) {
    //     baseURLp += '&supAdmin=' + this.islogin._id;
    //   } else {
    //     if (this.islogin.isDealer == true) {
    //       baseURLp += '&dealer=' + this.islogin._id;
    //     }
    //   }
    //   this.allDevicesSearch = [];
    //   this.allDevices = [];
    //   this.runningData = [];
    //   this.idlingData = [];
    //   this.stoppedData = [];
    //   this.outOfReachData = [];
    //   // this.apiCall.toastMsgStarted();
    //   // this.apiCall.startLoading().present();
    //   this.loadProgress = 0;
    //   this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
    //     .subscribe(data => {
    //       // this.apiCall.stopLoading();
    //       // this.apiCall.toastMsgDismised();
    //       // this.ndata = data.devices;
    //       // this.allDevices = this.ndata;
    //       // this.allDevicesSearch = this.ndata;
    //       // this.ndata = data.devices;
    //       this.allDevices = data.devices;
    //       this.allDevicesSearch = data.devices;
    //       let that = this;
    //       ////////////////////////////////////////////
    //       for (var i = 0; i < data.devices.length; i++) {
    //         if (data.devices[i].status === 'RUNNING') {
    //           that.runningData.push(data.devices[i]);
    //         } else if (data.devices[i].status === 'IDLING') {
    //           that.idlingData.push(data.devices[i]);
    //         } else if (data.devices[i].status === 'STOPPED') {
    //           that.stoppedData.push(data.devices[i]);
    //         } else if (data.devices[i].status === 'OUT OF REACH') {
    //           that.outOfReachData.push(data.devices[i]);
    //         }
    //       }
    //       debugger
    //       if (this.checkIfStat === 'ALL' || this.checkIfStat === 'Expired') {
    //         this.allDevicesSearch = data.devices;
    //       } else if (this.checkIfStat === 'RUNNING') {
    //         this.allDevicesSearch = this.runningData;
    //       } else if (this.checkIfStat === 'IDLING') {
    //         this.allDevicesSearch = this.idlingData;
    //       } else if (this.checkIfStat === 'OUT OF REACH') {
    //         this.allDevicesSearch = this.outOfReachData;
    //       } else if (this.checkIfStat === 'STOPPED') {
    //         this.allDevicesSearch = this.stoppedData;
    //       }
    //       if (this.allDevicesSearch.length === 0) {
    //         this.showEmptyStatus = true;
    //       }
    //       console.log('running data: ', that.runningData.length);
    //       console.log('idling data: ', that.idlingData.length);
    //       console.log('out of reach data: ', that.outOfReachData.length);
    //       console.log('stopped data: ', that.stoppedData.length);
    //       ////////////////////////////////////////////
    //       that.userPermission = JSON.parse(localStorage.getItem('details'));
    //       if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
    //         that.option_switch = true;
    //       } else {
    //         if (localStorage.getItem('isDealervalue') == 'true') {
    //           that.option_switch = true;
    //         } else {
    //           if (that.userPermission.isDealer == false) {
    //             that.option_switch = false;
    //           }
    //         }
    //       }
    //       this.loadProgress = 100;
    //     },
    //       err => {
    //         console.log("error=> ", err);
    //         // this.apiCall.stopLoading();
    //         // this.apiCall.toastMsgDismised();
    //       });
    // }
    AddDevicesPage.prototype.getdevicesTemp123 = function () {
        var _this = this;
        // debugger
        var baseURLp;
        if (this.checkIfStat != undefined && this.checkIfStat !== 'ALL') {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.checkIfStat + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            this.limit = 10;
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        // if (this.stausdevice) {
        //   baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        // }
        // else {
        // baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        // }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.progressBar();
        if (this.loadProgress == 100) {
            this.loadProgress = 0;
        }
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.loadProgress = 100;
            _this.ndata = data.devices;
            _this.allDevices = data.devices;
            // this.allDevicesSearch = this.ndata;
            var that = _this;
            ////////////////////////////////////////////
            _this.AllData = [];
            _this.runningData = [];
            _this.stoppedData = [];
            _this.idlingData = [];
            _this.outOfReachData = [];
            var expiredDevices = [];
            var nodataDevices = [];
            that.all_devices = [];
            for (var i = 0; i < data.devices.length; i++) {
                // console.log('icontype: ', res.rows.item(i).iconType);
                if (data.devices[i].status === 'RUNNING') {
                    that.runningData.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'IDLING') {
                    that.idlingData.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'STOPPED') {
                    that.stoppedData.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'OUT OF REACH') {
                    that.outOfReachData.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'Expired') {
                    expiredDevices.push(data.devices[i]);
                }
                else if (data.devices[i].status === 'NO DATA') {
                    nodataDevices.push(data.devices[i]);
                }
                that.all_devices.push(data.devices[i]);
            }
            if (_this.checkIfStat === 'ALL') {
                _this.allDevicesSearch = that.all_devices;
            }
            else if (_this.checkIfStat === 'RUNNING') {
                _this.allDevicesSearch = _this.runningData;
            }
            else if (_this.checkIfStat === 'IDLING') {
                _this.allDevicesSearch = _this.idlingData;
            }
            else if (_this.checkIfStat === 'OUT OF REACH') {
                _this.allDevicesSearch = _this.outOfReachData;
            }
            else if (_this.checkIfStat === 'STOPPED') {
                _this.allDevicesSearch = _this.stoppedData;
            }
            else if (_this.checkIfStat === 'Expired') {
                _this.allDevicesSearch = expiredDevices;
            }
            else if (_this.checkIfStat === 'NO DATA') {
                _this.allDevicesSearch = nodataDevices;
            }
            console.log('running data: ', that.runningData.length);
            console.log('idling data: ', that.idlingData.length);
            console.log('out of reach data: ', that.outOfReachData.length);
            console.log('stopped data: ', that.stoppedData.length);
            ////////////////////////////////////////////
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
        }, function (err) {
            console.log("error=> ", err);
        });
    };
    AddDevicesPage.prototype.activateVehicle = function (data) {
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": data
        });
    };
    AddDevicesPage.prototype.timeoutAlert = function () {
        var _this = this;
        var alerttemp = this.alertCtrl.create({
            message: "the server is taking much time to respond. Please try in some time.",
            buttons: [{
                    text: this.translate.instant('Okay'),
                    handler: function () {
                        _this.navCtrl.setRoot("DashboardPage");
                    }
                }]
        });
        alerttemp.present();
    };
    AddDevicesPage.prototype.showVehicleDetails = function (vdata) {
        this.navCtrl.push('VehicleDetailsPage', {
            param: vdata,
            option_switch: this.option_switch
        });
        this.showDrawer = false;
    };
    AddDevicesPage.prototype.doRefresh = function (refresher) {
        var that = this;
        that.page = 0;
        that.limit = 5;
        console.log('Begin async operation', refresher);
        this.getdevices();
        // this.getdevicesTemp();
        refresher.complete();
    };
    AddDevicesPage.prototype.shareVehicle = function (d_data) {
        var that = this;
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            inputs: [
                {
                    name: 'device_name',
                    value: d_data.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: this.translate.instant('Enter Email Id/Mobile Number')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('Cancel'),
                    handler: function () {
                    }
                },
                {
                    text: this.translate.instant('Share'),
                    handler: function (data) {
                        that.sharedevices(data, d_data);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.sharedevices = function (data, d_data) {
        var _this = this;
        var that = this;
        var devicedetails = {
            "did": d_data._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    AddDevicesPage.prototype.showDeleteBtn = function (b) {
        // debugger;
        var that = this;
        if (localStorage.getItem('isDealervalue') == 'true') {
            return false;
        }
        else {
            if (b) {
                var u = b.split(",");
                for (var p = 0; p < u.length; p++) {
                    if (that.islogin._id == u[p]) {
                        return true;
                    }
                }
            }
            else {
                return false;
            }
        }
    };
    AddDevicesPage.prototype.sharedVehicleDelete = function (device) {
        var that = this;
        that.deivceId = device;
        var alert = that.alertCtrl.create({
            message: this.translate.instant('Do you want to delete this share vehicle ?'),
            buttons: [{
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        that.removeDevice(that.deivceId._id);
                    }
                },
                {
                    text: this.translate.instant('NO')
                }]
        });
        alert.present();
    };
    AddDevicesPage.prototype.removeDevice = function (did) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
            .subscribe(function (data) {
            console.log(data);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Shared Device was deleted successfully!'),
                duration: 1500
            });
            toast.onDidDismiss(function () {
                // this.getdevices();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AddDevicesPage.prototype.showSharedBtn = function (a, b) {
        // debugger
        if (b) {
            return !(b.split(",").indexOf(a) + 1);
        }
        else {
            return true;
        }
    };
    AddDevicesPage.prototype.presentPopover = function (ev, data) {
        console.log("populated=> " + JSON.stringify(data));
        var popover = this.popoverCtrl.create(PopoverPage, {
            vehData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            // this.getdevices();
        });
        popover.present({
            ev: ev
        });
    };
    AddDevicesPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            var baseURLp;
            if (that.stausdevice) {
                baseURLp = _this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
            }
            else {
                baseURLp = _this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
            }
            if (_this.islogin.isSuperAdmin == true) {
                baseURLp += '&supAdmin=' + _this.islogin._id;
            }
            else {
                if (_this.islogin.isDealer == true) {
                    baseURLp += '&dealer=' + _this.islogin._id;
                }
            }
            that.ndata = [];
            that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(function (res) {
                if (res.devices.length > 0) {
                    that.ndata = res.devices;
                    for (var i = 0; i < that.ndata.length; i++) {
                        // that.allDevices.push(that.ndata[i]);
                        that.allDevicesSearch.push(that.ndata[i]);
                    }
                    // that.allDevicesSearch = that.allDevices;
                }
                infiniteScroll.complete();
            }, function (error) {
                console.log(error);
            });
        }, 100);
    };
    AddDevicesPage.prototype.progressBar = function () {
        var _this = this;
        // Test interval to show the progress bar
        this.progressIntervalId = setInterval(function () {
            if (_this.loadProgress < 100) {
                _this.loadProgress += 1;
                // console.log("progress: ", this.loadProgress);
            }
            else {
                clearInterval(_this.loadProgress);
            }
        }, 100);
    };
    // getdevicesTemp() {
    //   var baseURLp;
    //   if (this.stausdevice) {
    //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
    //   }
    //   else {
    //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    //   }
    //   if (this.islogin.isSuperAdmin == true) {
    //     baseURLp += '&supAdmin=' + this.islogin._id;
    //   } else {
    //     if (this.islogin.isDealer == true) {
    //       baseURLp += '&dealer=' + this.islogin._id;
    //     }
    //   }
    //   // this.apiCall.toastMsgStarted();
    //   // this.apiCall.startLoading().present();
    //   this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
    //     .subscribe(data => {
    //       // this.apiCall.stopLoading();
    //       // this.apiCall.toastMsgDismised();
    //       this.ndata = data.devices;
    //       this.allDevices = this.ndata;
    //       this.allDevicesSearch = this.ndata;
    //       let that = this;
    //       that.userPermission = JSON.parse(localStorage.getItem('details'));
    //       if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
    //         that.option_switch = true;
    //       } else {
    //         if (localStorage.getItem('isDealervalue') == 'true') {
    //           that.option_switch = true;
    //         } else {
    //           if (that.userPermission.isDealer == false) {
    //             that.option_switch = false;
    //           }
    //         }
    //       }
    //     },
    //       err => {
    //         console.log("error=> ", err);
    //         // this.apiCall.stopLoading();
    //         // this.apiCall.toastMsgDismised();
    //       });
    // }
    AddDevicesPage.prototype.livetrack = function (device) {
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        localStorage.setItem("LiveDevice", "LiveDevice");
        var animationsOptions;
        if (this.plt.is('android')) {
            this.navCtrl.push('LiveSingleDevice', { device: device });
        }
        else {
            if (this.plt.is('ios')) {
                animationsOptions = {
                    animation: 'ios-transition',
                    duration: 1000
                };
                this.navCtrl.push('LiveSingleDevice', { device: device }, animationsOptions);
            }
        }
    };
    AddDevicesPage.prototype.showHistoryDetail = function (device) {
        this.navCtrl.push('HistoryDevicePage', {
            device: device
        });
        if (this.showDrawer) {
            this.showDrawer = false;
        }
    };
    AddDevicesPage.prototype.device_address = function (device, index) {
        var _this = this;
        var that = this;
        var tempcord = {};
        if (!device.last_location) {
            that.allDevicesSearch[index].address = "N/A";
            if (!device.last_lat && !device.last_lng) {
                that.allDevicesSearch[index].address = "N/A";
            }
            else {
                tempcord = {
                    "lat": device.last_lat,
                    "long": device.last_lng
                };
                this.apiCall.getAddress(tempcord)
                    .subscribe(function (res) {
                    if (res.message === "Address not found in databse") {
                        _this.geocoderApi.reverseGeocode(device.last_lat, device.last_lng)
                            .then(function (res) {
                            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                            that.saveAddressToServer(str, device.last_lat, device.last_lng);
                            that.allDevicesSearch[index].address = str;
                            // console.log("inside", that.address);
                        });
                    }
                    else {
                        that.allDevicesSearch[index].address = res.address;
                    }
                });
            }
        }
        else {
            tempcord = {
                "lat": device.last_location.lat,
                "long": device.last_location.long
            };
            this.apiCall.getAddress(tempcord)
                .subscribe(function (res) {
                if (res.message === "Address not found in databse") {
                    _this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
                        .then(function (res) {
                        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                        that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
                        // that.allDevices[index].address = str;
                        that.allDevicesSearch[index].address = str;
                        // console.log("inside", that.address);
                    });
                }
                else {
                    // that.allDevices[index].address = res.address;
                    that.allDevicesSearch[index].address = res.address;
                }
            });
        }
    };
    // device_address(device, index) {
    //   let that = this;
    //   if (!device.last_location) {
    //     that.allDevices[index].address = "N/A";
    //     return;
    //   }
    //   let tempcord = {
    //     "lat": device.last_location.lat,
    //     "long": device.last_location.long
    //   }
    //   this.apiCall.getAddress(tempcord)
    //     .subscribe(res => {
    //       if (res.message === "Address not found in databse") {
    //         this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
    //           .then(res => {
    //             var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
    //             that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
    //             that.allDevices[index].address = str;
    //             // console.log("inside", that.address);
    //           })
    //       } else {
    //         that.allDevices[index].address = res.address;
    //       }
    //     })
    // }
    AddDevicesPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    AddDevicesPage.prototype.getDuration = function (device, index) {
        var that = this;
        that.allDevicesSearch[index].duration = "0hrs 0mins";
        if (!device.status_updated_at) {
            // console.log("did not found last ping on")
            that.allDevicesSearch[index].duration = "0hrs 0mins";
        }
        else if (device.status_updated_at) {
            // console.log("found last ping on")
            var a = __WEBPACK_IMPORTED_MODULE_1_moment__(new Date().toISOString()); //now
            var b = __WEBPACK_IMPORTED_MODULE_1_moment__(new Date(device.status_updated_at).toISOString());
            var mins;
            mins = a.diff(b, 'minutes') % 60;
            that.allDevicesSearch[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
        }
    };
    // getDuration(device, index) {
    //   let that = this;
    //   that.allDevices[index].duration = "0hrs 0mins";
    //   if (!device.status_updated_at) {
    //     // console.log("did not found last ping on")
    //     that.allDevices[index].duration = "0hrs 0mins";
    //   } else if (device.status_updated_at) {
    //     // console.log("found last ping on")
    //     var a = moment(new Date().toISOString());//now
    //     var b = moment(new Date(device.status_updated_at).toISOString());
    //     var mins;
    //     mins = a.diff(b, 'minutes') % 60;
    //     that.allDevices[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
    //   }
    // }
    AddDevicesPage.prototype.callSearch = function (ev) {
        var _this = this;
        var searchKey = ev.target.value;
        var _baseURL;
        if (this.islogin.isDealer == true) {
            _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        }
        else {
            if (this.islogin.isSuperAdmin == true) {
                _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
            }
            else {
                _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
            }
        }
        this.apiCall.callSearchService(_baseURL)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            console.log("search result=> " + JSON.stringify(data));
            _this.allDevicesSearch = data.devices;
            _this.allDevices = data.devices;
            // console.log("fuel percentage: " + data.devices[0].fuel_percent)
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.checkImmobilizePassword = function () {
        var _this = this;
        var rurl = this.apiCall.mainUrl + 'users/get_user_setting';
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(rurl, Var)
            .subscribe(function (data) {
            if (!data.engine_cut_psd) {
                _this.checkedPass = 'PASSWORD_NOT_SET';
            }
            else {
                _this.checkedPass = 'PASSWORD_SET';
            }
        });
    };
    AddDevicesPage.prototype.checkPointsAvailability = function () {
        var url = "https://www.oneqlik.in/users/checkPointAvailablity?u_id=" + this.islogin._id + "&demanded_points=0";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (resp) {
            console.log("points availability: ", resp);
        }, function (err) {
            console.log("error ", err);
        });
    };
    AddDevicesPage.prototype.IgnitionOnOff = function (d) {
        // debugger
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        var pModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__immobilize_modal__["a" /* ImmobilizeModelPage */], {
            param: d
        });
        pModal.onDidDismiss(function () {
            // this.getdevices();
        });
        pModal.present();
        // let that = this;
        // if (d.last_ACC != null || d.last_ACC != undefined) {
        //   if (that.clicked) {
        //     let alert = this.alertCtrl.create({
        //       message: this.translate.instant('Process ongoing..'),
        //       buttons: [this.translate.instant('Okay')]
        //     });
        //     alert.present();
        //   } else {
        //     this.checkImmobilizePassword();
        //     this.messages = undefined;
        //     this.dataEngine = d;
        //     var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        //     this.apiCall.startLoading().present();
        //     this.apiCall.ignitionoffCall(baseURLp)
        //       .subscribe(data => {
        //         this.apiCall.stopLoading();
        //         this.DeviceConfigStatus = data;
        //         this.immobType = data[0].imobliser_type;
        //         if (this.dataEngine.ignitionLock == '1') {
        //           this.messages = this.translate.instant('Do you want to unlock the engine?')
        //         } else {
        //           if (this.dataEngine.ignitionLock == '0') {
        //             this.messages = this.translate.instant('Do you want to lock the engine?')
        //           }
        //         }
        //         let alert = this.alertCtrl.create({
        //           message: this.messages,
        //           buttons: [{
        //             text: 'YES',
        //             handler: () => {
        //               if (this.immobType == 0 || this.immobType == undefined) {
        //                 that.clicked = true;
        //                 var devicedetail = {
        //                   "_id": this.dataEngine._id,
        //                   "engine_status": !this.dataEngine.engine_status
        //                 }
        //                 // this.apiCall.startLoading().present();
        //                 this.apiCall.deviceupdateCall(devicedetail)
        //                   .subscribe(response => {
        //                     // this.apiCall.stopLoading();
        //                     this.editdata = response;
        //                     const toast = this.toastCtrl.create({
        //                       message: response.message,
        //                       duration: 2000,
        //                       position: 'top'
        //                     });
        //                     toast.present();
        //                     // this.responseMessage = "Edit successfully";
        //                     this.getdevices();
        //                     var msg;
        //                     if (!this.dataEngine.engine_status) {
        //                       msg = this.DeviceConfigStatus[0].resume_command;
        //                     }
        //                     else {
        //                       msg = this.DeviceConfigStatus[0].immoblizer_command;
        //                     }
        //                     this.sms.send(d.sim_number, msg);
        //                     const toast1 = this.toastCtrl.create({
        //                       message: this.translate.instant('SMS sent successfully'),
        //                       duration: 2000,
        //                       position: 'bottom'
        //                     });
        //                     toast1.present();
        //                     that.clicked = false;
        //                   },
        //                     error => {
        //                       that.clicked = false;
        //                       // this.apiCall.stopLoading();
        //                       console.log(error);
        //                     });
        //               } else {
        //                 console.log("Call server code here!!")
        //                 if (that.checkedPass === 'PASSWORD_SET') {
        //                   this.askForPassword(d);
        //                   return;
        //                 }
        //                 that.serverLevelOnOff(d);
        //               }
        //             }
        //           },
        //           {
        //             text: this.translate.instant('NO')
        //           }]
        //         });
        //         alert.present();
        //       },
        //         error => {
        //           this.apiCall.stopLoading();
        //           console.log("some error: ", error._body.message);
        //         });
        //   }
        // }
    };
    ;
    AddDevicesPage.prototype.askForPassword = function (d) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Enter Password',
            message: "Enter password for engine cut",
            inputs: [
                {
                    name: 'password',
                    placeholder: 'Password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Proceed',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("data: ", data);
                        // if (data.password !== data.cpassword) {
                        //   this.toastmsg("Entered password and confirm password did not match.")
                        //   return;
                        // }
                        _this.verifyPassword(data, d);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.verifyPassword = function (pass, d) {
        var _this = this;
        var ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
        var payLd = {
            "uid": this.islogin._id,
            "psd": pass.password
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(ryurl, payLd)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log(resp);
            if (resp.message === 'password not matched') {
                _this.toastmsg(resp.message);
                return;
            }
            _this.serverLevelOnOff(d);
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.toastmsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        }).present();
    };
    AddDevicesPage.prototype.serverLevelOnOff = function (d) {
        var _this = this;
        var that = this;
        that.clicked = true;
        var data = {
            "imei": d.Device_ID,
            "_id": this.dataEngine._id,
            "engine_status": d.ignitionLock,
            "protocol_type": d.device_model.device_type
        };
        // this.apiCall.startLoading().present();
        this.apiCall.serverLevelonoff(data)
            .subscribe(function (resp) {
            // this.apiCall.stopLoading();
            console.log("ignition on off=> ", resp);
            _this.respMsg = resp;
            // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
            _this.intervalID = setInterval(function () {
                _this.apiCall.callResponse(_this.respMsg._id)
                    .subscribe(function (data) {
                    console.log("interval=> " + data);
                    _this.commandStatus = data.status;
                    if (_this.commandStatus == 'SUCCESS') {
                        clearInterval(_this.intervalID);
                        that.clicked = false;
                        // this.apiCall.stopLoadingnw();
                        var toast1 = _this.toastCtrl.create({
                            message: _this.translate.instant('process has been completed successfully!'),
                            duration: 1500,
                            position: 'bottom'
                        });
                        toast1.present();
                        // this.getdevices();
                    }
                });
            }, 5000);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error in onoff=>", err);
            that.clicked = false;
        });
    };
    AddDevicesPage.prototype.dialNumber = function (number) {
        if (this.showDrawer) {
            this.showDrawer = false;
        }
        window.open('tel:' + number, '_system');
    };
    AddDevicesPage.prototype.getItems = function (ev) {
        var val = ev.target.value.trim();
        this.allDevicesSearch = this.allDevices.filter(function (item) {
            return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.allDevicesSearch);
    };
    AddDevicesPage.prototype.onClear = function (ev) {
        this.getdevices();
        // this.getdevicesTemp();
        ev.target.value = '';
        // this.toggled = false;
    };
    AddDevicesPage.prototype.openAdddeviceModal = function () {
        var profileModal = this.modalCtrl.create('AddDeviceModalPage');
        profileModal.onDidDismiss(function (data) {
            console.log(data);
            // this.getdevices();
        });
        profileModal.present();
    };
    // IgnitionOnOff() {
    //   let pModal = this.modalCtrl.create('ImmobilizeModelPage');
    //   pModal.onDidDismiss(data => {
    //     console.log(data);
    //   })
    // }
    AddDevicesPage.prototype.upload = function (vehData) {
        this.navCtrl.push('UploadDocPage', { vehData: vehData });
    };
    AddDevicesPage.prototype.iconCheck = function (status, iconType) {
        var devStatus = status.split(" ");
        if ((iconType == 'car') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/car_blue_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'car') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/car_green_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'car') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/car_red_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'car') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/car_yellow_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'car') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/car_grey_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/bike_blue_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/bike_green_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/bike_red_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/bike_yellow_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bike') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/bike_grey_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/bus_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/bus_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/bus_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/bus_yellow.jpg";
            return this.devIcon;
        }
        else if ((iconType == 'bus') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/bus_gray.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/truck_icon_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/truck_icon_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/truck_icon_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/truck_icon_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'truck') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/truck_icon_grey.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/tractor_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/tractor_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/tractor_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/tractor_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'tractor') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/tractor_gray.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/car_blue_icon.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/car_green_icon.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/car_red_icon.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/car_yellow_icon.png";
            return this.devIcon;
        }
        else if ((!iconType) && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/car_grey_icon.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'user') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/user.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/jcb_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/jcb_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/jcb_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/jcb_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'jcb') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/jcb_gray.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/ambulance_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/ambulance_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/ambulance_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/ambulance_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'ambulance') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/ambulance_gray.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "OUT")) {
            this.devIcon = "../../assets/imgs/auto_blue.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "RUNNING")) {
            this.devIcon = "../../assets/imgs/auto_green.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "STOPPED")) {
            this.devIcon = "../../assets/imgs/auto_red.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "IDLING")) {
            this.devIcon = "../../assets/imgs/auto_yellow.png";
            return this.devIcon;
        }
        else if ((iconType == 'auto') && (devStatus[0] == "NO" || devStatus[0] == "Expired")) {
            this.devIcon = "../../assets/imgs/auto_gray.png";
            return this.devIcon;
        }
        else {
            this.devIcon = "../../assets/imgs/noIcon.png";
            return this.devIcon;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])("step"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"])
    ], AddDevicesPage.prototype, "steps", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])('myGauge'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"])
    ], AddDevicesPage.prototype, "myGauge", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Navbar"])
    ], AddDevicesPage.prototype, "navBar", void 0);
    AddDevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/add-devices/add-devices.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title (click)="toggled = false">{{ "Vehicle List" | translate }}</ion-title>\n    <ion-buttons end>\n      <div>\n        <ion-icon style="font-size: 2.2em;" color="light" *ngIf="!toggled" (click)="toggle()" name="search"></ion-icon>\n        <ion-searchbar *ngIf="toggled" (ionInput)="callSearch($event)" (ionClear)="onClear($event)"\n          (ionCancel)="cancelSearch($event)"></ion-searchbar>\n      </div>\n    </ion-buttons>\n    <ion-buttons end *ngIf="((dealer_Permission == true)||(dealer_Permission === undefined)) || isSuperAdmin">\n      <button ion-button icon-only (click)="openAdddeviceModal()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <progress-bar *ngIf="loadProgress !== 100" [progress]="loadProgress"></progress-bar>\n  <ion-segment mode="md" color="gpsc" scrollable="true" (ionChange)="segmentChanged($event)" [(ngModel)]="checkIfStat">\n    <ion-segment-button class="seg" style="color: #f0810f;" value="ALL">\n      <ion-icon name="car" style="color: #f0810f;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #11a46e;" value="RUNNING">\n      <ion-icon name="car" style="color: #11a46e;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #ff3f32;" value="STOPPED">\n      <ion-icon name="car" style="color: #ff3f32;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #ffc13c;" value="IDLING">\n      <ion-icon name="car" style="color: #ffc13c;"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button style="color: #1bb6d4;" value="OUT OF REACH">\n      <ion-icon name="car" style="color: #1bb6d4;"></ion-icon>\n    </ion-segment-button>\n  </ion-segment>\n\n  <!-- <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar> -->\n</ion-header>\n\n<ion-content *ngIf="!hideMe">\n  <!-- <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n      refreshingText="{{\'Refreshing...\' | translate}}">\n    </ion-refresher-content>\n  </ion-refresher> -->\n\n  <!-- Default Spinner -->\n  <!-- <div *ngIf="(loadProgress !== 100) && (allDevicesSearch === undefined || allDevicesSearch.length === 0)" style="text-align: center; margin-top: 2%;"> -->\n  <div *ngIf="(allDevicesSearch.length === 0) && !showEmptyStatus" style="text-align: center; margin-top: 2%;">\n    <ion-spinner color="gpsc"></ion-spinner>\n    <p>Loading...</p>\n  </div>\n\n  <!-- <div *ngIf="(loadProgress === 100) && (allDevicesSearch.length === 0)" style="text-align: center; margin-top: 2%;"> -->\n  <div *ngIf="showEmptyStatus" style="text-align: center; margin-top: 2%;">\n    <p>No <span *ngIf="checkIfStat != \'ALL\'">{{checkIfStat | titlecase}}</span> vehicle(s) found.</p>\n  </div>\n\n  <div *ngIf="allDevicesSearch.length > 0">\n    <ion-card *ngFor="let d of allDevicesSearch; let i = index">\n\n      <div *ngIf="d.expiration_date < now" style="position: relative">\n        <div>\n          <ion-item style="background-color: rgba(0, 0, 0, 0.9)">\n            <ion-avatar item-start *ngIf="iconCheck(d.status,d.iconType)">\n              <img *ngIf="d.iconType === \'jcb\'" width=\'30px\' style="margin-left: 10px;" src={{devIcon}}>\n              <img *ngIf="d.iconType != \'jcb\'" width=\'20px\' style="margin-left: 10px;" src={{devIcon}}>\n            </ion-avatar>\n            <div>\n              <h2 style="color: gray">{{ d.Device_Name }}</h2>\n              <p style="color:gray;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n                <span style="text-transform: uppercase; color: red;">{{ d.status }}\n                </span>\n                <span *ngIf="d.status_updated_at">{{ "since" | translate }}&nbsp;{{\n                  d.status_updated_at | date: "medium"\n                }}\n                </span>\n              </p>\n            </div>\n          </ion-item>\n          <ion-row style="background-color: rgba(0, 0, 0, 0.9)" padding-right>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center"></ion-col>\n            <ion-col width-20 style="text-align:center">\n              <button ion-button outline color="secondary" (tap)="activateVehicle(d)">\n                Activate\n              </button></ion-col>\n          </ion-row>\n        </div>\n      </div>\n\n      <div *ngIf="d.expiration_date == null || d.expiration_date > now" (click)="showBottomDrawer(d)">\n        <ion-item style="padding: 0px;">\n          <ion-avatar item-start *ngIf="iconCheck(d.status,d.iconType)">\n\n            <img *ngIf="d.iconType === \'jcb\'" width=\'30px\' style="margin-left: 10px;" src={{devIcon}} />\n            <img *ngIf="d.iconType != \'jcb\'" width=\'20px\' style="margin-left: 10px;" src={{devIcon}} />\n          </ion-avatar>\n          <div>\n            <p style="color: black; font-size: 4vw; padding-right: 8px;">\n              {{ d.Device_Name }} &nbsp;&nbsp;\n            </p>\n\n            <p style="color:#cf1c1c;font-size:8px;font-weight: 400;margin:0px;" ion-text text-wrap\n              (onCreate)="getDuration(d, i)">\n              <span style="text-transform: uppercase; color:#11a46e"\n                *ngIf="d.status === \'RUNNING\'">{{ d.status | lowercase }} </span>\n              <span style="text-transform: uppercase; color:#1bb6d4"\n                *ngIf="d.status === \'OUT OF REACH\'">{{ d.status | lowercase }} </span>\n              <span style="text-transform: uppercase; color:#ff3f32"\n                *ngIf="d.status === \'STOPPED\'">{{ d.status | lowercase }} </span>\n              <span style="text-transform: uppercase; color:#ffc13c"\n                *ngIf="d.status === \'IDLING\'">{{ d.status | lowercase }} </span>\n              <span style="text-transform: uppercase; color:#5a5e5c"\n                *ngIf="d.status === \'NO DATA\'">{{ d.status | lowercase }} </span>\n              <span *ngIf="d.last_ping_on">{{ "from" | translate }}&nbsp;{{\n                d.duration}} <br /> {{" Last updated - " | translate}} {{d.last_ping_on | date:\'shortTime\'}},\n                {{d.last_ping_on | date: \'mediumDate\'}}\n              </span>\n            </p>\n          </div>\n\n          <!-- <div item-end *ngIf="islogin.fuel_unit === \'PERCENTAGE\'">\n          <div *ngIf="d.fuel_percent">\n            <ion-avatar class="ava">\n              <img src="assets/imgs/fuel.png" />\n            </ion-avatar>\n            <p style="color: green; font-size: 8px;font-weight: 400;margin:0px;">\n              <b>{{ d.fuel_percent }}%</b>\n            </p>\n          </div>\n        </div> -->\n          <div item-end style="padding: 3px;" *ngIf="islogin.fuel_unit === \'PERCENTAGE\'">\n            <ion-row>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <img src="assets/imgs/fuel.png" style="width: 25px; height: 25px; margin: auto;" />\n                <!-- <ion-icon name="speedometer" color="gpsc" style="font-size: 1.5em;"></ion-icon> -->\n              </ion-col>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <p style="margin: 0px;font-weight: 600;">{{ d.fuel_percent ? d.fuel_percent : N/A }} <span\n                    style="font-size: 0.7em;font-weight: 600;">%</span></p>\n              </ion-col>\n            </ion-row>\n          </div>\n\n          <div item-end style="padding: 3px;" *ngIf="islogin.fuel_unit === \'LITRE\'">\n            <ion-row>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <img src="assets/imgs/fuel.png" style="width: 25px; height: 25px; margin: auto;" />\n                <!-- <ion-icon name="speedometer" color="gpsc" style="font-size: 1.5em;"></ion-icon> -->\n              </ion-col>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <p style="margin: 0px;font-weight: 600;">{{ d.currentFuel ? d.currentFuel : 0 }} <span\n                    style="font-size: 0.7em;font-weight: 600;">L</span></p>\n              </ion-col>\n            </ion-row>\n          </div>\n\n          <!-- <div item-end *ngIf="islogin.fuel_unit === \'LITRE\'">\n          <div *ngIf="d.currentFuel">\n            <ion-avatar class="ava">\n              <img src="assets/imgs/fuel.png" />\n            </ion-avatar>\n            <p style="color: green; font-size: 8px;font-weight: 400;margin:0px;">\n              <b>{{ d.currentFuel }}L</b>\n            </p>\n          </div>\n        </div> -->\n\n          <div item-end style="padding: 3px;">\n            <ion-row>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <ion-icon name="speedometer" color="gpsc" style="font-size: 1.4em;"></ion-icon>\n              </ion-col>\n              <ion-col col-12 style="text-align: center; padding: 0px;">\n                <p style="margin: 0px;font-weight: 600;">{{ d.last_speed ? d.last_speed : 0 }} <span\n                    style="font-size: 0.7em;font-weight: 600;">KMPH</span></p>\n              </ion-col>\n            </ion-row>\n            <!-- <ion-badge color="gpsc" style="font-size: 0.6em;">{{ d.last_speed }} km/h</ion-badge> -->\n          </div>\n\n          <div *ngIf="!showDeleteBtn(d.SharedWith)" item-end>\n            <button ion-button clear color="gpsc" (click)="presentPopover($event, d); $event.stopPropagation()"\n              *ngIf="option_switch" style="font-size: 4vw; margin: 0px;">\n              <ion-icon ios="ios-more" md="md-more"></ion-icon>\n            </button>\n          </div>\n\n          <!-- <button ion-button item-end clear color="gpsc" (click)="shareVehicle(d)" *ngIf="!option_switch"\n          style="font-size: 4vw; margin: 0px;">\n          <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id, d.SharedWith)"></ion-icon>\n        </button> -->\n\n          <div *ngIf="showDeleteBtn(d.SharedWith)">\n            <button ion-button item-end clear color="danger" (click)="sharedVehicleDelete(d)"\n              style="font-size: 4vw; margin: 0px;">\n              <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n            </button>\n          </div>\n        </ion-item>\n        <!-- <ion-item item-start class="itemStyle" style="background:rgb(215, 216, 218); padding-left: 5px;"> -->\n        <ion-row style="padding-left: 5px;margin-top: -10px;">\n          <ion-col col-1 class="colSt2">\n            <ion-icon name="pin" style="font-size: 0.9em; color: #696666;"></ion-icon>\n          </ion-col>\n          <ion-col col-10 (onCreate)="device_address(d, i)" style="margin-left: -15px;" class="colSt2">\n            <div class="overme">\n              {{ d.address }}\n            </div>\n          </ion-col>\n        </ion-row>\n        <!-- </ion-item> -->\n        <ion-item item-start class="itemStyle" style="padding-left: 5px;">\n          <ion-row style="margin-right: -3px;">\n            <ion-col col-6 style="padding: 10px 0px 0px 0px;">\n              <ion-row>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="d.last_ACC == \'0\'" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="d.last_ACC == \'1\'" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="d.last_ACC == null" width="20" height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="d.ac == null" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="d.ac == \'1\'" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="d.ac == \'0\'" width="20" height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="d.currentFuel != null" width="20"\n                    height="20" />\n                  <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="d.currentFuel == null" width="20"\n                    height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/power_na.png" *ngIf="d.power == null" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="d.power == \'0\'" width="20"\n                    height="20" />\n                  <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="d.power == \'1\'" width="20"\n                    height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="d.gpsTracking == null" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="d.gpsTracking == \'0\'" width="20"\n                    height="20" />\n                  <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="d.gpsTracking == \'1\'" width="20"\n                    height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <ion-icon name="thermometer" *ngIf="d.temp == null" style="color: gray; font-size: 1.4em;">\n                  </ion-icon>\n                  <ion-icon name="thermometer" *ngIf="d.temp == \'0\'" style="font-size: 1.4em; color: #ff3f32;">\n                  </ion-icon>\n                  <ion-icon name="thermometer" *ngIf="d.temp == \'1\'" style="font-size: 1.4em; color: #11a46e;">\n                  </ion-icon>\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <img src="assets/imgs/statusIcons/car_no_data.png" *ngIf="d.door == null" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/car_door_open.png" *ngIf="d.door == 0" width="20" height="20" />\n                  <img src="assets/imgs/statusIcons/car_door_close.png" *ngIf="d.door == 1" width="20" height="20" />\n                </ion-col>\n                <ion-col width-2 style="text-align:center">\n                  <ion-icon name="lock" style="color:#ff3f32;font-size: 1.2em;" *ngIf="d.ignitionLock == \'1\'">\n                  </ion-icon>\n                  <ion-icon name="unlock" style="color:#11a46e;font-size: 1.2em;" *ngIf="d.ignitionLock == \'0\'">\n                  </ion-icon>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n            <!-- <ion-col col-1 class="colStyle">\n            <div class="trd">\n              <div class="divc"></div>\n            </div>\n          </ion-col> -->\n            <ion-col col-6 class="colSt123">\n              <p style="color:#fff;font-size: 10px;font-weight: 400;margin-top: 6px; margin-left: 49%;">\n                {{ "Today\'s Odo" | translate }}\n              </p>\n              <p style="color:#fff;font-size: 11px;font-weight: bold;margin-top: 6px; margin-left: 49%;">\n                {{ d.today_odo | number: "1.0-2" }} {{ "Kms" | translate }}\n              </p>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n        <ion-row style="background-color: #000;\n      color: white;\n      font-weight: bold;font-size: 0.6em;">\n          <ion-col col-6>Licence Purchased On - {{d.created_on | date:\'mediumDate\'}}</ion-col>\n          <ion-col col-6 style="text-align: right;">Licence Expired On - {{d.expiration_date | date:\'mediumDate\'}}\n          </ion-col>\n        </ion-row>\n      </div>\n    </ion-card>\n  </div>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="{{ \'Loading more data...\' | translate }}">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n<div padding>\n  <ion-bottom-drawer *ngIf="showDrawer" [(state)]="drawerState" [minimumHeight]="minimumHeight"\n    [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce" [distanceTop]="distanceTop">\n    <div class="drawer-content">\n      <div id="content">\n        <ion-row style="padding-bottom: 16px;">\n          <ion-col col-4></ion-col>\n          <ion-col col-4 style="border-top: 3px solid lightgray;"></ion-col>\n          <ion-col col-4></ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="livetrack(drawerData)">\n            <img src="assets/imgs/segment/map_active.png" style="width: 30px;" />\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="showHistoryDetail(drawerData)">\n            <img src="assets/imgs/segment/history_active.png" style="width: 30px;" />\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="fonctionTest(drawerData)">\n            <img *ngIf="drawerData.theftAlert"\n              src="assets/imgs/anti-theft_anti_theft_protection_hacker_investigate-512.png" style="width: 30px;" />\n            <img *ngIf="!drawerData.theftAlert" src="assets/imgs/anti-theft_anti_theftred.png" style="width: 30px;" />\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="IgnitionOnOff(drawerData)">\n            <ion-icon name="lock" style="color:#ef473a;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'1\'">\n            </ion-icon>\n            <ion-icon name="unlock" style="color:#1de21d;font-size: 2em;" *ngIf="drawerData.ignitionLock == \'0\'">\n            </ion-icon>\n          </ion-col>\n        </ion-row>\n        <ion-row style="padding-bottom: 10px;">\n          <ion-col col-3 no-padding style="text-align: center;">Live</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">History</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">Parking</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">Immobilize</ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="towAlertCall(drawerData)">\n            <ion-icon name="alert" style="color:#ef473a;font-size: 2em;" *ngIf="!drawerData.towAlert"></ion-icon>\n            <ion-icon name="alert" style="color:#1de21d;font-size: 2em;" *ngIf="drawerData.towAlert"></ion-icon>\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="dialNumber(drawerData.contact_number)">\n            <ion-icon name="call" style="color:#3a8eef;font-size: 2em;"></ion-icon>\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" (click)="showVehicleDetails(drawerData)">\n            <ion-icon name="stats" style="color:#fc8803;font-size: 2em;"></ion-icon>\n          </ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" *ngIf="!option_switch"\n            (click)="shareVehicle(drawerData)">\n            <ion-icon ios="ios-share" md="md-share" color="gpsc" style="font-size: 2em;"\n              *ngIf="showSharedBtn(islogin._id, drawerData.SharedWith)"></ion-icon>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-3 no-padding style="text-align: center;">Tow</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">Driver</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;">Analytics</ion-col>\n          <ion-col col-3 no-padding style="text-align: center;" *ngIf="!option_switch">Share</ion-col>\n        </ion-row>\n      </div>\n    </div>\n  </ion-bottom-drawer>\n</div>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/add-devices/add-devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__["a" /* SQLite */]])
    ], AddDevicesPage);
    return AddDevicesPage;
}());

var PopoverPage = /** @class */ (function () {
    function PopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl, translate) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.vehData = navParams.get("vehData");
        console.log("popover data=> ", this.vehData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    PopoverPage.prototype.ngOnInit = function () { };
    PopoverPage.prototype.editItem = function () {
        var _this = this;
        console.log("edit");
        var modal = this.modalCtrl.create('UpdateDevicePage', {
            vehData: this.vehData
        });
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.viewCtrl.dismiss();
        });
        modal.present();
    };
    // valScreen(d_id) {
    //   let modal = this.modalCtrl.create('DailyReportNewPage', {
    //     vehData: this.vehData
    //   });
    //   console.log(this.vehData);
    //   modal.onDidDismiss(() => {
    //     console.log("modal dismissed!")
    //     this.viewCtrl.dismiss();
    //   })
    //   modal.present();
    // }
    PopoverPage.prototype.deleteItem = function () {
        var that = this;
        console.log("delete");
        var alert = this.alertCtrl.create({
            message: this.translate.instant('Do you want to delete this vehicle ?'),
            buttons: [{
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: this.translate.instant('NO')
                }]
        });
        alert.present();
    };
    PopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.deleteDeviceCall(d_id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Vehicle deleted successfully!'),
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    PopoverPage.prototype.shareItem = function () {
        var that = this;
        console.log("share");
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Share Vehicle'),
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'device_name',
                    value: that.vehData.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: this.translate.instant('Enter Email Id/Mobile Number')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('Cancel'),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.translate.instant('Share'),
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("clicked=> ", data);
                        that.sharedevices(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    // valScreen() {
    //   let that = this
    //   console.log("selected=>", that.vehData);
    //   localStorage.selectedVal = JSON.stringify(that.vehData);
    //   this.navCtrl.push('DailyReportNewPage');
    //   // if(this.vehData == "Device_Name" ) {
    //   //   console.log(this.vehData);
    //   //   this.navCtrl.push('DailyReportNewPage');
    //   // }
    // }
    PopoverPage.prototype.sharedevices = function (data) {
        var _this = this;
        var that = this;
        console.log(data.shareId);
        var devicedetails = {
            "did": that.vehData._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            // var editdata = data;
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;{{'edit' | translate}}\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteItem()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;{{'delete' | translate}}\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;{{'share' | translate}}\n      </ion-item>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=add-devices.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UploadDocPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_tinyurl__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_tinyurl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_tinyurl__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UploadDocPage = /** @class */ (function () {
    function UploadDocPage(navCtrl, navParams, viewCtrl, platform, toastCtrl, loadingCtrl, apiCall, modalCtrl, popoverCtrl, event) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.event = event;
        this._docTypeList = [];
        this.imgData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.vehData = this.navParams.get("vehData");
        this.event.subscribe("reloaddoclist", function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
    }
    UploadDocPage.prototype.ngOnInit = function () {
        this.checkDevice();
    };
    UploadDocPage.prototype._addDocument = function () {
        this.navCtrl.push("AddDocPage", { vehData: this.vehData });
    };
    UploadDocPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter UploadDocPage');
    };
    UploadDocPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UploadDocPage.prototype.onSelectChange = function (selectedValue) {
        console.log('Selected', selectedValue);
        this.docType = selectedValue;
    };
    UploadDocPage.prototype.brows = function () {
        this.docImge = true;
    };
    UploadDocPage.prototype.showToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    };
    UploadDocPage.prototype.viewDocFunc = function (imgData) {
        // let that = this;
        console.log("imgData: ", imgData);
        if (imgData == undefined) {
            this.showToast('Data not found');
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__["a" /* ViewDoc */], {
                param1: imgData
            });
            modal.present();
        }
    };
    UploadDocPage.prototype.checkDevice = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getsingledevice(this.vehData.Device_ID)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data.imageDoc.length > 0) {
                // console.log("imagedoc data: " + JSON.stringify(data.imageDoc))
                for (var i = 0; i < data.imageDoc.length; i++) {
                    _this._docTypeList.push({
                        docname: data.imageDoc[i].doctype,
                        expDate: data.imageDoc[i].docdate ? data.imageDoc[i].docdate : 'N/A',
                        imageURL: data.imageDoc[i].image
                    });
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UploadDocPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var that = this;
        var popover = this.popoverCtrl.create(DocPopoverPage, {
            vehData: that.vehData,
            imgData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
        popover.present({
            ev: ev
        });
    };
    UploadDocPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-upload-doc',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/add-devices/upload-doc/upload-doc.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ "Documents" | translate }}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="_addDocument()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-list *ngIf="_docTypeList.length > 0">\n    <ion-item *ngFor="let doc of _docTypeList" (click)="viewDocFunc(doc)">\n      <ion-avatar item-start>\n        <img *ngIf="doc.docname == \'RC\'" src="assets/imgs/rc.png">\n        <img *ngIf="doc.docname == \'PUC\'" src="assets/imgs/puc.png">\n        <img *ngIf="doc.docname == \'Insurance\'" src="assets/imgs/insurence.png">\n        <img *ngIf="doc.docname == \'Licence\'" src="assets/imgs/dl.png">\n        <img *ngIf="doc.docname == \'Fitment\'" src="assets/imgs/rc.png">\n        <img *ngIf="doc.docname == \'Permit\'" src="assets/imgs/rc.png">\n      </ion-avatar>\n      <h1>{{doc.docname}}</h1>\n      <p>{{ "Expire On*" | translate }} &nbsp;\n        <span *ngIf="doc.expDate != \'N/A\'">{{doc.expDate | date:\'mediumDate\'}}</span>\n        <span *ngIf="doc.expDate == \'N/A\'">{{doc.expDate}}</span>\n      </p>\n      <button ion-button item-end small icon-only color="gpsc" (click)="presentPopover($event, doc); $event.stopPropagation()">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-item>\n  </ion-list>\n  <ion-card *ngIf="_docTypeList.length == 0">\n    <ion-card-content>\n      {{ " No doucments uploaded yet... Upload documents of your vehicle by pressing above + button" | translate }}\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/add-devices/upload-doc/upload-doc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], UploadDocPage);
    return UploadDocPage;
}());

var DocPopoverPage = /** @class */ (function () {
    function DocPopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl, socialSharing) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.socialSharing = socialSharing;
        this.vehData = {};
        this.imgData = {};
        this.vehData = navParams.get("vehData");
        this.imgData = navParams.get("imgData");
        console.log("popover data=> ", this.imgData);
        var str = this.imgData.imageURL;
        var str1 = str.split('public/');
        this.link = this.apiCall.mainUrl + str1[1];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    DocPopoverPage.prototype.ngOnInit = function () { };
    DocPopoverPage.prototype.editItem = function () {
        console.log("edit");
    };
    DocPopoverPage.prototype.deleteDoc = function () {
        var that = this;
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this Document?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    DocPopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        var that = this;
        var payload = {
            devId: d_id,
            doctype: that.imgData.docname
        };
        this.apiCall.startLoading().present();
        var baseUrl = this.apiCall.mainUrl + "devices/deleteDocuments";
        this.apiCall.urlpasseswithdata(baseUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Document deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    DocPopoverPage.prototype.shareItem = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5_tinyurl__["shorten"](this.link).then(function (res) {
            // alert("tinyurl: " + res);
            // that.socialSharing.share(that.islogin.fn + " " + that.islogin.ln + " has shared " + that.vehicleData.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);
            _this.socialSharing.share(_this.islogin.fn + " " + _this.islogin.ln + " has shared the document with you.", "OneQlik- Vehicle Document", res, "");
        });
    };
    DocPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteDoc()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n      \n    </ion-list>\n  "
        })
        // <ion-item id="step1" class="text-seravek" (click)="upload()">
        //        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;Docs
        //        </ion-item>
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], DocPopoverPage);
    return DocPopoverPage;
}());

//# sourceMappingURL=upload-doc.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FilterPage = /** @class */ (function () {
    function FilterPage(apiCall, viewCtrl, translate
    // private datePicker: DatePicker
    ) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.modal = {};
        this.filterList = [
            { filterID: 1, filterName: this.translate.instant('Overspeeding'), filterValue: 'overspeed', checked: false },
            { filterID: 2, filterName: this.translate.instant('Ignition'), filterValue: 'IGN', checked: false },
            { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
            { filterID: 4, filterName: this.translate.instant('GEOFENCE'), filterValue: 'Geo-Fence', checked: false },
            { filterID: 5, filterName: this.translate.instant('Stoppage'), filterValue: 'MAXSTOPPAGE', checked: false },
            { filterID: 6, filterName: this.translate.instant('Fuel'), filterValue: 'Fuel', checked: false },
            { filterID: 7, filterName: this.translate.instant("AC"), filterValue: 'AC', checked: false },
            { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
            { filterID: 9, filterName: this.translate.instant('Power'), filterValue: 'power', checked: false },
            { filterID: 10, filterName: this.translate.instant('Immobilize'), filterValue: 'immo', checked: false },
            { filterID: 11, filterName: "Theft", filterValue: 'theft', checked: false },
            { filterID: 12, filterName: this.translate.instant('SOS'), filterValue: 'SOS', checked: false },
            { filterID: 13, filterName: 'Reminder', filterValue: 'Reminder', checked: false }
        ];
        this.selectedArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        if (localStorage.getItem("checkedList") != null) {
            this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
            console.log("checked data=> " + this.checkedData);
        }
    }
    FilterPage.prototype.ngOnInit = function () {
        // this.getNotifTypes()
    };
    FilterPage.prototype.getNotifTypes = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getVehicleListCall(this.apiCall.mainUrl + "notifs/getTypes")
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("notif type list: ", data);
            _this.filterList = data;
        });
    };
    FilterPage.prototype.applyFilter = function () {
        localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));
        if (this.selectedArray.length == 0) {
            localStorage.setItem("dates", "dates");
            this.viewCtrl.dismiss(this.modal);
        }
        else {
            localStorage.setItem("types", "types");
            this.viewCtrl.dismiss(this.selectedArray);
        }
    };
    FilterPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage.prototype.selectMember = function (data) {
        console.log("selected=> " + JSON.stringify(data));
        if (data.checked == true) {
            this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.filterID !== data.filterID;
            });
            this.selectedArray = newArray;
        }
    };
    FilterPage.prototype.cancel = function () {
        console.log("do nothing");
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-filter',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/all-notifications/filter/filter.html"*/'<ion-list>\n    <ion-item-group>\n        <ion-item-divider color="light">{{ "Filter Type" | translate }}</ion-item-divider>\n        <div *ngIf="filterList.length > 0">\n            <ion-item *ngFor="let member of filterList">\n                <ion-label>{{member?.filterName || \'N/A\'}}</ion-label>\n                <ion-checkbox color="danger" (click)="selectMember(member)" [(ngModel)]="member.checked"></ion-checkbox>\n            </ion-item>\n        </div>\n    </ion-item-group>\n</ion-list>\n<ion-row style="background-color: darkgray">\n    <ion-col width-50 style="text-align: center; background-color: #f53d3d;">\n        <button ion-button clear small color="light" (click)="cancel()">{{ "Cancel" | translate }}</button>\n    </ion-col>\n    <ion-col width-50 style="text-align: center; background-color: #11b700;">\n        <button ion-button clear small color="light" (click)="applyFilter()">{{ "Apply Filter" | translate }}</button>\n    </ion-col>\n</ion-row>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/all-notifications/filter/filter.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]
            // private datePicker: DatePicker
        ])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportSettingModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportSettingModal = /** @class */ (function () {
    function ReportSettingModal(viewCtrl, apiCall, toastCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.reportArray = [
            { name: "Daily Report", Rstatus: true, Astatus: false, value: "daily_report" },
            { name: "Daywise Report", Rstatus: true, Astatus: false, value: "daywise_report" },
            { name: "Speed Variation", Rstatus: true, Astatus: false, value: "speed_variation" },
            { name: "Fuel Report", Rstatus: true, Astatus: false, value: "fuel_report" },
            { name: "Idle Report", Rstatus: true, Astatus: false, value: "idle_report" },
            { name: "Fuel Consumption Report", Rstatus: true, Astatus: false, value: "fuel_consumption_report" },
            { name: "Trip Report", Rstatus: true, Astatus: false, value: "trip_report" },
            { name: "Travel Path Report", Rstatus: true, Astatus: false, value: "travel_path_report" },
            { name: "Summary Report", Rstatus: true, Astatus: false, value: "summary_report" },
            { name: "Geofence Report", Rstatus: true, Astatus: false, value: "geofence_report" },
            { name: "Overspeed Report", Rstatus: true, Astatus: false, value: "overspeed_report" },
            { name: "Route Violation Report", Rstatus: true, Astatus: false, value: "route_violation_report" },
            { name: "Stoppage Report", Rstatus: true, Astatus: false, value: "stoppage_report" },
            { name: "Ignition Report", Rstatus: true, Astatus: false, value: "ignition_report" },
            { name: "Distance Report", Rstatus: true, Astatus: false, value: "distance_report" },
            { name: "POI Report", Rstatus: true, Astatus: false, value: "poi_report" },
            { name: "SOS Report", Rstatus: true, Astatus: false, value: "sos_report" },
            { name: "AC Report", Rstatus: true, Astatus: false, value: "ac_report" },
            { name: "Driver Performance Report", Rstatus: true, Astatus: false, value: "driver_performance_report" },
            { name: "User Trip Report", Rstatus: true, Astatus: false, value: "user_trip_report" },
            { name: "Alert Report", Rstatus: true, Astatus: false, value: "alert_report" }
        ];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.params = navParams.get('param');
        console.log("navparams: ", this.params);
        this.getCostumerDetail();
    }
    ReportSettingModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ReportSettingModal.prototype.submit = function () {
        var _this = this;
        var report_preference_payload = [];
        report_preference_payload = JSON.parse(JSON.stringify(this.reportArray));
        var tempsemp = {};
        console.log(report_preference_payload);
        for (var index in report_preference_payload) {
            var k = report_preference_payload[index].value;
            if (report_preference_payload[index].Rstatus == false) {
                report_preference_payload[index].Astatus = false;
            }
            tempsemp[k] = report_preference_payload[index];
            delete tempsemp[k].name;
            delete tempsemp[k].value;
        }
        console.log(tempsemp);
        var reportPref = {
            reportsArr: tempsemp,
            id: this.params._id
        };
        var url = this.apiCall.mainUrl + "users/reportPrefrence";
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, reportPref).subscribe(function (res) {
            console.log("check status: ", res);
            _this.apiCall.stopLoading();
            _this.toastCtrl.create({
                message: 'Report preference updated',
                duration: 1000,
                position: 'top'
            }).present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error", err);
        });
    };
    ReportSettingModal.prototype.selectedStat = function (ind, ev, flag) {
        if (flag == 'rVisibility') {
            this.reportArray[ind].Rstatus = ev.checked;
        }
        // console.log('this.reportArray', this.reportArray);
    };
    ReportSettingModal.prototype.getCostumerDetail = function () {
        var that = this;
        this.apiCall.getcustToken(this.params._id).subscribe(function (res) {
            // console.log("resresresresresresresresres", res.cust.report_preference);
            var tempReport = res.cust.report_preference;
            var _loop_1 = function (dt) {
                var arr = that.reportArray.filter(function (d) {
                    return d.value == dt;
                });
                arr[0].Astatus = tempReport[dt].Astatus;
                arr[0].Rstatus = tempReport[dt].Rstatus;
            };
            for (var dt in tempReport) {
                _loop_1(dt);
            }
            // console.log("that.reportArray=>", that.reportArray);
        });
    };
    ReportSettingModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/customers/modals/report-setting/report-setting.html"*/'<ion-content style="background-color: rgba(0, 0, 0, 0.7);">\n    <div class="mainDiv">\n        <div class="secondDiv">\n            <ion-row>\n                <ion-col col-12 text-center>\n                    <h5 style="margin-top: -10px;">REPORT PREFERENCE</h5>\n                </ion-col>\n            </ion-row>\n            <ion-list radio-group [(ngModel)]="min_time">\n                <ion-item *ngFor="let item of reportArray; let i = index">\n                    <ion-label>{{item.name}}</ion-label>\n                    <!-- <md-checkbox type="checkbox" [checked]="devStatus.Rstatus"\n                        (change)="selectedStatus(i,$event,\'rVisibility\')"></md-checkbox> -->\n                    <ion-checkbox item-end color="gpsc" [checked]="item.Rstatus"\n                        (ionChange)="selectedStat(i,$event,\'rVisibility\')"></ion-checkbox>\n                </ion-item>\n            </ion-list>\n            <ion-row>\n                <ion-col col-6>\n                    <div style="padding: 5px;"><button ion-button (click)="submit()" color="gpsc" block>Submit</button>\n                    </div>\n\n                </ion-col>\n                <ion-col col-6>\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="gpsc" block>Dismiss</button>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/customers/modals/report-setting/report-setting.html"*/,
            styles: ["\n    .mainDiv {\n        padding: 50px;\n        \n    }\n\n    .secondDiv {\n        padding-top: 20px;\n        border-radius: 18px;\n        border: 2px solid black;\n        background: white;\n    }"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"]])
    ], ReportSettingModal);
    return ReportSettingModal;
}());

//# sourceMappingURL=report-setting.js.map

/***/ }),

/***/ 414:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DealerPermModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DealerPermModalPage = /** @class */ (function () {
    function DealerPermModalPage(navParams, viewCtrl, apiCall, toastCtrl) {
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.dev_permission = true;
        this.cust_permission = true;
        this.dealerData = navParams.get('param');
        console.log("dealer's data: ", this.dealerData);
        if (this.dealerData.device_add_permission != undefined && this.dealerData.cust_add_permission != undefined) {
            this.dev_permission = this.dealerData.device_add_permission;
            this.cust_permission = this.dealerData.cust_add_permission;
        }
    }
    DealerPermModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    DealerPermModalPage.prototype.submit = function () {
        var _this = this;
        console.log("changed values: ", this.dev_permission);
        console.log("changed values: ", this.cust_permission);
        var url = this.apiCall.mainUrl + "users/setDealerPermission";
        var payload = {
            "device_add_permission": this.dev_permission,
            "cust_add_permission": this.cust_permission,
            "uid": this.dealerData._id
        };
        this.apiCall.urlpasseswithdata(url, payload).subscribe(function (respData) {
            console.log("response data check: ", respData);
            if (respData.message) {
                _this.toastCtrl.create({
                    message: respData.message,
                    duration: 1500,
                    position: 'middle'
                }).present();
                _this.dismiss();
            }
            else {
                _this.toastCtrl.create({
                    message: 'Something went wrong.. Please try after some time.',
                    duration: 1500,
                    position: 'middle'
                }).present();
                _this.dismiss();
            }
        });
    };
    DealerPermModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            template: "\n    <div class=\"mainDiv\">\n        <div class=\"secondDiv\">\n        <div text-center>\n            <h5>Dealer Permission</h5>\n            </div>\n            <ion-list>\n                <ion-item>\n                    <ion-label>Add Vehicle</ion-label>\n                    <ion-toggle [(ngModel)]=\"dev_permission\"></ion-toggle>\n                </ion-item>\n                <ion-item>\n                    <ion-label>Add Customer</ion-label>\n                    <ion-toggle [(ngModel)]=\"cust_permission\"></ion-toggle>\n                </ion-item>\n            </ion-list>\n            <ion-row>\n                <ion-col col-6>\n                    <button ion-button (click)=\"submit()\" color=\"gpsc\" block>Submit</button>\n                </ion-col>\n                <ion-col col-6>\n                    <button ion-button (click)=\"dismiss()\" color=\"grey\" block>Dismiss</button>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n    ",
            styles: ["\n      \n            .mainDiv {\n                padding: 50px;\n                height: 100%;\n                background-color: rgba(0, 0, 0, 0.7);\n            }\n\n            .secondDiv {\n                padding-top: 20px;\n                border-radius: 18px;\n                border: 2px solid black;\n                background: white;\n            }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], DealerPermModalPage);
    return DealerPermModalPage;
}());

//# sourceMappingURL=dealer-perm.js.map

/***/ }),

/***/ 415:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPointsModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddPointsModalPage = /** @class */ (function () {
    function AddPointsModalPage(navParams, viewCtrl, apiCall, toastCtrl) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.min_time = "10";
        this.perunitprice = 0;
        this.pointShareCount = 0;
        this.params = {};
        this.totAllocatedPoints = 0;
        this.totAvailablePoints = 0;
        this.totalAmount = 0;
        this.params = this.navParams.get("params");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // debugger
        if (this.params.point_Allocated) {
            this.totAllocatedPoints = this.params.point_Allocated;
        }
    }
    AddPointsModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.min_time);
    };
    AddPointsModalPage.prototype.ionViewDidEnter = function () {
        this.checkPointsAvailability();
    };
    AddPointsModalPage.prototype.checkPointsAvailability = function () {
        var _this = this;
        var url = "https://www.oneqlik.in/users/checkPointAvailablity?u_id=" + this.islogin._id + "&demanded_points=0";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (res) {
            // console.log("points availability: ", res);
            if ((res.available_points == undefined) || (res.available_points < 0)) {
                _this.totAvailablePoints = 0;
            }
            else {
                _this.totAvailablePoints = res.available_points ? res.available_points : 0;
            }
        }, function (err) {
            console.log("error ", err);
        });
    };
    AddPointsModalPage.prototype.calculateTotal = function () {
        this.totalAmount = this.pointShareCount * this.perunitprice;
        // console.log("calculated amount: ", this.totalAmount)
    };
    AddPointsModalPage.prototype.proceed = function () {
        var _this = this;
        debugger;
        var _bUrl = 'https://www.oneqlik.in/points/addpointAllocation';
        var insertPoint = {
            by: this.islogin._id,
            to: this.params._id,
            points: this.pointShareCount,
            rate: this.perunitprice,
            totalAmount: (this.pointShareCount * this.perunitprice),
            Payment_type: this.payment_stauts,
            created_date: new Date().toISOString(),
        };
        if (this.pointShareCount > this.totAvailablePoints) {
            this.errmsg = "Insufficient Points";
            this.showToast(this.errmsg);
            return;
        }
        else {
            if ((this.pointShareCount == undefined) || (this.payment_stauts == undefined)) {
                this.errmsg = "All Fields are mendatory";
                this.showToast(this.errmsg);
                return;
            }
            else {
                this.apiCall.startLoading().present();
                this.apiCall.urlpasseswithdata(_bUrl, insertPoint).subscribe(function (resp) {
                    _this.apiCall.stopLoading();
                    console.log("get response: ", resp);
                    if (resp.message) {
                        _this.showToast(resp.message);
                        _this.dismiss();
                    }
                }, function (err) {
                    _this.apiCall.stopLoading();
                    console.log("error ", err);
                });
            }
        }
    };
    AddPointsModalPage.prototype.showToast = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'middle'
        }).present();
    };
    AddPointsModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            //template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/dealers/add-points-modal.html"*/'<div class="mainDiv">\n    <div class="secondDiv">\n        <ion-row>\n            <ion-col style="padding: 0px; text-align: center;">\n                <p style="margin: 0px; font-size:20px;">Add Points</p>\n            </ion-col>\n        </ion-row>\n        <div class="temp">\n            <ion-item class="logitem">\n                <ion-input [(ngModel)]="username" type="text">\n                </ion-input>\n            </ion-item>\n        </div>\n        <!-- <ion-item class="logitem1"\n            *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)">\n            <p>{{\'username required!\' | translate}}</p>\n        </ion-item> -->\n        <div class="temp">\n            <ion-item class="logitem">\n                <ion-input [(ngModel)]="password" type="password"></ion-input>\n\n            </ion-item>\n        </div>\n        <!-- <ion-item class="logitem1"\n            *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)">\n            <p>{{\'Password required!\' | translate}}</p>\n        </ion-item> -->\n        <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="gpsc" block>Submit</button></div>\n    </div>\n</div>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/dealers/add-points-modal.html"*/,
            template: "\n    <div class=\"mainDiv\">\n    <div class=\"secondDiv\">\n        <ion-row>\n            <ion-col style=\"padding: 0px; text-align: center;\">\n                <p style=\"margin: 0px; font-size:20px;\">Add Points</p>\n            </ion-col>\n        </ion-row>\n        <div class=\"temp\">  \n            <ion-label style=\"font-size: 1.2em;\">Points</ion-label>\n            <ion-item class=\"logitem\" style=\"border: 1px solid black;\">\n                <ion-input [(ngModel)]=\"pointShareCount\" type=\"number\" (ngModelChange) =\"calculateTotal()\">\n                </ion-input>\n            </ion-item>\n        </div>\n        <p style=\"margin: 0px;color: #666; padding-left: 10px;padding-bottom: 2px;\">\n            Please type No. of points to be shared\n        </p>\n           \n        <div class=\"temp\">\n            <ion-label style=\"font-size: 1.2em;\">Price/Unit</ion-label>\n            <ion-item class=\"logitem\" style=\"border: 1px solid black;\">\n                <ion-input [(ngModel)]=\"perunitprice\" type=\"number\" (ngModelChange) =\"calculateTotal()\"></ion-input>\n            </ion-item>\n        </div>\n        <p style=\"margin: 0px;color: #666; padding-left: 10px;padding-bottom: 10px;\">\n            Please type per unit price of points\n        </p>\n        <div class=\"temp\">\n            <ion-item>\n                <ion-label>Payment Status</ion-label>\n                <ion-select [(ngModel)]=\"payment_stauts\">\n                    <ion-option value=\"paid\">Paid</ion-option>\n                    <ion-option value=\"credit\">Credit</ion-option>\n                    <ion-option value=\"link\">Send Payment Link</ion-option>\n                </ion-select>\n            </ion-item>\n        </div>\n\n        <div class=\"temp\" *ngIf=\"payment_stauts === 'paid'\">\n            <ion-label style=\"font-size: 1.2em;\">Remarks</ion-label>\n            <ion-item class=\"logitem\" style=\"border: 1px solid black;\">\n                <ion-input [(ngModel)]=\"remarks\" type=\"text\"></ion-input>\n            </ion-item>\n        </div>\n\n        <div class=\"temp\" *ngIf=\"payment_stauts === 'credit'\">\n            <ion-item class=\"logitem\" style=\"border: 1px solid black;\">\n                <ion-datetime displayFormat=\"DD/MM/YYYY hh:mm a\" pickerFormat=\"DD/MM/YY hh:mm a\" [(ngModel)]=\"credit_date\"></ion-datetime>\n            </ion-item>\n        </div>\n        <p *ngIf=\"payment_stauts === 'credit'\" style=\"margin: 0px;color: #666; padding-left: 10px; padding-bottom: 10px;\">\n            Please Select Credit Date\n        </p>\n        <div class=\"temp\" *ngIf=\"payment_stauts === 'link'\">\n           <ion-row>\n                <ion-col col-8></ion-col>\n                <ion-col col-2 style=\"padding: 0px;text-align: right;\">\n                    <ion-icon color=\"gpsc\" style=\"font-size: 1.5em;\" name=\"mail\"></ion-icon>\n                </ion-col>\n                <ion-col col-2 style=\"padding: 0px;text-align: right;\">\n                    <ion-icon color=\"secondary\" style=\"font-size: 1.8em;\" name=\"text\"></ion-icon>\n                </ion-col>\n           </ion-row>\n        </div>\n        <div class=\"temp\">\n            <p><span style=\"font-weight: bold;\">Total Allocated Points</span> <span style=\"float: right;\">{{totAllocatedPoints}}</span></p>\n            <p><span style=\"font-weight: bold;\">Total Available Points</span> <span style=\"float: right;\">{{totAvailablePoints}}</span></p>\n            <p><span style=\"font-weight: bold;\">Total Amount</span> <span style=\"float: right;\">{{totalAmount}}</span></p>\n        </div>\n      \n        <div style=\"padding: 5px;\">\n            <ion-row>\n                <ion-col col-5>\n                    <button ion-button (click)=\"dismiss()\" color=\"light\" block>Cancle</button>\n                </ion-col>\n                <ion-col col-7>\n                    <button ion-button (click)=\"proceed()\" color=\"gpsc\" block>Proceed</button>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n</div>\n    ",
            styles: ["\n    .logitem {\n        background-color: transparent;\n        padding-bottom: 5px;\n\n        ion-input {\n            color: #000;\n            ::placeholder {\n                color: #000;\n            }\n        }\n        border: 1px solid black;\n        border-radius: 5px;\n    }\n  \n    .mainDiv {\n        padding: 15px;\n        height: 100%;\n        background-color: rgba(0, 0, 0, 0.7);\n    }\n\n    .secondDiv {\n        padding-top: 20px;\n        border-radius: 18px;\n        border: 2px solid black;\n        background: white;\n    }\n    .temp {\n        padding-left: 10px;\n        padding-right: 10px;\n        padding-top: 5px;\n        padding-bottom: 5px;\n    }\n    ion-input {\n        background-color: transparent;\n    }\n    .item-md.item-block .item-inner,\n    .item-ios.item-block .item-inner {\n        padding-right: 8px;\n        border: none;\n    }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], AddPointsModalPage);
    return AddPointsModalPage;
}());

//# sourceMappingURL=AddPointsModalPage.js.map

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FastagPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FasttagPayNow; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// declare var RazorpayCheckout: any;
var FastagPage = /** @class */ (function () {
    function FastagPage(navCtrl, navParams, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.buttonColor = '#fff'; //Default Color
        this.buttonColor1 = '#fff';
        this.mainCost = 200;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    FastagPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter FastagPage');
    };
    FastagPage.prototype.calc = function () {
        this.totalPayable = this.mainCost * this.numFastag;
    };
    FastagPage.prototype.addEvent = function () {
        this.vehicleType = 'Truck';
        this.buttonColor1 = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent1 = function () {
        this.vehicleType = 'Bus';
        this.buttonColor = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor1 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent2 = function () {
        this.vehicleType = 'Taxi';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor2 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent3 = function () {
        this.vehicleType = 'Car';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor3 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent4 = function () {
        this.vehicleType = 'Bike';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor4 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent5 = function () {
        this.vehicleType = 'Other';
        this.buttonColor4 = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor5 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.fastagReq = function () {
        var _this = this;
        if (this.vehicleType === undefined || this.numFastag === undefined) {
            this.toastCtrl.create({
                message: 'Please select the vehicle type and add number of requests.',
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        var url = this.apiCall.mainUrl + "fastTag/addRequest";
        var payload = {};
        // debugger
        if (this.islogin.Dealer_ID === undefined) {
            payload = {
                user: this.islogin._id,
                Dealer: this.islogin.supAdmin,
                supAdmin: this.islogin.supAdmin,
                date: new Date().toISOString(),
                quantity: this.numFastag,
                vehicle_type: this.vehicleType
            };
        }
        else {
            payload = {
                user: this.islogin._id,
                Dealer: this.islogin.Dealer_ID._id,
                supAdmin: this.islogin.supAdmin,
                date: new Date().toISOString(),
                quantity: this.numFastag,
                vehicle_type: this.vehicleType
            };
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, payload)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log('response data: ', respData);
            if (respData.message === 'saved succesfully') {
                _this.toastCtrl.create({
                    message: 'Fastag added successfully.',
                    duration: 2000,
                    position: 'bottom'
                }).present();
                _this.navCtrl.pop();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FastagPage.prototype.payNow = function () {
        this.navCtrl.push(FasttagPayNow, {
            param: {
                amount: this.totalPayable,
                fastagType: this.vehicleType,
                quantity: this.numFastag
            }
        });
    };
    FastagPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fastag',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/fastag-list/fastag/fastag.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Add Fastag</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row>\n    <ion-col>Choose Vehicle Type</ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-4 (click)="addEvent();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/truck.png" />\n          </ion-col>\n          <ion-col col-12 text-center>TRUCK</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-4 (click)="addEvent1();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor1}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/bus.png" />\n          </ion-col>\n          <ion-col col-12 text-center>BUS</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-4 (click)="addEvent2();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor2}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/cab.png" />\n          </ion-col>\n          <ion-col col-12 text-center>TAXI</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-4 (click)="addEvent3();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor3}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/car.png" />\n          </ion-col>\n          <ion-col col-12 text-center>CAR</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-4 (click)="addEvent4();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor4}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/motorcycle.png" />\n          </ion-col>\n          <ion-col col-12 text-center>BIKE</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-4 (click)="addEvent5();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor5}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/truck.png" />\n          </ion-col>\n          <ion-col col-12 text-center>OTHERS</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <ion-row *ngIf="vehicleType">\n    <ion-col col-6>\n      <ion-list>\n        <ion-item>\n          <ion-label floating>Fastag Quantity</ion-label>\n          <ion-input type="number" [(ngModel)]="numFastag"></ion-input>\n        </ion-item>\n      </ion-list>\n    </ion-col>\n    <ion-col col-6><button ion-button *ngIf="numFastag" round color="primary" style="margin-top: 12%;"\n        (click)="calc()">Calculate Cost</button></ion-col>\n  </ion-row>\n  <!-- <ion-list *ngIf="vehicleType">\n    <ion-item>\n      <ion-label floating>Fastag Quantity</ion-label>\n      <ion-input type="number" [(ngModel)]="numFastag"></ion-input>\n    </ion-item>\n  </ion-list> -->\n  <ion-row *ngIf="totalPayable">\n    <ion-col col-12 style="border: 2px solid black;">\n      <p style="font-size: 1.3em;\n      text-align: center;\n      margin: 2px;">Total Payable Amount - {{totalPayable}}</p>\n    </ion-col>\n  </ion-row>\n  <ion-row *ngIf="totalPayable">\n    <ion-col>\n\n      <button ion-button block color="gpsc" (click)="payNow()"><b>PAY NOW</b></button>\n      <button ion-button block color="primary" (click)="fastagReq()"><b>PAY LATTER</b></button>\n    </ion-col>\n  </ion-row>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/fastag-list/fastag/fastag.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], FastagPage);
    return FastagPage;
}());

var FasttagPayNow = /** @class */ (function () {
    function FasttagPayNow(navParams) {
        this.navParams = navParams;
        this.razor_key = 'rzp_live_jB4onRx1BUUvxt';
        this.currency = 'INR';
        this.data = {};
        this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
        this.data = this.navParams.get('param');
        this.paymentAmount = this.data.amount * 100;
        console.log(this.navParams.get('param'));
    }
    FasttagPayNow = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/fastag-list/fastag/paynow.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{ "Payment Method" | translate }}</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content no-padding>\n    <ion-grid text-center>\n        <ion-row>\n            <ion-col>\n                ** Pay with razorpay **\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <ion-card class="welcome-card">\n        <img src="/assets/imgs/dc-Cover-u0b349upqugfio195s4lpk8144-20190213120303.Medi.jpeg">\n        <ion-card-header>\n            <!-- <ion-card-subtitle>Get Started</ion-card-subtitle> -->\n            <ion-card-title>{{"Fastag for vehicle type" | translate}} - {{data.fastagType}}</ion-card-title>\n            <!-- <p>{{"Total Quantity" | translate}} - {{data.quantity}}</p> -->\n            <ion-row>\n                <ion-col>\n                    {{"Total Quantity" | translate}}\n                </ion-col>\n                <ion-col>\n                    {{data.quantity}}\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    {{"Total Payment Amount" | translate}}\n                </ion-col>\n                <ion-col>\n                    {{currencyIcon}}{{paymentAmount/100}}\n                </ion-col>\n            </ion-row>\n        </ion-card-header>\n        <ion-card-content>\n            <!-- <ion-button expand="full" color="success" (click)="payWithRazor()">Pay with RazorPay</ion-button> -->\n            <button ion-button full color="gpsc" (click)="payWithRazor()">Pay with RazorPay</button>\n        </ion-card-content>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/fastag-list/fastag/paynow.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FasttagPayNow);
    return FasttagPayNow;
}());

//# sourceMappingURL=fastag.js.map

/***/ }),

/***/ 418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagerProvider = /** @class */ (function () {
    function PagerProvider(http) {
        this.http = http;
        console.log('Hello PagerProvider Provider');
    }
    PagerProvider.prototype.getPager = function (totalItems, currentPage, pageSize) {
        if (currentPage === void 0) { currentPage = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        //calculate total pages
        var totalPages = Math.ceil(totalItems / pageSize);
        //ensure current page isn't out of range
        if (currentPage < 1) {
            currentPage = 1;
        }
        else if (currentPage > totalPages) {
            currentPage = totalPages;
        }
        var startPage, endPage;
        if (totalPages <= 10) {
            //less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        }
        else {
            //more that 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            }
            else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            }
            else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        // create an array of pages to ng-repeat in the pager control
        var pages = Array.from(Array((endPage + 1) - startPage).keys()).map(function (i) { return startPage + i; });
        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    };
    PagerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], PagerProvider);
    return PagerProvider;
}());

//# sourceMappingURL=pager.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelConsumptionPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FuelEntryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FuelConsumptionPage = /** @class */ (function () {
    function FuelConsumptionPage(apiCall, navCtrl, navParams, toastCtrl, event) {
        var _this = this;
        this.apiCall = apiCall;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.event = event;
        this.portstemp = [];
        this.fuelList = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_4_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_4_moment__().format();
        this.event.subscribe('reloadFuellist', function () {
            if (_this._vehId != undefined) {
                _this.getFuelList();
            }
        });
    }
    FuelConsumptionPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter FuelConsumptionPage');
    };
    FuelConsumptionPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelConsumptionPage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    FuelConsumptionPage.prototype.addFuelEntry = function () {
        this.navCtrl.push(FuelEntryPage, {
            portstemp: this.portstemp
        });
    };
    FuelConsumptionPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    FuelConsumptionPage.prototype.getFuelList = function () {
        var _this = this;
        this.fuelList = [];
        if (this._vehId == undefined) {
            var toast = this.toastCtrl.create({
                message: 'Please select the vehicle first..',
                position: 'bottom',
                duration: 1500
            });
            toast.present();
        }
        else {
            var _baseURL = this.apiCall.mainUrl + "fuel/getFuels?vehicle=" + this._vehId + "&user=" + this.islogin._id;
            this.apiCall.startLoading().present();
            this.apiCall.getSOSReportAPI(_baseURL)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("fule entry list: ", data);
                if (!data.message) {
                    _this.fuelList = data;
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: "Report not found!",
                    duration: 1500,
                    position: "bottom"
                });
                toast.present();
            });
        }
    };
    FuelConsumptionPage.prototype.getid = function (veh) {
        this._vehId = veh._id;
    };
    FuelConsumptionPage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    FuelConsumptionPage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    FuelConsumptionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-consumption',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/fuel/fuel-consumption/fuel-consumption.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Fuel Entry" | translate }}</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{\n      "Select Vehicle" | translate\n    }}</ion-label>\n    <select-searchable\n      item-content\n      [(ngModel)]="selectedVehicle"\n      [items]="portstemp"\n      itemValueField="Device_Name"\n      itemTextField="Device_Name"\n      [canSearch]="true"\n      (onChange)="getid(selectedVehicle)"\n    >\n    </select-searchable>\n  </ion-item>\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n    <ion-col width-20 class="cust">\n      <ion-label>\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n        <ion-datetime\n          displayFormat="DD-MM-YYYY hh:mm a"\n          pickerFormat="DD/MM/YY hh:mm a"\n          [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"\n        >\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20 class="cust">\n      <ion-label>\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n        <ion-datetime\n          displayFormat="DD-MM-YYYY hh:mm a"\n          pickerFormat="DD/MM/YY hh:mm a"\n          [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"\n        >\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon\n          ios="ios-search"\n          md="md-search"\n          style="font-size:2.3em;"\n          (click)="getFuelList()"\n        >\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <ion-card *ngFor="let f of fuelList">\n    <ion-card-content>\n      <ion-row>\n        <ion-col col-6 style="font-size: 1.2em;" no-padding>\n          <ion-row>\n            <ion-col col-2>\n              <ion-icon name="car" color="gpsc"></ion-icon>\n            </ion-col>\n            <ion-col col-10>\n              {{ f.vehicle.Device_Name }}\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col col-6 style="text-align: right;" no-padding>\n          <ion-row>\n            <ion-col col-12>\n              <ion-badge color="gpsc" style="font-size: 1.2em;"\n                >{{ f.quantity }} {{\'liters\' | translate}}</ion-badge\n              >\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 no-padding>\n          <ion-row>\n            <ion-col col-2>\n              <ion-icon style="color:gray;" name="calendar"></ion-icon>\n            </ion-col>\n            <ion-col\n              col-10\n              style="color:gray;font-size:14px;font-weight: 400;"\n              >{{ f.Date | date: "mediumDate" }}</ion-col\n            >\n          </ion-row>\n        </ion-col>\n        <ion-col col-6 no-padding>\n          <ion-row style="text-align: right;">\n            <ion-col col-12 style="color:gray;font-size:14px;font-weight: 400;">\n              <ion-icon style="color:gray;" name="cash"></ion-icon\n              >&nbsp;&nbsp;{{ f.price }} INR\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n<ion-fab right bottom>\n  <button ion-fab color="gpsc" (click)="addFuelEntry()">\n    <ion-icon name="add"></ion-icon>\n  </button>\n</ion-fab>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/fuel/fuel-consumption/fuel-consumption.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], FuelConsumptionPage);
    return FuelConsumptionPage;
}());

var FuelEntryPage = /** @class */ (function () {
    function FuelEntryPage(navParams, apiCall, toastCtrl, event) {
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.event = event;
        this.portstemp = [];
        this.fuelType = 'CNG';
        this.liter = 0;
        this.amt = 0;
        this.ddate = new Date().toISOString();
        this.portstemp = this.navParams.get("portstemp");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
    }
    FuelEntryPage.prototype.onChnageEvent = function (veh) {
        console.log("vehicle info:", veh);
        debugger;
        var that = this;
        that.veh_id = veh._id;
        var sb = veh.total_odo;
        that.tot_odo = sb.toFixed(2);
    };
    FuelEntryPage.prototype.fixDecimals = function (value) {
        value = "" + value;
        value = value.trim();
        value = parseFloat(value).toFixed(2);
        return value;
    };
    FuelEntryPage.prototype.submit = function () {
        var _this = this;
        var that = this;
        debugger;
        if (this.liter == undefined || this.amt == undefined || this.tot_odo == undefined || this.fuelType == undefined || this.ddate == undefined || this.vehName == undefined) {
            var toast = this.toastCtrl.create({
                message: "Fields should not be empty.",
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }
        else {
            var payload = {};
            var _baseURL = this.apiCall.mainUrl + "fuel/addFuel";
            payload = {
                "vehicle": that.veh_id,
                "user": that.islogin._id,
                "date": new Date(that.ddate).toISOString(),
                "quantity": that.liter,
                "odometer": that.tot_odo,
                "price": that.amt,
                "fuel_type": that.fuelType,
                "comment": "First fill"
            };
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(_baseURL, payload)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("fuel entry added: " + data);
                _this.liter = null;
                _this.amt = null;
                _this.tot_odo = null;
                _this.fuelType = null;
                _this.ddate = null;
                _this.vehName = null;
                _this.veh_id = null;
                var toast = _this.toastCtrl.create({
                    message: "Fuel entry added successfully! Add another entry..",
                    position: 'bottom',
                    duration: 2000
                });
                toast.present();
                _this.event.publish('reloadFuellist');
            });
        }
    };
    FuelEntryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/fuel/fuel-consumption/fuel-entry.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Add Fuel Entry</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item>\n        <ion-label>Select Vehicle</ion-label>\n        <ion-select [(ngModel)]="vehName">\n            <ion-option *ngFor="let veh of portstemp" [value]="veh.Device_Name" (ionSelect)="onChnageEvent(veh)">\n                {{veh.Device_Name}}</ion-option>\n        </ion-select>\n    </ion-item>\n    <ion-item *ngIf="tot_odo != undefined">\n        <ion-label>Total Odo</ion-label>\n        <ion-input type="number" [(ngModel)]="tot_odo" readonly></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label>Fuel Type</ion-label>\n        <ion-select [(ngModel)]="fuelType">\n            <ion-option [value]="CNG" selected>CNG</ion-option>\n            <ion-option [value]="Petrol">PETROL</ion-option>\n            <ion-option [value]="Diesel">DIESEL</ion-option>\n            <ion-option [value]="Other">OTHER</ion-option>\n        </ion-select>\n    </ion-item>\n\n    <ion-item>\n        <ion-label>Liter(s)</ion-label>\n        <ion-input type="number" [(ngModel)]="liter"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label>Amount</ion-label>\n        <ion-input type="number" [(ngModel)]="amt"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label>Date</ion-label>\n        <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" [(ngModel)]="ddate" name="ddate">\n        </ion-datetime>\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-footer class="footSty">\n    <ion-toolbar>\n        <ion-row no-padding>\n            <ion-col width-50 style="text-align: center;">\n                <button ion-button clear color="light" (click)="submit()">SUBMIT</button>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/fuel/fuel-consumption/fuel-entry.html"*/,
            selector: 'page-fuel-consumption'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], FuelEntryPage);
    return FuelEntryPage;
}());

//# sourceMappingURL=fuel-consumption.js.map

/***/ }),

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalPage = /** @class */ (function () {
    function ModalPage(
    // private navParams: NavParams,
    viewCtrl) {
        this.viewCtrl = viewCtrl;
        this.min_time = "10";
    }
    ModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.min_time);
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            template: "\n        <div class=\"mainDiv\">\n            <div class=\"secondDiv\">\n                <ion-list radio-group [(ngModel)]=\"min_time\">\n                    <ion-item>\n                        <ion-label>10 Mins</ion-label>\n                        <ion-radio value=\"10\" checked></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>20 Mins</ion-label>\n                        <ion-radio value=\"20\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>30 Mins</ion-label>\n                        <ion-radio value=\"30\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>40 Mins</ion-label>\n                        <ion-radio value=\"40\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>50 Mins</ion-label>\n                        <ion-radio value=\"50\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>60 Mins</ion-label>\n                        <ion-radio value=\"60\"></ion-radio>\n                    </ion-item>\n                    \n                </ion-list>\n                <div style=\"padding: 5px;\"><button ion-button (click)=\"dismiss()\" color=\"gpsc\" block>Submit</button></div>\n            </div>\n        </div>\n    ",
            styles: ["\n      \n            .mainDiv {\n                padding: 50px;\n                height: 100%;\n                background-color: rgba(0, 0, 0, 0.7);\n            }\n\n            .secondDiv {\n                padding-top: 20px;\n                border-radius: 18px;\n                border: 2px solid black;\n                background: white;\n            }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=modal.js.map

/***/ }),

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MyBookingsPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddBookingModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyBookingsPage = /** @class */ (function () {
    function MyBookingsPage(navCtrl, navParams, modalCtrl, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.bookingsData = [];
        this.isLogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    MyBookingsPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidLoad MyBookingsPage');
        this.getBooking();
    };
    MyBookingsPage.prototype.createBooking = function () {
        var _this = this;
        var modal = this.modalCtrl.create(AddBookingModal);
        modal.onDidDismiss(function () {
            _this.getBooking();
        });
        modal.present();
    };
    MyBookingsPage.prototype.getBooking = function () {
        var _this = this;
        var _baseUrl = "https://server2.oneqlik.in/vehicleBooking/getBookingDetail?user_id=" + this.isLogin._id;
        // this.apiCall.startLoading()
        this.apiCall.getdevicesForAllVehiclesApi(_baseUrl).subscribe(function (data) {
            console.log("check bookings: ", data);
            if (data) {
                _this.bookingsData = data;
            }
        }, function (err) {
            console.log(err);
            _this.toastCtrl.create({
                message: JSON.parse(err._body).message,
                position: 'middle',
                duration: 2000
            }).present();
        });
    };
    MyBookingsPage.prototype.showBooking = function (item) {
        this.navCtrl.push('BookingDetailPage', {
            params: item
        });
    };
    MyBookingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-my-bookings',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/my-bookings/my-bookings.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>My Bookings</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="createBooking()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item *ngFor="let item of bookingsData" (click)="showBooking(item)">\n      <ion-avatar item-start>\n        <img src="assets/imgs/dummy-user-img.png">\n      </ion-avatar>\n      <h2>{{item.customer_id.customer_full_name}}</h2>\n      <p>\n        <ion-icon name="car"></ion-icon>&nbsp;&nbsp;Vehicle No &mdash; {{item.device.Device_Name}}\n      </p>\n      <p>\n        <ion-icon name="calendar"></ion-icon>&nbsp;&nbsp;Date of booking &mdash; {{item.Booking_date | date:\'medium\'}}\n      </p>\n      <ion-badge item-end color="gpsc">Status</ion-badge>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/my-bookings/my-bookings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], MyBookingsPage);
    return MyBookingsPage;
}());

var AddBookingModal = /** @class */ (function () {
    function AddBookingModal(viewCtrl, formBuilder, apiCall, actionSheetCtrl, camera, transfer, toastCtrl) {
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.toastCtrl = toastCtrl;
        this.showCustForm = false;
        this.showVehicleForm = false;
        this.portstemp = [];
        this.allData = [];
        this.upload_doc = false;
        this.showBtn = false;
        this.showUpdateBtn = false;
        this.isLogin = JSON.parse(localStorage.getItem('details')) || {};
        this.getdevices();
        this.addBookingForm = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            contract_number: [''],
            tele: [''],
            email: [''],
            dob: [''],
            driver_licence: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            nationality: [''],
            permanent_address: [''],
            dl_VU: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            idcard_passport: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            id_VU: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
        this.addVehicleForm = formBuilder.group({
            // vehicle_name: ['', Validators.required],
            brand: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_model: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            tuition: [''],
            number: [''],
            home_delivery: [''],
            damage_limit: [''],
            expected_return_date: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            pickup_date: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    AddBookingModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AddBookingModal.prototype.addBooking = function (key) {
        var _this = this;
        this.submitAttempt = true;
        var _baseUrl, payload;
        if (key === 'cust') {
            if (this.addBookingForm.valid) {
                // _baseUrl = this.apiCall.mainUrl + "order/addOrderContact";
                _baseUrl = "https://server2.oneqlik.in/order/addOrderContact";
                payload = {
                    "customer_full_name": this.addBookingForm.value.name,
                    "customer_phone": (this.addBookingForm.value.tele ? this.addBookingForm.value.tele : null),
                    "customer_address": (this.addBookingForm.value.permanent_address ? this.addBookingForm.value.permanent_address : null),
                    "icard_detail": {
                        "icard_number": this.addBookingForm.value.idcard_passport,
                        "valid_upto": new Date(this.addBookingForm.value.id_VU).toISOString()
                    },
                    "date_of_birth": (this.addBookingForm.value.dob ? new Date(this.addBookingForm.value.dob).toISOString() : null),
                    "driving_license": {
                        "dl_number": this.addBookingForm.value.driver_licence,
                        "valid_upto": new Date(this.addBookingForm.value.dl_VU).toISOString()
                    },
                    "admin": this.isLogin._id,
                    "contract_no": (this.addBookingForm.value.contract_number ? this.addBookingForm.value.contract_number : null),
                    "email_id": (this.addBookingForm.value.email ? this.addBookingForm.value.email : null),
                    "nationality": (this.addBookingForm.value.nationality ? this.addBookingForm.value.nationality : 'Indian')
                };
                this.apiCall.startLoading().present();
                this.apiCall.urlpasseswithdata(_baseUrl, payload).subscribe(function (respData) {
                    _this.apiCall.stopLoading();
                    console.log(respData);
                    if (!respData.message) {
                        _this.customer_id = respData._id;
                        _this.toastCtrl.create({
                            message: 'Customer info saved successfully',
                            duration: 2000,
                            position: 'middle'
                        }).present();
                        _this.addBookingForm.reset();
                        _this.submitAttempt = false;
                        _this.showCustForm = false;
                        _this.showVehicleForm = true;
                    }
                    if (respData.message) {
                        _this.toastCtrl.create({
                            message: respData.message,
                            duration: 2000,
                            position: 'bottom'
                        }).present();
                    }
                }, function (err) {
                    _this.apiCall.stopLoading();
                    console.log(err);
                });
            }
        }
        else if (key === 'vehicle') {
            if (this.addVehicleForm.valid) {
                if (this.customer_id === undefined) {
                    this.toastCtrl.create({
                        message: 'Please submit customer details first...',
                        duration: 2000,
                        position: 'middle'
                    }).present();
                    return;
                }
                // _baseUrl = this.apiCall.mainUrl + "vehicleBooking/addBooking";
                _baseUrl = "https://server2.oneqlik.in/vehicleBooking/addBooking";
                payload = {
                    "user_id": this.isLogin._id,
                    "device": this.vehData._id,
                    "customer_id": this.customer_id,
                    "pickup_date": new Date(this.addVehicleForm.value.pickup_date).toISOString(),
                    "expected_return_date": new Date(this.addVehicleForm.value.expected_return_date).toISOString(),
                    "Vehicle_brand": this.addVehicleForm.value.brand,
                    "Vehicle_model": this.device_model_id,
                    "home_delivery": (this.addVehicleForm.value.home_delivery ? this.addVehicleForm.value.home_delivery : null),
                    "damage_limit": (this.addVehicleForm.value.damage_limit ? this.addVehicleForm.value.damage_limit : null),
                    "images": this.allData
                };
                this.apiCall.startLoading().present();
                this.apiCall.urlpasseswithdata(_baseUrl, payload).subscribe(function (respData) {
                    _this.apiCall.stopLoading();
                    console.log("vehicle booking data: ", respData);
                    _this.toastCtrl.create({
                        message: 'Booking confirmed!!',
                        duration: 2000,
                        position: 'middle'
                    }).present();
                    _this.addVehicleForm.reset();
                    _this.submitAttempt = false;
                    _this.vehicle_name = undefined;
                    _this.vehData = undefined;
                    _this.device_model_id = undefined;
                    _this.allData = [];
                }, function (err) {
                    _this.apiCall.stopLoading();
                    console.log("got error: ", err);
                });
            }
        }
        else if (key === 'custupdate') {
            if (this.addBookingForm.valid) {
                _baseUrl = "https://server2.oneqlik.in/order/updateOrderContact";
                payload = {
                    "customer_full_name": this.addBookingForm.value.name,
                    "customer_phone": (this.addBookingForm.value.tele ? this.addBookingForm.value.tele : null),
                    "customer_address": (this.addBookingForm.value.permanent_address ? this.addBookingForm.value.permanent_address : null),
                    "icard_detail": {
                        "icard_number": this.addBookingForm.value.idcard_passport,
                        "valid_upto": new Date(this.addBookingForm.value.id_VU).toISOString()
                    },
                    "date_of_birth": (this.addBookingForm.value.dob ? new Date(this.addBookingForm.value.dob).toISOString() : null),
                    "driving_license": {
                        "dl_number": this.addBookingForm.value.driver_licence,
                        "valid_upto": new Date(this.addBookingForm.value.dl_VU).toISOString()
                    },
                    "admin": this.isLogin._id,
                    "contract_no": (this.addBookingForm.value.contract_number ? this.addBookingForm.value.contract_number : null),
                    "email_id": (this.addBookingForm.value.email ? this.addBookingForm.value.email : null),
                    "nationality": (this.addBookingForm.value.nationality ? this.addBookingForm.value.nationality : 'Indian'),
                    "_id": this.customer_id
                };
                this.apiCall.startLoading().present();
                this.apiCall.urlpasseswithdata(_baseUrl, payload).subscribe(function (respData) {
                    _this.apiCall.stopLoading();
                    console.log(respData);
                    if (!respData.message) {
                        _this.customer_id = respData._id;
                        _this.toastCtrl.create({
                            message: 'Customer info saved successfully',
                            duration: 2000,
                            position: 'middle'
                        }).present();
                        _this.addBookingForm.reset();
                        _this.submitAttempt = false;
                        _this.showCustForm = false;
                        _this.showVehicleForm = true;
                    }
                }, function (err) {
                    _this.apiCall.stopLoading();
                    console.log(err);
                });
            }
        }
    };
    AddBookingModal.prototype.checkBtn = function (key) {
        if (key === 'cust') {
            this.showCustForm = !this.showCustForm;
            this.showVehicleForm = false;
            this.submitAttempt = false;
        }
        else if (key === 'vehicle') {
            this.showVehicleForm = !this.showVehicleForm;
            this.showCustForm = false;
            this.submitAttempt = false;
        }
    };
    AddBookingModal.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.isLogin._id + '&email=' + this.isLogin.email;
        if (this.isLogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.isLogin._id;
        }
        else {
            if (this.isLogin.isDealer == true) {
                baseURLp += '&dealer=' + this.isLogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
    };
    AddBookingModal.prototype.onChangedSelect = function (pdata) {
        this.allData = [];
        this.upload_doc = false;
        console.log(pdata);
        this.addVehicleForm.patchValue({
            device_model: pdata.vehicleType.model,
            brand: pdata.vehicleType.brand
        });
        this.device_model_id = pdata.device_model._id;
        this.vehData = pdata;
        if (this.vehData.imageDoc.length > 0) {
            for (var i = 0; i < this.vehData.imageDoc.length; i++) {
                var gg = this.vehData.imageDoc[i];
                var imgUrl = gg + '';
                ;
                var str1 = imgUrl.split('public/');
                this.allData.push({
                    fileUrl: "http://13.126.36.205/" + str1[1]
                });
            }
            console.log("length: ", this.allData.length);
            console.log("allData: " + JSON.stringify(this.allData));
            this.upload_doc = true;
        }
    };
    AddBookingModal.prototype.selectImage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.pickImage(_this.camera.PictureSourceType.SAVEDPHOTOALBUM);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.pickImage(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    AddBookingModal.prototype.pickImage = function (sourceType) {
        var _this = this;
        var url = "http://13.126.36.205/users/uploadImage";
        // var url = "https://www.oneqlik.in/users/uploadProfilePicture";
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            // let base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log("imageData: ", imageData);
            debugger;
            // this.crop.crop(imageData, { quality: 100 })
            //   .then(
            //     newImage => {
            var dlink123 = imageData.split('?');
            var wear = dlink123[0];
            var fileTransfer = _this.transfer.create();
            var uploadOpts = {
                fileKey: 'photo',
                // fileName: imageData.substr(imageData.lastIndexOf('/') + 1)
                fileName: wear.substr(wear.lastIndexOf('/') + 1)
            };
            _this.apiCall.startLoading().present();
            debugger;
            fileTransfer.upload(wear, url, uploadOpts)
                .then(function (data) {
                _this.apiCall.stopLoading();
                console.log(data);
                // this.selectedFile = <File>event.target.files[0];
                var respData = data.response;
                console.log("image data response: ", respData);
                _this.dlUpdate(respData);
                // this.fileUrl = this.respData.fileUrl;
                // this.fileUrl = this.respData;
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log(err);
                _this.toastCtrl.create({
                    message: 'Something went wrong while uploading file... Please try after some time..',
                    duration: 2000,
                    position: 'bottom'
                }).present();
            });
            // });
        }, function (err) {
            console.log("imageData err: ", err);
            // Handle error
        });
    };
    AddBookingModal.prototype.dlUpdate = function (dllink) {
        var _this = this;
        // debugger
        // let that = this;
        var dlink123 = dllink.split('?');
        var wear = dlink123[0];
        console.log("new download link: ", wear);
        // var _burl = this.apiCall.mainUrl + "users/updateImagePath";
        var _burl = "http://13.126.36.205/devices/deviceupdate";
        var payload = {
            imageDoc: [dllink],
            _id: this.vehData._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_burl, payload)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log("check profile upload: ", respData);
            _this.getImgUrl();
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    AddBookingModal.prototype.getImgUrl = function () {
        var _this = this;
        this.allData = [];
        // var url = this.apiCall.mainUrl + "users/shareProfileImage?uid=" + this.islogin._id;
        // var url = "http://13.126.36.205/users/shareProfileImage?uid=" + this.isLogin._id;
        var url = 'http://13.126.36.205/devices/getDevicebyId?deviceId=' + this.vehData.Device_ID;
        // this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(function (resp) {
            // this.apiCall.stopLoading();
            console.log("server image url=> ", resp);
            if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
                for (var i = 0; i < JSON.parse(JSON.stringify(resp)).imageDoc.length; i++) {
                    var gg = JSON.parse(JSON.stringify(resp)).imageDoc[i];
                    var imgUrl = gg + '';
                    ;
                    var str1 = imgUrl.split('public/');
                    _this.allData.push({
                        fileUrl: "http://13.126.36.205/" + str1[1]
                    });
                }
                console.log("length: ", _this.allData.length);
                console.log("allData: " + JSON.stringify(_this.allData));
            }
        }, function (err) {
            // this.apiCall.stopLoading();
        });
    };
    AddBookingModal.prototype.uploadDocCheck = function () {
        if (this.vehData === undefined) {
            this.toastCtrl.create({
                message: 'Please select the vehicle first...',
                duration: 2000,
                position: 'middle'
            }).present();
            return;
        }
        this.upload_doc = true;
    };
    AddBookingModal.prototype.verifyId = function () {
        var _this = this;
        if (this.addBookingForm.value.idcard_passport) {
            var url = this.apiCall.mainUrl + "order/getCustomerwithicard?icard_number=" + this.addBookingForm.value.idcard_passport;
            this.apiCall.getSOSReportAPI(url)
                .subscribe(function (res) {
                console.log(res);
                debugger;
                if (res.message === 'costumer alredy exists') {
                    _this.showUpdateBtn = true;
                    var submittedData = res['costumer object'];
                    console.log("data: ", submittedData);
                    console.log("slkdsnlsdsaldmas;dma;s ", __WEBPACK_IMPORTED_MODULE_6_moment__(new Date(submittedData.date_of_birth), 'DD/MM/YYYY').format('YYYY-MM-DD'));
                    _this.customer_id = submittedData._id;
                    _this.addBookingForm.patchValue({
                        name: submittedData.customer_full_name,
                        contract_number: (submittedData.contract_no ? submittedData.contract_no : null),
                        tele: (submittedData.customer_phone ? submittedData.customer_phone : null),
                        email: (submittedData.email_id ? submittedData.email_id : null),
                        dob: (submittedData.date_of_birth ? __WEBPACK_IMPORTED_MODULE_6_moment__(new Date(submittedData.date_of_birth), 'DD/MM/YYYY').format('YYYY-MM-DD') : null),
                        driver_licence: submittedData.driving_license.dl_number,
                        nationality: submittedData.nationality,
                        permanent_address: (submittedData.customer_address ? submittedData.customer_address : null),
                        dl_VU: __WEBPACK_IMPORTED_MODULE_6_moment__(new Date(submittedData.driving_license.valid_upto), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                        idcard_passport: submittedData.icard_detail.icard_number,
                        id_VU: __WEBPACK_IMPORTED_MODULE_6_moment__(new Date(submittedData.icard_detail.valid_upto), 'DD/MM/YYYY').format('YYYY-MM-DD')
                    });
                }
                else if (res.message === 'No customer found') {
                    _this.showUpdateBtn = false;
                    _this.addBookingForm.patchValue({
                        name: null,
                        contract_number: null,
                        tele: null,
                        email: null,
                        dob: null,
                        driver_licence: null,
                        nationality: null,
                        permanent_address: null,
                        dl_VU: null,
                        idcard_passport: _this.addBookingForm.value.idcard_passport,
                        id_VU: null
                    });
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    AddBookingModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-my-bookings',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/my-bookings/add-booking.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{\'Add Booking\' | translate}}</ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="dismiss()">\n                <ion-icon name="close-circle"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n\n</ion-header>\n<ion-content padding>\n    <ion-row>\n        <ion-col col-12>\n            <button ion-button block outline color="gpsc" *ngIf="!showCustForm" (click)="checkBtn(\'cust\')">Add Customer\n                Detail</button>\n            <button ion-button block color="gpsc" *ngIf="showCustForm" (click)="checkBtn(\'cust\')">Add Customer\n                Detail</button>\n        </ion-col>\n        <ion-col col-12>\n            <button ion-button block outline color="gpsc" *ngIf="!showVehicleForm" (click)="checkBtn(\'vehicle\')">Add\n                Vehicle Detail</button>\n            <button ion-button block color="gpsc" *ngIf="showVehicleForm" (click)="checkBtn(\'vehicle\')">Add Vehicle\n                Detail</button>\n        </ion-col>\n    </ion-row>\n    <form *ngIf="showCustForm" [formGroup]="addBookingForm" style="padding-right: 16px;">\n        <ion-item>\n            <ion-label stacked>{{\'Full Name\' | translate}}</ion-label>\n            <ion-input formControlName="name" type="text"></ion-input>\n        </ion-item>\n        <ion-item class="logitem1" no-lines\n            *ngIf="!addBookingForm.controls.name.valid && (addBookingForm.controls.name.dirty || submitAttempt)">\n            <p style="font-size: 0.7em; color: red;">{{\'Full name is required!\' | translate}}</p>\n        </ion-item>\n        <ion-row>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Identity Card / Passport\' | translate}}</ion-label>\n                    <ion-input type="text" formControlName="idcard_passport" (input)="showBtn = true">\n                    </ion-input>\n                </ion-item>\n                <ion-item class="logitem1" no-lines\n                    *ngIf="!addBookingForm.controls.idcard_passport.valid && (addBookingForm.controls.idcard_passport.dirty || submitAttempt)">\n                    <p style="font-size: 0.7em; color: red;">{{\'Identity card number is required!\' | translate}}</p>\n                </ion-item>\n            </ion-col>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Valid Until\' | translate}}</ion-label>\n                    <ion-input type="date" formControlName="id_VU">\n                    </ion-input>\n                </ion-item>\n                <ion-item class="logitem1" no-lines\n                    *ngIf="!addBookingForm.controls.id_VU.valid && (addBookingForm.controls.id_VU.dirty || submitAttempt)">\n                    <p style="font-size: 0.7em; color: red;">{{\'Valid until date is required!\' | translate}}</p>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-row *ngIf="showBtn">\n            <ion-col col-12 style="padding: 5px 0px 5px 16px;">\n                <button ion-button block round outline color="gpsc" (click)="verifyId()">Verify Id</button>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Driver Licence\' | translate}}</ion-label>\n                    <ion-input type="text" formControlName="driver_licence">\n                    </ion-input>\n                </ion-item>\n                <ion-item class="logitem1" no-lines\n                    *ngIf="!addBookingForm.controls.driver_licence.valid && (addBookingForm.controls.driver_licence.dirty || submitAttempt)">\n                    <p style="font-size: 0.7em; color: red;">{{\'Driver Licence number is required!\' | translate}}</p>\n                </ion-item>\n            </ion-col>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Valid Until\' | translate}}</ion-label>\n                    <ion-input type="date" formControlName="dl_VU">\n                    </ion-input>\n                </ion-item>\n                <ion-item class="logitem1" no-lines\n                    *ngIf="!addBookingForm.controls.dl_VU.valid && (addBookingForm.controls.dl_VU.dirty || submitAttempt)">\n                    <p style="font-size: 0.7em; color: red;">{{\'Valid until date is required!\' | translate}}</p>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Nationality\' | translate}}</ion-label>\n                    <ion-input formControlName="nationality" type="text"></ion-input>\n                </ion-item>\n            </ion-col>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Date Of Birth\' | translate}}</ion-label>\n                    <ion-input formControlName="dob" type="date"></ion-input>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-item>\n            <ion-label stacked>{{\'Contract Number\' | translate}}</ion-label>\n            <ion-input formControlName="contract_number" type="number"></ion-input>\n        </ion-item>\n        <ion-item class="logitem1" no-lines\n            *ngIf="!addBookingForm.controls.contract_number.valid && (addBookingForm.controls.contract_number.dirty || submitAttempt)">\n            <p style="font-size: 0.7em; color: red;">{{\'Contract number is required!\' | translate}}</p>\n        </ion-item>\n\n        <ion-item>\n            <ion-label stacked>{{\'Telephone\'| translate}}</ion-label>\n            <ion-input formControlName="tele" type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label stacked>{{\'Email\'| translate}}</ion-label>\n            <ion-input formControlName="email" type="email"></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label stacked>{{\'Permanent Address\' | translate}}</ion-label>\n            <ion-input formControlName="permanent_address" type="text"></ion-input>\n        </ion-item>\n    </form>\n    <ion-item *ngIf="showVehicleForm">\n        <ion-label stacked>{{\'Select Vehicle\' | translate}}</ion-label>\n        <select-searchable style="padding: 0px;" item-content [(ngModel)]="vehicle_name" [items]="portstemp"\n            itemValueField="Device_Name" itemTextField="Device_Name" [canSearch]="true"\n            (onChange)="onChangedSelect(vehicle_name)">\n        </select-searchable>\n    </ion-item>\n    <form *ngIf="showVehicleForm" [formGroup]="addVehicleForm" style="padding-right: 16px;">\n\n        <ion-item>\n            <ion-label stacked>{{\'Vehicle Brand\' | translate}}</ion-label>\n            <ion-input formControlName="brand" type="text" readonly></ion-input>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addVehicleForm.controls.brand.valid && (addVehicleForm.controls.brand.dirty || submitAttempt)"\n            no-lines>\n            <p style="font-size: 0.7em; color: red;">{{\'vehicle brand is required!\' | translate}}</p>\n        </ion-item>\n        <ion-item>\n            <ion-label stacked>{{\'Vehicle Model\'| translate}}</ion-label>\n            <ion-input formControlName="device_model" type="text" readonly></ion-input>\n        </ion-item>\n        <ion-item class="logitem1"\n            *ngIf="!addVehicleForm.controls.device_model.valid && (addVehicleForm.controls.device_model.dirty || submitAttempt)"\n            no-lines>\n            <p style="font-size: 0.7em; color: red;">{{\'device model required!\'| translate}}</p>\n        </ion-item>\n        <!-- <ion-row>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Tuition\' | translate}}</ion-label>\n                    <ion-input type="text" formControlName="tuition">\n                    </ion-input>\n                </ion-item>\n            </ion-col>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Number\' | translate}}</ion-label>\n                    <ion-input type="number" formControlName="number">\n                    </ion-input>\n                </ion-item>\n            </ion-col>\n        </ion-row> -->\n        <ion-row>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Home Delivery\' | translate}}</ion-label>\n                    <ion-input type="text" formControlName="home_delivery">\n                    </ion-input>\n                </ion-item>\n            </ion-col>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Damage Limit\' | translate}}</ion-label>\n                    <ion-input type="number" formControlName="damage_limit">\n                    </ion-input>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Pickup Date\' | translate}}</ion-label>\n                    <ion-input type="date" formControlName="pickup_date">\n                    </ion-input>\n                </ion-item>\n                <ion-item class="logitem1" no-lines\n                    *ngIf="!addVehicleForm.controls.pickup_date.valid && (addVehicleForm.controls.pickup_date.dirty || submitAttempt)">\n                    <p style="font-size: 0.7em; color: red;">{{\'pickup date required!\'| translate}}</p>\n                </ion-item>\n            </ion-col>\n            <ion-col col-6 no-padding>\n                <ion-item>\n                    <ion-label stacked>{{\'Expected Return Date\' | translate}}</ion-label>\n                    <ion-input type="date" formControlName="expected_return_date">\n                    </ion-input>\n                </ion-item>\n                <ion-item class="logitem1" no-lines\n                    *ngIf="!addVehicleForm.controls.expected_return_date.valid && (addVehicleForm.controls.expected_return_date.dirty || submitAttempt)">\n                    <p style="font-size: 0.7em; color: red;">{{\'expected return date required!\'| translate}}</p>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-row *ngIf="!upload_doc && allData.length === 0">\n            <ion-col col-12 style="padding: 5px 0px 5px 16px;">\n                <button ion-button color="gpsc" round block outline (click)="uploadDocCheck()">Upload Vehicle Images</button>\n            </ion-col>\n        </ion-row>\n        <ion-row *ngIf="upload_doc || allData.length > 0" style="padding-top: 5px;">\n            <ion-col col-12 style="padding: 5px 0px 5px 16px;">\n                <div class="img_container" (click)="selectImage()">\n                    <ion-icon *ngIf="allData.length === 0" name="add-circle" color="gpsc" style="font-size: 1.5em;">\n                    </ion-icon>\n                    <img *ngIf="allData.length !== 0" src="{{allData[0].fileUrl}}" style="height: 150px; width: 100%;" />\n                </div>\n            </ion-col>\n            <ion-col col-12 style="padding: 5px 0px 5px 16px;">\n                <div class="img_container" (click)="selectImage()">\n                    <img *ngIf="allData.length >= 2" src="{{allData[1].fileUrl}}" style="height: 150px; width: 100%;" />\n                    <ion-icon *ngIf="allData.length === 1 || allData.length === 0" name="add-circle" color="gpsc"\n                        style="font-size: 1.5em;"></ion-icon>\n                </div>\n            </ion-col>\n        </ion-row>\n        <!-- <ion-row *ngIf="upload_doc || allData.length > 0">\n            <ion-col col-6 style="padding: 5px 0px 5px 16px;">\n                <div class="img_container" (click)="selectImage()">\n                    <img *ngIf="allData.length >= 3" src="{{allData[2].fileUrl}}" style="height: 120px; width: 100%;" />\n                    <ion-icon *ngIf="allData.length === 2 || allData.length === 0 || allData.length === 1"\n                        name="add-circle" color="gpsc" style="font-size: 1.5em;"></ion-icon>\n                </div>\n            </ion-col>\n            <ion-col col-6 style="padding: 5px 0px 5px 16px;">\n                <div class="img_container" (click)="selectImage()">\n                    <img *ngIf="allData.length >= 4" src="{{allData[3].fileUrl}}" style="height: 120px; width: 100%;" />\n                    <ion-icon\n                        *ngIf="allData.length === 3 || allData.length === 0 || allData.length === 1 || allData.length === 2"\n                        name="add-circle" color="gpsc" style="font-size: 1.5em;"></ion-icon>\n                </div>\n            </ion-col>\n        </ion-row> -->\n    </form>\n</ion-content>\n\n<ion-footer class="footSty">\n    <ion-toolbar>\n        <ion-row no-padding>\n            <ion-col col-12 *ngIf="showVehicleForm" style="text-align: center; padding: 0px !important">\n                <button ion-button style="padding: 0px;" clear color="light"\n                    (click)="addBooking(\'vehicle\')">{{\'SUBMIT VEHICLE DETAIL\' | translate}}</button>\n            </ion-col>\n            <ion-col col-12 *ngIf="showCustForm" style="text-align: center; padding: 0px !important">\n                <button *ngIf="!showUpdateBtn" ion-button style="padding: 0px;" clear color="light"\n                    (click)="addBooking(\'cust\')">{{\'SUBMIT CUSTOMER DETAIL\' | translate}}</button>\n                    <button *ngIf="showUpdateBtn" ion-button style="padding: 0px;" clear color="light"\n                    (click)="addBooking(\'custupdate\')">{{\'UPDATE CUSTOMER DETAIL\' | translate}}</button>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/my-bookings/add-booking.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], AddBookingModal);
    return AddBookingModal;
}());

//# sourceMappingURL=my-bookings.js.map

/***/ }),

/***/ 425:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotifModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotifModalPage = /** @class */ (function () {
    function NotifModalPage(navParams, viewCtrl, apiCall, toastCtrl) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.isAddEmail = false;
        this.isAddPhone = false;
        this.emailList = [];
        this.phonelist = [];
        this.femail = '';
        this.fphone = 0;
        this.model = {};
        this.fData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.notifType = navParams.get('notifData').notifType;
        this.data = navParams.get('notifData').compData;
        if (navParams.get('notifData').buttonClick == 'email') {
            this.isAddEmail = true;
        }
        if (navParams.get('notifData').buttonClick == 'phone') {
            this.isAddPhone = true;
        }
    }
    NotifModalPage.prototype.ngOnInit = function () {
        this.emailList = this.data[this.notifType].emails;
        this.phonelist = this.data[this.notifType].phones;
    };
    NotifModalPage.prototype.onSubmit = function () {
        console.log('Form submitted!!');
    };
    NotifModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    NotifModalPage.prototype.addEmail = function (id, index) {
        var _this = this;
        if (id == 'add') {
            if (!this.data[this.notifType].emails) {
                this.data[this.notifType].emails = [];
            }
            this.data[this.notifType].emails.push(this.model.email);
            console.log(this.data);
            this.fData.contactid = this.islogin._id;
            this.fData.alert = this.data;
            console.log("fData object =>", this.fData);
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.email = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Email Id added!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
        if (id == 'delete') {
            var splicedmail = this.data[this.notifType].emails[index];
            console.log("spliceNumber =>", splicedmail);
            this.data[this.notifType].emails.splice(index, 1);
            this.fData.contactid = this.islogin._id;
            this.fData.alert = this.data;
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                _this.model.email = null;
                console.log("Please check: ", respData);
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Email Id deleted!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
    };
    NotifModalPage.prototype.addPhone = function (id, index) {
        var _this = this;
        if (id == 'add') {
            if (!this.data[this.notifType].phones) {
                this.data[this.notifType].phones = [];
            }
            this.data[this.notifType].phones.push(this.model.phone);
            // console.log("in addd", this.fphone);
            this.fData.contactid = this.islogin._id;
            console.log("This.data=>", this.data);
            this.fData.alert = this.data;
            console.log("finalData=>", this.fData);
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.phone = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Phone number added!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
        if (id == 'delete') {
            var splicedNum = this.data[this.notifType].phones[index];
            console.log("spliceNumber =>", splicedNum);
            this.data[this.notifType].phones.splice(index, 1);
            this.fData.contactid = this.islogin._id;
            this.fData.alert = this.data;
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.phone = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Phone number deleted!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
    };
    NotifModalPage.prototype.Close = function () {
        // this.dialogRef.close();
    };
    NotifModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-notif-modal',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/settings/notif-setting/notif-modal.html"*/'<div class="mainDiv">\n    <div class="secondDiv">\n        <form novalidate #f="ngForm" *ngIf="isAddEmail">\n            <ion-list>\n                <ion-item>\n                    <ion-label floating>Add Email</ion-label>\n                    <ion-input type="email" name="femail" [(ngModel)]="model.email" required pattern="[^ @]*@[^ @]*"\n                        #femail="ngModel"></ion-input>\n                </ion-item>\n                <div style="padding: 0px 16px 0px; color: red"\n                    *ngIf="femail.errors && (femail.dirty || femail.touched)">\n                    <p *ngIf="femail.errors.required">Email is required</p>\n                    <p *ngIf="femail.errors.pattern">Email must contain at least the @ character</p>\n                </div>\n                <!-- <pre>Valid? {{f.form.controls.email?.valid}}</pre>\n                <pre>Dirty? {{f.form.controls.email?.dirty}}</pre>\n                <pre>Touched? {{f.form.controls.email?.touched}}</pre> -->\n            </ion-list>\n            <ion-row>\n                <ion-col col-6>\n                    <div style="padding: 5px;"><button ion-button (click)="addEmail(\'add\',null)" color="gpsc" round\n                            small style="width: 100px;" [disabled]="f.invalid">Add</button></div>\n                </ion-col>\n                <ion-col col-6>\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="grey" round small\n                            style="width: 100px;">Close</button>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </form>\n        <form novalidate #f1="ngForm" *ngIf="isAddPhone">\n            <ion-list>\n                <ion-item>\n                    <ion-label floating>Add Phone Number</ion-label>\n                    <ion-input type="tel" name="fphone" [(ngModel)]="model.phone" required minlength="10" maxlength="10"\n                        #fphone="ngModel" pattern="[0-9]*"></ion-input>\n                </ion-item>\n                <div style="padding: 0px 16px 0px; color: red"\n                    *ngIf="fphone.errors && (fphone.dirty || fphone.touched)">\n                    <p *ngIf="fphone.errors.required">Phone number is required</p>\n                    <p *ngIf="fphone.errors.pattern">Phone number must contain 10 digits.</p>\n                </div>\n            </ion-list>\n            <ion-row>\n                <ion-col col-6>\n                    <div style="padding: 5px;"><button ion-button (click)="addPhone(\'add\',null)" color="gpsc" round\n                            small style="width: 100px;" [disabled]="f1.invalid">Add</button></div>\n                </ion-col>\n                <ion-col col-6>\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="grey" round small\n                            style="width: 100px;">Close</button>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </form>\n        <ion-list *ngIf="isAddEmail">\n            <ion-item *ngFor=\'let email of emailList; let i = index\'>\n                <ion-grid item-content>\n                    <ion-row>\n                        <ion-col col-2>\n                            <ion-checkbox></ion-checkbox>\n                        </ion-col>\n                        <ion-col col-8>\n                            <p style="margin-top: 8px;">{{email}}</p>\n                        </ion-col>\n                        <ion-col col-2 style="margin: auto;">\n                            <ion-icon style="font-size: 1em;" color="danger" name="trash"\n                                (click)="addEmail(\'delete\',i)"></ion-icon>\n                        </ion-col>\n                    </ion-row>\n                </ion-grid>\n            </ion-item>\n        </ion-list>\n        <ion-list *ngIf="isAddPhone">\n            <ion-item *ngFor=\'let phone of phonelist; let i = index\'>\n                <ion-grid item-content>\n                    <ion-row>\n                        <ion-col col-2>\n                            <ion-checkbox></ion-checkbox>\n                        </ion-col>\n                        <ion-col col-8>\n                            <p style="margin-top: 8px;">{{phone}}</p>\n                        </ion-col>\n                        <ion-col col-2 style="margin: auto;">\n                            <ion-icon style="font-size: 1em;" color="danger" name="trash"\n                                (click)="addPhone(\'delete\',i)"></ion-icon>\n                        </ion-col>\n                    </ion-row>\n                </ion-grid>\n            </ion-item>\n        </ion-list>\n        <!-- <pre>{{f.value | json }}</pre> -->\n    </div>\n</div>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/settings/notif-setting/notif-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], NotifModalPage);
    return NotifModalPage;
}());

//# sourceMappingURL=notif-modal.js.map

/***/ }),

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ServiceProviderPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return UpdatePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_crop__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_transfer__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










// import { TextToSpeech } from '@ionic-native/text-to-speech';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(appVersion, apiCall, alertCtrl, navCtrl, navParams, events, formBuilder, storage, platform, actionSheetCtrl, crop, camera, transfer, toastCtrl) {
        var _this = this;
        this.appVersion = appVersion;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.formBuilder = formBuilder;
        this.storage = storage;
        this.platform = platform;
        this.actionSheetCtrl = actionSheetCtrl;
        this.crop = crop;
        this.camera = camera;
        this.transfer = transfer;
        this.toastCtrl = toastCtrl;
        debugger;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.appVersion.getVersionNumber().then(function (version) {
            _this.aVer = version;
            console.log("app version=> " + _this.aVer);
        });
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.footerState = __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        this.credentialsForm = this.formBuilder.group({
            fname: [this.islogin.fn],
            lname: [this.islogin.ln],
            email: [this.islogin.email],
            phonenum: [this.islogin.phn],
            org: [this.islogin._orgName],
        });
        this.getImgUrl();
    }
    ProfilePage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter ProfilePage');
    };
    ProfilePage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    ProfilePage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    ProfilePage.prototype.getImgUrl = function () {
        var _this = this;
        // var url = this.apiCall.mainUrl + "users/shareProfileImage?uid=" + this.islogin._id;
        var url = "http://13.126.36.205/users/shareProfileImage?uid=" + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(function (resp) {
            // this.apiCall.stopLoading();
            console.log("server image url=> ", resp);
            if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
                var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
                var str1 = imgUrl.split('public/');
                _this.fileUrl = "http://13.126.36.205/" + str1[1];
            }
        }, function (err) {
            // this.apiCall.stopLoading();
        });
    };
    ProfilePage.prototype.selectImage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.pickImage(_this.camera.PictureSourceType.SAVEDPHOTOALBUM);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.pickImage(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    ProfilePage.prototype.pickImage = function (sourceType) {
        var _this = this;
        var url = "http://13.126.36.205/users/uploadProfilePicture";
        // var url = "https://www.oneqlik.in/users/uploadProfilePicture";
        var options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            // let base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log("imageData: ", imageData);
            _this.crop.crop(imageData, { quality: 100 })
                .then(function (newImage) {
                var dlink123 = newImage.split('?');
                var wear = dlink123[0];
                var fileTransfer = _this.transfer.create();
                var uploadOpts = {
                    fileKey: 'photo',
                    // fileName: imageData.substr(imageData.lastIndexOf('/') + 1)
                    fileName: wear.substr(wear.lastIndexOf('/') + 1)
                };
                // this.footerState = 1;
                _this.toggleFooter();
                _this.apiCall.startLoading().present();
                debugger;
                fileTransfer.upload(wear, url, uploadOpts)
                    .then(function (data) {
                    _this.apiCall.stopLoading();
                    console.log(data);
                    // this.selectedFile = <File>event.target.files[0];
                    var respData = data.response;
                    console.log("image data response: ", respData);
                    _this.dlUpdate(respData);
                    // this.fileUrl = this.respData.fileUrl;
                    // this.fileUrl = this.respData;
                }, function (err) {
                    _this.apiCall.stopLoading();
                    console.log(err);
                    _this.toastCtrl.create({
                        message: 'Something went wrong while uploading file... Please try after some time..',
                        duration: 2000,
                        position: 'bottom'
                    }).present();
                });
            });
        }, function (err) {
            console.log("imageData err: ", err);
            // Handle error
        });
    };
    ProfilePage.prototype.dlUpdate = function (dllink) {
        var _this = this;
        // debugger
        // let that = this;
        var dlink123 = dllink.split('?');
        var wear = dlink123[0];
        console.log("new download link: ", wear);
        // var _burl = this.apiCall.mainUrl + "users/updateImagePath";
        var _burl = "http://13.126.36.205/users/updateImagePath";
        var payload = {
            imageDoc: [dllink],
            _id: this.islogin._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_burl, payload)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log("check profile upload: ", respData);
            _this.getImgUrl();
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    ProfilePage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        console.log("footer state: ", this.footerState);
    };
    ProfilePage.prototype.settings = function () {
        this.navCtrl.push('SettingsPage');
    };
    ProfilePage.prototype.service = function () {
        this.navCtrl.push(ServiceProviderPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.password = function () {
        this.navCtrl.push(UpdatePasswordPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.onSignIn = function () {
        var _this = this;
        // this.logger.info('SignInPage: onSignIn()');
        console.log(this.credentialsForm.value);
        var data = {
            "fname": this.credentialsForm.value.fname,
            "lname": this.credentialsForm.value.lname,
            "org": this.credentialsForm.value.org,
            "noti": true,
            "uid": this.islogin._id,
            "fuel_unit": "LITRE"
        };
        this.apiCall.startLoading().present();
        this.apiCall.updateprofile(data)
            .subscribe(function (resdata) {
            _this.apiCall.stopLoading();
            console.log("response from server=> ", resdata);
            if (resdata.token) {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Profile updated succesfully!',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                var logindata = JSON.stringify(resdata);
                                var logindetails = JSON.parse(logindata);
                                var userDetails = window.atob(logindetails.token.split('.')[1]);
                                var details = JSON.parse(userDetails);
                                console.log(details.email);
                                localStorage.setItem("loginflag", "loginflag");
                                localStorage.setItem('details', JSON.stringify(details));
                                localStorage.setItem('condition_chk', details.isDealer);
                                _this.islogin = JSON.parse(localStorage.getItem('details')) || {};
                                _this.footerState = 1;
                                _this.toggleFooter();
                            }
                        }]
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error from service=> ", err);
        });
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        if (localStorage.getItem('getDevicesInterval_ID')) {
            var intervalid = localStorage.getItem('getDevicesInterval_ID');
            clearInterval(JSON.parse(intervalid));
        }
        this.token = localStorage.getItem("DEVICE_TOKEN");
        var pushdata = {};
        if (this.platform.is('android')) {
            pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "android"
            };
        }
        else {
            pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "ios"
            };
        }
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            console.log("push notifications updated " + data.message);
                            localStorage.clear();
                            localStorage.setItem('count', null);
                            _this.storage.clear().then(function () {
                                console.log("ionic storage cleared!");
                            });
                            _this.navCtrl.setRoot('LoginPage');
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        // this.menuCtrl.close();
                    }
                }]
        });
        alert.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'Me\' | translate}}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (tap)="toggleFooter()">\n        <ion-icon color="light" name="create" *ngIf="footerState == 0"></ion-icon>\n        <ion-icon color="light" name="done-all" *ngIf="footerState == 1"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item>\n      <ion-avatar item-start>\n        <img *ngIf="!fileUrl" src="assets/imgs/dummy-user-img.png">\n        <img *ngIf="fileUrl" src="{{fileUrl}}">\n      </ion-avatar>\n      <h2>{{islogin.fn}}&nbsp;{{islogin.ln}}</h2>\n      <p>{{\'Account:\' | translate}}{{islogin.account}}</p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <!-- <ion-item (tap)="service()" *ngIf="!isDealer"> -->\n    <ion-item (tap)="service()">\n      <ion-icon name="person" item-start></ion-icon>\n      {{\'Service Provider\' | translate}}\n    </ion-item>\n    <ion-item (tap)="password()">\n      <ion-icon name="lock" item-start></ion-icon>\n      {{\'Password\' | translate}}\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item (tap)="settings()">\n      <ion-icon name="cog" item-start></ion-icon>\n      {{\'Settings\' | translate}}\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group (tap)="logout()">\n    <ion-item ion-text text-center>\n      <ion-icon name="log-out"></ion-icon>&nbsp;&nbsp;{{\'Logout\' | translate}}\n    </ion-item>\n  </ion-item-group>\n\n</ion-content>\n<ion-footer padding-bottom padding-top>\n  <div style="text-align: center;">{{\'Version\' | translate}} {{aVer}}</div>\n  <br />\n  <div id="photo" style="text-align: center">\n    <span style="vertical-align:middle">{{\'Powered by\' | translate}} </span>&nbsp;\n    <a href="http://www.tracktive.in/telematics/">\n      <img style="vertical-align:middle" src="assets/imgs/sign.png" width="55" height="19">\n    </a>\n  </div>\n</ion-footer>\n\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n  <ion-content>\n    <ion-row padding>\n      <ion-col style="text-align: center; font-size: 15px">{{\'Update Profile\' | translate}}</ion-col>\n    </ion-row>\n    <div style="text-align: center;">\n      <img *ngIf="!fileUrl" class="circular--square" src="assets/imgs/dummy-user-img.png" />\n      <img *ngIf="fileUrl" class="circular--square" src="{{fileUrl}}" />\n      <ion-fab>\n        <button ion-fab mini style="margin-left: -45px; margin-top: 80px;" color="medium" (click)="selectImage()">\n          <ion-icon name="camera"></ion-icon>\n        </button>\n      </ion-fab>\n    </div>\n    <form [formGroup]="credentialsForm">\n      <ion-item>\n        <ion-label floating>{{\'First Name\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'fname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Last Name\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'lname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Email Id\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'email\']" type="email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Phone Number\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'phonenum\']" type="number"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Organisation\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'org\']" type="text"></ion-input>\n      </ion-item>\n\n\n      <ion-row>\n        <ion-col text-center>\n          <button ion-button block color="gpsc" (click)="onSignIn()">\n            {{\' Update Account\' | translate}}\n          </button>\n        </ion-col>\n      </ion-row>\n\n    </form>\n  </ion-content>\n</ion-pullup>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_crop__["a" /* Crop */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ProfilePage);
    return ProfilePage;
}());

var ServiceProviderPage = /** @class */ (function () {
    function ServiceProviderPage(navParam) {
        this.navParam = navParam;
        this.uData = {};
        this.sorted = [];
        this.uData = this.navParam.get("param");
        if (this.uData.Dealer_ID != undefined) {
            this.sorted = this.uData.Dealer_ID;
        }
        else {
            this.sorted.first_name = this.uData.fn;
            this.sorted.last_name = this.uData.ln;
            this.sorted.phone = this.uData.phn;
            this.sorted.email = this.uData.email;
        }
        console.log("udata=> ", this.uData);
        console.log("udata=> ", JSON.stringify(this.uData));
    }
    ServiceProviderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/service-provider.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{\'Provider Info\' | translate}}</ion-title>\n    </ion-navbar>\n\n</ion-header>\n<ion-content>\n    <div class="div">\n        <img src="assets/imgs/dummy_user.jpg">\n    </div>\n    <hr>\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n        <ion-col>{{\'Name:\' | translate}}</ion-col>\n        <ion-col>\n            <p style="float: right; margin:0px; padding:0px">{{sorted.first_name}}&nbsp;{{sorted.last_name}}</p>\n        </ion-col>\n    </ion-row>\n    <hr>\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n        <ion-col>{{\'Contacts\' | translate}}</ion-col>\n        <ion-col>\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n        </ion-col>\n    </ion-row>\n    <hr>\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n        <ion-col>{{"Mobile" | translate}}</ion-col>\n        <ion-col>\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n        </ion-col>\n    </ion-row>\n    <hr>\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n        <ion-col>{{"E-mail" | translate}}</ion-col>\n        <ion-col>\n            <p style="float: right; margin:0px; padding:0px">{{sorted.email}}</p>\n        </ion-col>\n    </ion-row>\n    <hr>\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n        <ion-col>{{\'Address\' | translate}}</ion-col>\n        <ion-col>\n            <p style="float: right; margin:0px; padding:0px">N/A</p>\n        </ion-col>\n    </ion-row>\n    <hr>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/service-provider.html"*/,
            selector: 'page-profile'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ServiceProviderPage);
    return ServiceProviderPage;
}());

var UpdatePasswordPage = /** @class */ (function () {
    function UpdatePasswordPage(navParam, alertCtrl, toastCtrl, apiSrv, navCtrl) {
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiSrv = apiSrv;
        this.navCtrl = navCtrl;
        this.passData = this.navParam.get("param");
        console.log("passData=> " + JSON.stringify(this.passData));
    }
    UpdatePasswordPage.prototype.savePass = function () {
        var _this = this;
        if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
            var alert_2 = this.alertCtrl.create({
                message: 'Fields should not be empty!',
                buttons: ['OK']
            });
            alert_2.present();
        }
        else {
            if (this.newP != this.cnewP) {
                var alert_3 = this.alertCtrl.create({
                    message: 'Password Missmatched!!',
                    buttons: ['Try Again']
                });
                alert_3.present();
            }
            else {
                var data = {
                    "ID": this.passData._id,
                    "OLD_PASS": this.oldP,
                    "NEW_PASS": this.newP
                };
                this.apiSrv.startLoading().present();
                this.apiSrv.updatePassword(data)
                    .subscribe(function (respData) {
                    _this.apiSrv.stopLoading();
                    console.log("respData=> ", respData);
                    var toast = _this.toastCtrl.create({
                        message: 'Password Updated successfully',
                        position: "bottom",
                        duration: 2000
                    });
                    toast.onDidDismiss(function () {
                        _this.oldP = "";
                        _this.newP = "";
                        _this.cnewP = "";
                    });
                    toast.present();
                }, function (err) {
                    _this.apiSrv.stopLoading();
                    console.log("error in update password=> ", err);
                    // debugger
                    if (err.message == "Timeout has occurred") {
                        // alert("the server is taking much time to respond. Please try in some time.")
                        var alerttemp = _this.alertCtrl.create({
                            message: "the server is taking much time to respond. Please try in some time.",
                            buttons: [{
                                    text: 'Okay',
                                    handler: function () {
                                        _this.navCtrl.setRoot("DashboardPage");
                                    }
                                }]
                        });
                        alerttemp.present();
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: err._body.message,
                            position: "bottom",
                            duration: 2000
                        });
                        toast.onDidDismiss(function () {
                            _this.oldP = "";
                            _this.newP = "";
                            _this.cnewP = "";
                            _this.navCtrl.setRoot("DashboardPage");
                        });
                        toast.present();
                    }
                });
            }
        }
    };
    UpdatePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/update-password.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{\'Change Password\' | translate}}</ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="savePass()">\n                {{\'SAVE\' | translate}}\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-list>\n        <ion-item>\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n            <ion-input placeholder="{{\'Old\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="oldP"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n            <ion-input placeholder="{{\'New\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="newP"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n            <ion-input placeholder="{{\'Confirm\' | translate}} {{\'New\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="cnewP"></ion-input>\n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/profile/update-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], UpdatePasswordPage);
    return UpdatePasswordPage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLanguages; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__(545);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppLanguages = /** @class */ (function () {
    function AppLanguages(translate) {
        this.translate = translate;
        this._lang = [
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('English'), 'en'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Hindi'), 'hi'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Marathi'), 'mr'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Bangali'), 'bn'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Tamil'), 'ta'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Telugu'), 'te'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Gujarati'), 'gu'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Kannada'), 'kn'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Malayalam'), 'ml'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Spanish'), 'sp'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Persian'), 'fa'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Arabic'), 'ar'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Nepali'), 'ne'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('French'), 'fr'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Punjabi'), 'pn')
        ];
    }
    Object.defineProperty(AppLanguages.prototype, "Languages", {
        get: function () {
            return this._lang.slice();
        },
        enumerable: true,
        configurable: true
    });
    AppLanguages = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]])
    ], AppLanguages);
    return AppLanguages;
}());

//# sourceMappingURL=app-languages.js.map

/***/ }),

/***/ 430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DetailedPage = /** @class */ (function () {
    function DetailedPage(navParams, alertCtrl) {
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.slideData = [
            {
                image: "../../assets/slides/img1.jpg"
            },
            {
                image: "../../assets/slides/img2.jpg"
            },
            {
                image: "../../assets/slides/img3.jpg"
            }
        ];
        this.deviceDetail = {};
        console.log("check parameters: ", navParams.get("param"));
        this.deviceDetail = navParams.get("param");
    }
    DetailedPage.prototype.ionViewDidEnter = function () {
        this.slides.autoplayDisableOnInteraction = false;
    };
    DetailedPage.prototype.purchase = function () {
        this.alertCtrl.create({
            message: 'Do you want to proceed to payment?',
            buttons: [{
                    text: 'Proceed',
                    handler: function () {
                    }
                },
                {
                    text: 'Back'
                }]
        }).present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Slides"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Slides"])
    ], DetailedPage.prototype, "slides", void 0);
    DetailedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-supported-devices',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/supported-devices/detailed/detailed.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Device Details</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <!-- <ion-slides pager>\n\n        <ion-slide style="background-color: green">\n          <h2>Slide 1</h2>\n        </ion-slide>\n      \n        <ion-slide style="background-color: blue">\n          <h2>Slide 2</h2>\n        </ion-slide>\n      \n        <ion-slide style="background-color: red">\n          <h2>Slide 3</h2>\n        </ion-slide>\n      \n      </ion-slides> -->\n      <!-- <ion-slides  class="slide-css" autoplay="100" loop="true" speed="3000" pager="true" autoplayDisableOnInteraction = "false"> -->\n        <ion-slides  class="slide-css" pager="true" >\n        <ion-slide *ngFor="let slide of slideData">\n        <img src="{{slide.image}}" />\n        </ion-slide>\n        </ion-slides>\n   <ion-list>\n    <ion-item>\n      <ion-avatar item-start>\n        <img src="assets/slides/img1.jpg" />\n      </ion-avatar>\n      <h2>{{deviceDetail.Product_name}}</h2>\n      <h3>Don\'t Know What To Do!</h3>\n      <p>I\'ve had a pretty messed up day. If we just...</p>\n    </ion-item>\n    </ion-list>\n\n    <p padding-left padding-right style="text-align: justify;">\n      This Tk103 GPS Vehicle Tracker is based on the GSM/GPRS network and GPS satellite positioning system, which set multiple functions of security, positioning, monitoring surveillance, emergency alarms and tracking in its entirety. It can track and monitor remote target by SMS or internet.\n    </p>\n    <ion-row>  <ion-col col-9>\n        <p padding-left padding-right><span style="font-size: 20px; color: cadetblue;">Current Stock - {{deviceDetail.current_stock}}</span></p>\n      </ion-col>\n\n      <ion-col col-3>\n        <p padding-left padding-right><ion-icon name="custom-rupee"></ion-icon> <span style="font-size: 20px;">{{deviceDetail.discount_price}}</span></p>\n      </ion-col>\n    \n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <button ion-button block color="gpsc" (click)="purchase()">Purchase</button>\n      </ion-col>\n    </ion-row>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/supported-devices/detailed/detailed.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], DetailedPage);
    return DetailedPage;
}());

//# sourceMappingURL=detailed.js.map

/***/ }),

/***/ 431:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__polyfills__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(449);



Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min__);

//# sourceMappingURL=polyfills.js.map

/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(563);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_app_languages__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_network_network__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__node_modules_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__ = __webpack_require__(572);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_android_permissions__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_app_version__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_speech_recognition__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_path__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_live_single_device_live_single_device__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_native_geocoder__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_add_devices_upload_doc_view_doc_view_doc__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_ac_report_ac_report__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_fuel_fuel_consumption_fuel_consumption__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_screen_orientation__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ngx_translate_http_loader__ = __webpack_require__(573);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__angular_common_http__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_login_login__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_geocoder_geocoder__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__providers_pager_pager__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_history_device_modal__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_profile_settings_notif_setting_notif_modal__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_customers_modals_report_setting_report_setting__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_fastag_list_fastag_fastag__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_dealers_dealer_perm_dealer_perm__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__ionic_native_sms__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__ionic_native_barcode_scanner__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_supported_devices_detailed_detailed__ = __webpack_require__(430);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_add_devices_immobilize_modal__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_dealers_AddPointsModalPage__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__providers_chat_service__ = __webpack_require__(575);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_profile_settings_notif_setting_time_picker_time_picker__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__ionic_native_sqlite__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__providers_cordova_sq_lite_cordova_sq_lite__ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__angular_common__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__ionic_native_native_audio__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_my_bookings_my_bookings__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62_angular2_signaturepad__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62_angular2_signaturepad___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_62_angular2_signaturepad__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















// Custom components











































function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_40__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, 'assets/i18n/', '.json');
}
// export function createTranslateLoader(http: Http) {
//   return new TranslateHttpLoader(http, './assets/i18n/', '.json');
// }
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_20__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */],
                __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                // OnCreate,
                __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_ac_report_ac_report__["a" /* ACDetailPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_fuel_fuel_consumption_fuel_consumption__["b" /* FuelEntryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_login_login__["a" /* LanguagesPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_history_device_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_profile_settings_notif_setting_notif_modal__["a" /* NotifModalPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_customers_modals_report_setting_report_setting__["a" /* ReportSettingModal */],
                __WEBPACK_IMPORTED_MODULE_48__pages_fastag_list_fastag_fastag__["b" /* FasttagPayNow */],
                __WEBPACK_IMPORTED_MODULE_49__pages_dealers_dealer_perm_dealer_perm__["a" /* DealerPermModalPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_supported_devices_detailed_detailed__["a" /* DetailedPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_add_devices_immobilize_modal__["a" /* ImmobilizeModelPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_dealers_AddPointsModalPage__["a" /* AddPointsModalPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_profile_settings_notif_setting_time_picker_time_picker__["a" /* TimePickerModal */],
                __WEBPACK_IMPORTED_MODULE_61__pages_my_bookings_my_bookings__["a" /* AddBookingModal */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_41__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                // scrollPadding: false,
                // scrollAssist: true,
                // autoFocusAssist: true
                // scrollAssist: false,
                // autoFocusAssist: false
                }, {
                    links: [
                        { loadChildren: '../pages/ac-report/ac-report.module#AcReportPageModule', name: 'AcReportPage', segment: 'ac-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/add-devices.module#AddDevicesPageModule', name: 'AddDevicesPage', segment: 'add-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/immobilize/immobilize.module#ImmobilizePageModule', name: 'ImmobilizePage', segment: 'immobilize', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module#PaytmwalletloginPageModule', name: 'PaytmwalletloginPage', segment: 'paytmwalletlogin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/update-device/update-device.module#UpdateDevicePageModule', name: 'UpdateDevicePage', segment: 'update-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/add-doc/add-doc.module#AddDocPageModule', name: 'AddDocPage', segment: 'add-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/upload-doc.module#UploadDocPageModule', name: 'UploadDocPage', segment: 'upload-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/vehicle-details/vehicle-details.module#VehicleDetailsPageModule', name: 'VehicleDetailsPage', segment: 'vehicle-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/all-notifications.module#AllNotificationsPageModule', name: 'AllNotificationsPage', segment: 'all-notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/notif-map/notif-map.module#NotifMapPageModule', name: 'NotifMapPage', segment: 'notif-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-us/contact-us.module#ContactUsPageModule', name: 'ContactUsPage', segment: 'contact-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-trip/create-trip.module#CreateTripPageModule', name: 'CreateTripPage', segment: 'create-trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-customer-modal/add-customer-modal.module#AddCustomerModalModule', name: 'AddCustomerModal', segment: 'add-customer-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-device-modal.module#AddDeviceModalPageModule', name: 'AddDeviceModalPage', segment: 'add-device-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/group-modal/group-modal.module#GroupModalPageModule', name: 'GroupModalPage', segment: 'group-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/update-cust/update-cust.module#UpdateCustModalPageModule', name: 'UpdateCustModalPage', segment: 'update-cust', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report-new/daily-report-new.module#DailyReportNewPageModule', name: 'DailyReportNewPage', segment: 'daily-report-new', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report/daily-report.module#DailyReportPageModule', name: 'DailyReportPage', segment: 'daily-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daywise-report/daywise-report.module#DaywiseReportPageModule', name: 'DaywiseReportPage', segment: 'daywise-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/add-dealer/add-dealer.module#AddDealerPageModule', name: 'AddDealerPage', segment: 'add-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/dealers.module#DealerPageModule', name: 'DealerPage', segment: 'dealers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/device-summary-repo/device-summary-repo.module#DeviceSummaryRepoPageModule', name: 'DeviceSummaryRepoPage', segment: 'device-summary-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/distance-report/distance-report.module#DistanceReportPageModule', name: 'DistanceReportPage', segment: 'distance-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/edit-dealer/edit-dealer.module#EditDealerPageModule', name: 'EditDealerPage', segment: 'edit-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/add-expense/add-expense.module#AddExpensePageModule', name: 'AddExpensePage', segment: 'add-expense', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/expense-type-detail/expense-type-detail.module#ExpenseTypeDetailPageModule', name: 'ExpenseTypeDetailPage', segment: 'expense-type-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/expenses.module#ExpensesPageModule', name: 'ExpensesPage', segment: 'expenses', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fastag-list/fastag-list.module#FastagListPageModule', name: 'FastagListPage', segment: 'fastag-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fastag-list/fastag/fastag.module#FastagPageModule', name: 'FastagPage', segment: 'fastag', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-chart/fuel-chart.module#FuelChartPageModule', name: 'FuelChartPage', segment: 'fuel-chart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-consumption-report/fuel-consumption-report.module#FuelConsumptionReportPageModule', name: 'FuelConsumptionReportPage', segment: 'fuel-consumption-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-consumption/fuel-consumption.module#FuelConsumptionPageModule', name: 'FuelConsumptionPage', segment: 'fuel-consumption', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-events/fuel-events.module#FuelEventsComponentModule', name: 'FuelEventsComponent', segment: 'fuel-events', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence-report/geofence-report.module#GeofenceReportPageModule', name: 'GeofenceReportPage', segment: 'geofence-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/add-geofence/add-geofence.module#AddGeofencePageModule', name: 'AddGeofencePage', segment: 'add-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence-show/geofence-show.module#GeofenceShowPageModule', name: 'GeofenceShowPage', segment: 'geofence-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence.module#GeofencePageModule', name: 'GeofencePage', segment: 'geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/idle-report/idle-report.module#IdleReportPageModule', name: 'IdleReportPage', segment: 'idle-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ignition-report/ignition-report.module#IgnitionReportPageModule', name: 'IgnitionReportPage', segment: 'ignition-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/device-settings/device-settings.module#DeviceSettingsPageModule', name: 'DeviceSettingsPage', segment: 'device-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-device/history-device.module#HistoryDevicePageModule', name: 'HistoryDevicePage', segment: 'history-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/expired/expired.module#ExpiredPageModule', name: 'ExpiredPage', segment: 'expired', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/live-single-device.module#LiveSingleDeviceModule', name: 'LiveSingleDevice', segment: 'live-single-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loading-unloading-trip/loading-unloading-trip.module#LoadingUnloadingTripPageModule', name: 'LoadingUnloadingTripPage', segment: 'loading-unloading-trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/live.module#LivePageModule', name: 'LivePage', segment: 'live', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/maintenance-reminder/add-reminder/add-reminder.module#AddReminderPageModule', name: 'AddReminderPage', segment: 'add-reminder', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/maintenance-reminder/maintenance-reminder.module#MaintenanceReminderPageModule', name: 'MaintenanceReminderPage', segment: 'maintenance-reminder', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/my-bookings/booking-detail/booking-detail.module#BookingDetailPageModule', name: 'BookingDetailPage', segment: 'booking-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/my-bookings/my-bookings.module#MyBookingsPageModule', name: 'MyBookingsPage', segment: 'my-bookings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/over-speed-repo/over-speed-repo.module#OverSpeedRepoPageModule', name: 'OverSpeedRepoPage', segment: 'over-speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/add-poi/add-poi.module#AddPoiPageModule', name: 'AddPoiPage', segment: 'add-poi', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/poi-list.module#PoiListPageModule', name: 'PoiListPage', segment: 'poi-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-report/poi-report.module#POIReportPageModule', name: 'POIReportPage', segment: 'poi-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/notif-setting/notif-setting.module#NotifSettingPageModule', name: 'NotifSettingPage', segment: 'notif-setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-map-show/route-map-show.module#RouteMapShowPageModule', name: 'RouteMapShowPage', segment: 'route-map-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-voilations/route-voilations.module#RouteVoilationsPageModule', name: 'RouteVoilationsPage', segment: 'route-voilations', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route/route.module#RoutePageModule', name: 'RoutePage', segment: 'route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup-otp/signup-otp.module#SignupOtpPageModule', name: 'SignupOtpPage', segment: 'signup-otp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/siren-alert/siren-alert.module#SirenAlertPageModule', name: 'SirenAlertPage', segment: 'siren-alert', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sos-report/sos-report.module#SosReportPageModule', name: 'SosReportPage', segment: 'sos-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speed-repo/speed-repo.module#SpeedRepoPageModule', name: 'SpeedRepoPage', segment: 'speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stoppages-repo/stoppages-repo.module#StoppagesRepoPageModule', name: 'StoppagesRepoPage', segment: 'stoppages-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/supported-devices/supported-devices.module#SupportedDevicesPageModule', name: 'SupportedDevicesPage', segment: 'supported-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-report.module#TripReportPageModule', name: 'TripReportPage', segment: 'trip-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-review/trip-review.module#TripReviewPageModule', name: 'TripReviewPage', segment: 'trip-review', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/working-hours-report/working-hours-report.module#WorkingHoursReportPageModule', name: 'WorkingHoursReportPage', segment: 'working-hours-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/your-trips/your-trips.module#YourTripsPageModule', name: 'YourTripsPage', segment: 'your-trips', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_62_angular2_signaturepad__["SignaturePadModule"],
                __WEBPACK_IMPORTED_MODULE_26__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_41__angular_common_http__["a" /* HttpClient */]],
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_15__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_ac_report_ac_report__["a" /* ACDetailPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_fuel_fuel_consumption_fuel_consumption__["b" /* FuelEntryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_login_login__["a" /* LanguagesPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_history_device_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_profile_settings_notif_setting_notif_modal__["a" /* NotifModalPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_customers_modals_report_setting_report_setting__["a" /* ReportSettingModal */],
                __WEBPACK_IMPORTED_MODULE_48__pages_fastag_list_fastag_fastag__["b" /* FasttagPayNow */],
                __WEBPACK_IMPORTED_MODULE_49__pages_dealers_dealer_perm_dealer_perm__["a" /* DealerPermModalPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_supported_devices_detailed_detailed__["a" /* DetailedPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_add_devices_immobilize_modal__["a" /* ImmobilizeModelPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_dealers_AddPointsModalPage__["a" /* AddPointsModalPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_profile_settings_notif_setting_time_picker_time_picker__["a" /* TimePickerModal */],
                __WEBPACK_IMPORTED_MODULE_61__pages_my_bookings_my_bookings__["a" /* AddBookingModal */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"],
                // NativePageTransitions,
                // [{ provide: ErrorHandler, useClass: MyErrorHandler }],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_11__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_12__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__["h" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_8__providers_app_languages__["a" /* AppLanguages */],
                __WEBPACK_IMPORTED_MODULE_43__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
                __WEBPACK_IMPORTED_MODULE_43__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
                __WEBPACK_IMPORTED_MODULE_44__providers_pager_pager__["a" /* PagerProvider */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_50__ionic_native_sms__["a" /* SMS */],
                __WEBPACK_IMPORTED_MODULE_51__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_speech_recognition__["a" /* SpeechRecognition */],
                __WEBPACK_IMPORTED_MODULE_55__providers_chat_service__["a" /* ChatService */],
                __WEBPACK_IMPORTED_MODULE_57__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_58__providers_cordova_sq_lite_cordova_sq_lite__["a" /* CordovaSqLiteProvider */],
                __WEBPACK_IMPORTED_MODULE_59__angular_common__["DatePipe"],
                __WEBPACK_IMPORTED_MODULE_60__ionic_native_native_audio__["a" /* NativeAudio */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 481:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 200,
	"./af.js": 200,
	"./ar": 201,
	"./ar-dz": 202,
	"./ar-dz.js": 202,
	"./ar-kw": 203,
	"./ar-kw.js": 203,
	"./ar-ly": 204,
	"./ar-ly.js": 204,
	"./ar-ma": 205,
	"./ar-ma.js": 205,
	"./ar-sa": 206,
	"./ar-sa.js": 206,
	"./ar-tn": 207,
	"./ar-tn.js": 207,
	"./ar.js": 201,
	"./az": 208,
	"./az.js": 208,
	"./be": 209,
	"./be.js": 209,
	"./bg": 210,
	"./bg.js": 210,
	"./bm": 211,
	"./bm.js": 211,
	"./bn": 212,
	"./bn.js": 212,
	"./bo": 213,
	"./bo.js": 213,
	"./br": 214,
	"./br.js": 214,
	"./bs": 215,
	"./bs.js": 215,
	"./ca": 216,
	"./ca.js": 216,
	"./cs": 217,
	"./cs.js": 217,
	"./cv": 218,
	"./cv.js": 218,
	"./cy": 219,
	"./cy.js": 219,
	"./da": 220,
	"./da.js": 220,
	"./de": 221,
	"./de-at": 222,
	"./de-at.js": 222,
	"./de-ch": 223,
	"./de-ch.js": 223,
	"./de.js": 221,
	"./dv": 224,
	"./dv.js": 224,
	"./el": 225,
	"./el.js": 225,
	"./en-au": 226,
	"./en-au.js": 226,
	"./en-ca": 227,
	"./en-ca.js": 227,
	"./en-gb": 228,
	"./en-gb.js": 228,
	"./en-ie": 229,
	"./en-ie.js": 229,
	"./en-il": 230,
	"./en-il.js": 230,
	"./en-nz": 231,
	"./en-nz.js": 231,
	"./eo": 232,
	"./eo.js": 232,
	"./es": 233,
	"./es-do": 234,
	"./es-do.js": 234,
	"./es-us": 235,
	"./es-us.js": 235,
	"./es.js": 233,
	"./et": 236,
	"./et.js": 236,
	"./eu": 237,
	"./eu.js": 237,
	"./fa": 238,
	"./fa.js": 238,
	"./fi": 239,
	"./fi.js": 239,
	"./fo": 240,
	"./fo.js": 240,
	"./fr": 241,
	"./fr-ca": 242,
	"./fr-ca.js": 242,
	"./fr-ch": 243,
	"./fr-ch.js": 243,
	"./fr.js": 241,
	"./fy": 244,
	"./fy.js": 244,
	"./gd": 245,
	"./gd.js": 245,
	"./gl": 246,
	"./gl.js": 246,
	"./gom-latn": 247,
	"./gom-latn.js": 247,
	"./gu": 248,
	"./gu.js": 248,
	"./he": 249,
	"./he.js": 249,
	"./hi": 250,
	"./hi.js": 250,
	"./hr": 251,
	"./hr.js": 251,
	"./hu": 252,
	"./hu.js": 252,
	"./hy-am": 253,
	"./hy-am.js": 253,
	"./id": 254,
	"./id.js": 254,
	"./is": 255,
	"./is.js": 255,
	"./it": 256,
	"./it.js": 256,
	"./ja": 257,
	"./ja.js": 257,
	"./jv": 258,
	"./jv.js": 258,
	"./ka": 259,
	"./ka.js": 259,
	"./kk": 260,
	"./kk.js": 260,
	"./km": 261,
	"./km.js": 261,
	"./kn": 262,
	"./kn.js": 262,
	"./ko": 263,
	"./ko.js": 263,
	"./ky": 264,
	"./ky.js": 264,
	"./lb": 265,
	"./lb.js": 265,
	"./lo": 266,
	"./lo.js": 266,
	"./lt": 267,
	"./lt.js": 267,
	"./lv": 268,
	"./lv.js": 268,
	"./me": 269,
	"./me.js": 269,
	"./mi": 270,
	"./mi.js": 270,
	"./mk": 271,
	"./mk.js": 271,
	"./ml": 272,
	"./ml.js": 272,
	"./mn": 273,
	"./mn.js": 273,
	"./mr": 274,
	"./mr.js": 274,
	"./ms": 275,
	"./ms-my": 276,
	"./ms-my.js": 276,
	"./ms.js": 275,
	"./mt": 277,
	"./mt.js": 277,
	"./my": 278,
	"./my.js": 278,
	"./nb": 279,
	"./nb.js": 279,
	"./ne": 280,
	"./ne.js": 280,
	"./nl": 281,
	"./nl-be": 282,
	"./nl-be.js": 282,
	"./nl.js": 281,
	"./nn": 283,
	"./nn.js": 283,
	"./pa-in": 284,
	"./pa-in.js": 284,
	"./pl": 285,
	"./pl.js": 285,
	"./pt": 286,
	"./pt-br": 287,
	"./pt-br.js": 287,
	"./pt.js": 286,
	"./ro": 288,
	"./ro.js": 288,
	"./ru": 289,
	"./ru.js": 289,
	"./sd": 290,
	"./sd.js": 290,
	"./se": 291,
	"./se.js": 291,
	"./si": 292,
	"./si.js": 292,
	"./sk": 293,
	"./sk.js": 293,
	"./sl": 294,
	"./sl.js": 294,
	"./sq": 295,
	"./sq.js": 295,
	"./sr": 296,
	"./sr-cyrl": 297,
	"./sr-cyrl.js": 297,
	"./sr.js": 296,
	"./ss": 298,
	"./ss.js": 298,
	"./sv": 299,
	"./sv.js": 299,
	"./sw": 300,
	"./sw.js": 300,
	"./ta": 301,
	"./ta.js": 301,
	"./te": 302,
	"./te.js": 302,
	"./tet": 303,
	"./tet.js": 303,
	"./tg": 304,
	"./tg.js": 304,
	"./th": 305,
	"./th.js": 305,
	"./tl-ph": 306,
	"./tl-ph.js": 306,
	"./tlh": 307,
	"./tlh.js": 307,
	"./tr": 308,
	"./tr.js": 308,
	"./tzl": 309,
	"./tzl.js": 309,
	"./tzm": 310,
	"./tzm-latn": 311,
	"./tzm-latn.js": 311,
	"./tzm.js": 310,
	"./ug-cn": 312,
	"./ug-cn.js": 312,
	"./uk": 313,
	"./uk.js": 313,
	"./ur": 314,
	"./ur.js": 314,
	"./uz": 315,
	"./uz-latn": 316,
	"./uz-latn.js": 316,
	"./uz.js": 315,
	"./vi": 317,
	"./vi.js": 317,
	"./x-pseudo": 318,
	"./x-pseudo.js": 318,
	"./yo": 319,
	"./yo.js": 319,
	"./zh-cn": 320,
	"./zh-cn.js": 320,
	"./zh-hk": 321,
	"./zh-hk.js": 321,
	"./zh-tw": 322,
	"./zh-tw.js": 322
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 481;

/***/ }),

/***/ 500:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 502:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 532:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 543:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapStyle; });
var mapStyle = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#263c3f"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#6b9a76"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#38414e"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#212a37"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9ca5b3"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#1f2835"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#f3d19c"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#2f3948"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#515c6d"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    }
];
//# sourceMappingURL=map-style.model.js.map

/***/ }),

/***/ 545:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Language; });
var Language = /** @class */ (function () {
    function Language(name, key) {
        this.name = name;
        this.key = key;
    }
    return Language;
}());

//# sourceMappingURL=app.model.js.map

/***/ }),

/***/ 563:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_network_network__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_ReplaySubject__ = __webpack_require__(566);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_ReplaySubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_ReplaySubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_api_service_api_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_text_to_speech__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_app_version__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












// import { Network } from '@ionic-native/network';
var MyApp = /** @class */ (function () {
    function MyApp(translate, platform, statusBar, splashScreen, events, networkProvider, 
    // public network: Network,
    // public menuProvider: MenuProvider,
    menuCtrl, modalCtrl, push, alertCtrl, app, apiCall, toastCtrl, tts, appVersion) {
        // setTimeout(() => {
        //   this.resize();
        // }, 2000);
        // console.log("check network: ", this.networkProvider.getCurrentNetworkStatus());
        // this.networkProvider.initializeNetworkEvents();
        var _this = this;
        this.translate = translate;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.networkProvider = networkProvider;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.tts = tts;
        this.appVersion = appVersion;
        this.islogin = {};
        // Settings for the SideMenuContentComponent
        this.sideMenuSettings = {
            accordionMode: true,
            showSelectedOption: true,
            selectedOptionClass: 'active-side-menu-option'
        };
        this.unreadCountObservable = new __WEBPACK_IMPORTED_MODULE_7_rxjs_ReplaySubject__["ReplaySubject"](0);
        this.textDir = "ltr";
        this.pageRoot = "DashboardPage";
        // this.networkProvider.onNetworkChange().subscribe(res => {
        //   debugger
        // console.log("observer: ", res);
        // });
        debugger;
        if (navigator.onLine) {
            console.log('Internet is connected');
        }
        else {
            var alert_1 = this.alertCtrl.create({
                message: 'No internet connection. Make sure that Wi-Fi or mobile data turned on, then try again.',
                buttons: [
                    {
                        text: 'Okay',
                        handler: function () {
                            // this.platform.exitApp(); // Close this application
                        }
                    }
                ]
            });
            alert_1.present();
            console.log('No internet connection');
        }
        this.callBaseURL();
        this.headerContent = "header";
        // translate.setDefaultLang('en');
        this.events.subscribe('lang:key', function (key) {
            console.log("subscribed key: ", key);
            translate.setDefaultLang(key);
            translate.use(key);
            // this.translate.use('sp');
        });
        this.events.subscribe('latest:version', (function (data) {
            console.log("latest version available? ", data);
            _this.appVersion.getVersionNumber().then(function (version) {
                console.log("app version: ", version);
                // debugger;
                if (version < data) {
                    var alert_2 = _this.alertCtrl.create({
                        message: 'New version is available. Please update application. Latest Version - ' + data.Version,
                        buttons: [
                            {
                                text: 'Cancel'
                            },
                            {
                                text: 'Proceed',
                                handler: function () {
                                    window.open("https://play.google.com/store/apps/details?id=com.OneQlikVTS.ionic", "_blank");
                                }
                            }
                        ]
                    });
                    alert_2.present();
                    // alert("app update is avalible");
                }
            });
        }));
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
            _this.isDealer = udata.isDealer;
            console.log("islogin=> " + JSON.stringify(_this.islogin));
            if (localStorage.getItem('FirstGreetDone') === null) {
                _this.greetings();
            }
            _this.getImgUrl();
            _this.checkReportStatus();
        });
        this.events.subscribe('Screen:Changed', function (screenData) {
            console.log("screen data changed: ", screenData);
            if (screenData === 'vehiclelist') {
                _this.rootPage = 'AddDevicesPage';
                _this.pageRoot = 'AddDevicesPage';
            }
            else if (screenData === 'live') {
                _this.rootPage = 'LivePage';
                _this.pageRoot = 'LivePage';
            }
            else {
                _this.rootPage = 'DashboardPage';
                _this.pageRoot = 'DashboardPage';
            }
        });
        platform.ready().then(function () {
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
            platform.registerBackButtonAction(function () {
                var nav = _this.app.getActiveNavs()[0];
                var activeView = nav.getActive();
                if (activeView.name == "DashboardPage") {
                    var alert_3 = _this.alertCtrl.create({
                        title: 'App termination',
                        message: 'Do you want to close the app?',
                        buttons: [{
                                text: 'Cancel',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Application exit prevented!');
                                }
                            }, {
                                text: 'Close App',
                                handler: function () {
                                    _this.platform.exitApp(); // Close this application
                                }
                            }]
                    });
                    alert_3.present();
                }
                else {
                    if (nav.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
                        nav.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
                    }
                    else {
                        platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
                    }
                }
            });
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin=> " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
        this.dealerStatus = this.islogin.isDealer;
        // this.getSideMenuData();
        this.initializeApp();
        if (localStorage.getItem("loginflag")) {
            if (localStorage.getItem("SCREEN") != null) {
                if (localStorage.getItem("SCREEN") === 'live') {
                    this.rootPage = 'LivePage';
                }
                else if (localStorage.getItem("SCREEN") === 'dashboard') {
                    this.rootPage = 'DashboardPage';
                }
                else if (localStorage.getItem("SCREEN") === 'vehiclelist') {
                    this.rootPage = 'AddDevicesPage';
                }
            }
            else {
                this.rootPage = 'DashboardPage';
            }
        }
        else {
            this.rootPage = 'LoginPage';
        }
    }
    MyApp.prototype.ionViewWillEnter = function () {
        console.log("ionViewWillEnter");
        this.chkCondition();
    };
    // ngOnInit() {
    //   this.resize();
    // }
    MyApp.prototype.greetings = function () {
        var welcome;
        var date = new Date();
        var hour = date.getHours();
        if (hour < 12) {
            welcome = "good morning ";
        }
        else if (hour < 17) {
            welcome = "good afternoon ";
        }
        else {
            welcome = "good evening ";
        }
        // this.tts.isLocaleAvailable()
        this.tts.speak({
            text: welcome + this.islogin.fn + " " + this.islogin.ln,
            locale: 'en-UK',
        })
            .then(function () {
            console.log('Success');
            localStorage.setItem('FirstGreetDone', 'true');
        })
            .catch(function (reason) { return console.log(reason); });
    };
    MyApp.prototype.callBaseURL = function () {
        var _this = this;
        // debugger
        var url = "https://www.oneqlik.in/pullData/getUrlnew";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (data) {
            console.log("base url: ", data);
            if (data.url) {
                localStorage.setItem("BASE_URL", JSON.stringify(data.url));
            }
            if (data.app_name === 'oneqlik') {
                if (data.Version) {
                    _this.events.publish('latest:version', data.Version);
                }
            }
            if (data.socket) {
                localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
            }
        });
    };
    // getSideMenuData() {
    //   this.pages = this.menuProvider.getSideMenus();
    // }
    MyApp.prototype.pushNotify = function () {
        var that = this;
        that.push.hasPermission() // to check if we have permission
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
                that.pushSetup();
            }
            else {
                console.log('We do not have permission to send push notifications');
                that.pushSetup();
            }
        });
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
        this.push.createChannel({
            id: "ignitionon",
            description: "ignition on description",
            sound: 'ignitionon',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('ignitionon Channel created'); });
        this.push.createChannel({
            id: "ignitionoff",
            description: "ignition off description",
            sound: 'ignitionoff',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('ignitionoff Channel created'); });
        this.push.createChannel({
            id: "devicepowerdisconnected",
            description: "devicepowerdisconnected description",
            sound: 'devicepowerdisconnected',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('devicepowerdisconnected Channel created'); });
        this.push.createChannel({
            id: "default",
            description: "default description",
            sound: 'default',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('default Channel created'); });
        this.push.createChannel({
            id: "sos",
            description: "default description",
            sound: 'notif',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('sos Channel created'); });
        this.push.createChannel({
            id: "theft",
            description: "default description",
            sound: 'theft',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('theft Channel created'); });
        this.push.createChannel({
            id: "tow",
            description: "default description",
            sound: 'tow',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('tow Channel created'); });
        // Delete a channel (Android O and above)
        // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));
        // Return a list of currently configured channels
        this.push.listChannels().then(function (channels) { return console.log('List of channels', channels); });
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '644983599736',
                icon: 'icicon',
                iconColor: 'red'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            debugger;
            if (localStorage.getItem("notifValue") != null) {
                if (localStorage.getItem("notifValue") == 'true') {
                    // this.tts.speak(notification.message)
                    //   .then(() => console.log('Success'))
                    //   .catch((reason: any) => console.log(reason));
                }
            }
            if (notification.additionalData.coldstart) {
                console.log(notification);
                var str = notification.message;
                var strArray = str.split(' ');
                console.log("splited string ", strArray[0]);
                var vehName = void 0;
                if (strArray[0] === 'Tow' || strArray[0] === 'Theft') {
                    if (strArray[3] === 'Vehicle') {
                        var totLength = strArray.length - 4;
                        if (totLength === 1) {
                            vehName = strArray[4];
                        }
                        else {
                            vehName = strArray[4] + (strArray[5] ? (' ' + strArray[5]) : '') + (strArray[6] ? (' ' + strArray[6]) : '') + (strArray[7] ? (' ' + strArray[7]) : '') + (strArray[8] ? (' ' + strArray[8]) : '') + (strArray[9] ? (' ' + strArray[9]) : '');
                        }
                    }
                    _this.nav.push('SirenAlertPage', {
                        "vehName": vehName
                    });
                }
                else
                    _this.nav.setRoot('AllNotificationsPage');
                // Background
            }
            else if (notification.additionalData.foreground) {
                console.log("Notification text: ", notification.message);
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
                var str = notification.message;
                var strArray = str.split(' ');
                console.log("splited string ", strArray[0]);
                var vehName = void 0;
                if (strArray[0] === 'Tow' || strArray[0] === 'Theft') {
                    if (strArray[3] === 'Vehicle') {
                        var totLength = strArray.length - 4;
                        if (totLength === 1) {
                            vehName = strArray[4];
                        }
                        else {
                            vehName = strArray[4] + (strArray[5] ? (' ' + strArray[5]) : '') + (strArray[6] ? (' ' + strArray[6]) : '') + (strArray[7] ? (' ' + strArray[7]) : '') + (strArray[8] ? (' ' + strArray[8]) : '') + (strArray[9] ? (' ' + strArray[9]) : '');
                        }
                    }
                    _this.nav.push('SirenAlertPage', {
                        "vehName": vehName
                    });
                }
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            // alert(registration.registrationId)
            console.log("device token => " + registration.registrationId);
            // console.log("reg type=> " + registration.registrationType);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
            // that.storage.set("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
            // alert('Error with Push plugin' + error)
        });
    };
    MyApp.prototype.showToastMsg = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var that = this;
        that.platform.ready().then(function () {
            that.platform.pause.subscribe(function (ev) {
                console.log("platform paused");
                localStorage.setItem("backgroundModeTime", JSON.stringify(new Date()));
            });
            that.pushNotify();
        });
        // Initialize some options
        that.initializeOptions();
        // Change the value for the batch every 5 seconds
        setInterval(function () {
            _this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
        }, 5000);
    };
    MyApp.prototype.checkReportStatus = function () {
        this.suboptions = [];
        this.fuelSuboptions = [];
        if (this.islogin.report_preference.ac_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('AC Report'),
                component: 'AcReportPage'
            });
        }
        if (this.islogin.report_preference.daily_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Daily Report'),
                component: 'DailyReportPage'
            });
        }
        if (this.islogin.report_preference.daywise_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Daywise Report'),
                component: 'DaywiseReportPage'
            });
        }
        if (this.islogin.report_preference.distance_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Distance Report'),
                component: 'DistanceReportPage'
            });
        }
        if (this.islogin.report_preference.geofence_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Geofence Report'),
                component: 'GeofenceReportPage'
            });
        }
        if (this.islogin.report_preference.idle_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Idle Report'),
                component: 'IdleReportPage'
            });
        }
        if (this.islogin.report_preference.ignition_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Ignition Report'),
                component: 'IgnitionReportPage'
            });
        }
        this.suboptions.push({
            iconName: 'clipboard',
            displayText: this.translate.instant('Loading Unloading Trip'),
            component: 'LoadingUnloadingTripPage'
        });
        if (this.islogin.report_preference.overspeed_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Over Speed Report'),
                component: 'OverSpeedRepoPage'
            });
        }
        if (this.islogin.report_preference.poi_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('POI Report'),
                component: 'POIReportPage'
            });
        }
        if (this.islogin.report_preference.route_violation_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Route Violation Report'),
                component: 'RouteVoilationsPage'
            });
        }
        if (this.islogin.report_preference.sos_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('SOS'),
                component: 'SosReportPage'
            });
        }
        if (this.islogin.report_preference.speed_variation.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Speed Variation Report'),
                component: 'SpeedRepoPage'
            });
        }
        if (this.islogin.report_preference.stoppage_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Stoppages Report'),
                component: 'StoppagesRepoPage'
            });
        }
        if (this.islogin.report_preference.summary_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Summary Report'),
                component: 'DeviceSummaryRepoPage'
            });
        }
        if (this.islogin.report_preference.trip_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Trip Report'),
                component: 'TripReportPage'
            });
        }
        if (this.islogin.report_preference.user_trip_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('User Trip report'),
                component: 'YourTripsPage'
            });
        }
        this.suboptions.push({
            iconName: 'clipboard',
            displayText: this.translate.instant('Value Screen'),
            component: 'DailyReportNewPage'
        });
        if (this.islogin.report_preference.user_trip_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Working Hours Report'),
                component: 'WorkingHoursReportPage'
            });
        }
        console.log("suboptions list: ", this.suboptions);
        this.fuelSuboptions.push({
            iconName: 'custom-fuel',
            displayText: this.translate.instant('Fuel Entry'),
            component: 'FuelConsumptionPage'
        });
        if (this.islogin.report_preference.fuel_consumption_report.Rstatus) {
            this.fuelSuboptions.push({
                iconName: 'custom-fuel',
                displayText: this.translate.instant('Fuel Consumption'),
                component: 'FuelConsumptionReportPage'
            });
        }
        this.fuelSuboptions.push({
            iconName: 'custom-fuel',
            displayText: this.translate.instant('Fuel Chart'),
            component: 'FuelChartPage'
        });
    };
    MyApp.prototype.onOptionSelected = function (option) {
        var _this = this;
        this.menuCtrl.close().then(function () {
            if (option.custom && option.custom.isLogin) {
                _this.presentAlert('You\'ve clicked the login option!');
            }
            else if (option.custom && option.custom.isLogout) {
                _this.presentAlert('You\'ve clicked the logout option!');
            }
            else if (option.custom && option.custom.isExternalLink) {
                var url = option.custom.externalUrl;
                window.open(url, '_blank');
            }
            else {
                var params = option.custom && option.custom.param;
                if (option.displayText == _this.translate.instant('Admin') && option.component == 'DashboardPage') {
                    localStorage.setItem("dealer_status", 'OFF');
                    localStorage.setItem('details', localStorage.getItem("superAdminData"));
                    localStorage.removeItem('superAdminData');
                }
                if (option.displayText == _this.translate.instant('Dealers') && option.component == 'DashboardPage') {
                    if (localStorage.getItem('custumer_status') == 'ON') {
                        var _dealdata = JSON.parse(localStorage.getItem("dealer"));
                        if (localStorage.getItem("superAdminData") != null || _this.islogin.isSuperAdmin == true) {
                            localStorage.setItem("dealer_status", 'ON');
                        }
                        else {
                            if (_dealdata.isSuperAdmin == true) {
                                localStorage.setItem("dealer_status", 'OFF');
                            }
                            else {
                                localStorage.setItem("OnlyDealer", "true");
                            }
                        }
                        localStorage.setItem("custumer_status", 'OFF');
                        localStorage.setItem('details', localStorage.getItem("dealer"));
                    }
                    else {
                        console.log("something wrong!!");
                    }
                }
                _this.nav.setRoot(option.component, params);
            }
        });
    };
    MyApp.prototype.collapseMenuOptions = function () {
        this.sideMenu.collapseAllOptions();
    };
    MyApp.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Information',
            message: message,
            buttons: ['Ok']
        });
        alert.present();
    };
    MyApp.prototype.openPage = function (page, index) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component) {
            this.nav.setRoot(page.component);
            this.menuCtrl.close();
        }
        else {
            if (this.selectedMenu) {
                this.selectedMenu = 0;
            }
            else {
                this.selectedMenu = index;
            }
        }
    };
    MyApp.prototype.resize = function () {
        var offset = this.headerTag.nativeElement.offsetHeight;
        this.scrollableTag.nativeElement.style.marginTop = offset + 'px';
    };
    MyApp.prototype.chkCondition = function () {
        var _this = this;
        // this.resize();
        this.events.subscribe("event_sidemenu", function (data) {
            _this.islogin = JSON.parse(data);
            _this.options[2].displayText = _this.translate.instant('Dealers');
            _this.options[2].iconName = 'person';
            _this.options[2].component = 'DashboardPage';
            _this.initializeOptions();
        });
        this.initializeOptions();
    };
    MyApp.prototype.initializeOptions = function () {
        var _this = this;
        this.options = new Array();
        // if ((this.islogin.role == undefined || this.islogin.role == 'supAdm' || this.islogin.role == 'adm') && this.islogin.isSuperAdmin == true && (this.islogin.isDealer == false || this.islogin.isDealer == undefined)) {
        if (this.islogin.isSuperAdmin === true && (this.islogin.isDealer === false || this.islogin.isDealer === undefined)) {
            this.options.push({
                iconName: 'home',
                displayText: this.translate.instant('Home'),
                component: this.pageRoot,
            });
            this.options.push({
                iconName: 'people',
                displayText: this.translate.instant('Groups'),
                component: 'GroupsPage'
            });
            this.options.push({
                iconName: 'people',
                displayText: this.translate.instant('Dealers'),
                component: 'DealerPage'
            });
            this.options.push({
                iconName: 'contacts',
                displayText: this.translate.instant('Customers'),
                component: 'CustomersPage'
            });
            this.options.push({
                iconName: 'calendar',
                displayText: this.translate.instant('My Bookings'),
                component: 'MyBookingsPage'
            });
            this.options.push({
                iconName: 'list',
                displayText: this.translate.instant('POI List'),
                component: 'PoiListPage'
            });
            this.options.push({
                iconName: 'cash',
                displayText: this.translate.instant('Expenses'),
                component: 'ExpensesPage'
            });
            this.options.push({
                displayText: this.translate.instant('Fuel'),
                iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Entry'),
                        component: 'FuelConsumptionPage'
                    },
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Consumption'),
                        component: 'FuelConsumptionReportPage'
                    },
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Chart'),
                        component: 'FuelChartPage'
                    }
                ]
            });
            this.options.push({
                displayText: this.translate.instant('Vehicle Maintenance'),
                iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'notifications',
                        displayText: this.translate.instant('Reminders'),
                        component: 'MaintenanceReminderPage'
                    }
                ]
            });
            // Load options with nested items (with icons)
            // -----------------------------------------------
            this.options.push({
                displayText: this.translate.instant(this.translate.instant('Reports')),
                iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('AC Report'),
                        component: 'AcReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Daily Report'),
                        component: 'DailyReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Daywise Report'),
                        component: 'DaywiseReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Distance Report'),
                        component: 'DistanceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Geofence Report'),
                        component: 'GeofenceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Idle Report',
                        component: 'IdleReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Ignition Report'),
                        component: 'IgnitionReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Loading Unloading Trip',
                        component: 'LoadingUnloadingTripPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Over Speed Report'),
                        component: 'OverSpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('POI Report'),
                        component: 'POIReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Route Violation Report'),
                        component: 'RouteVoilationsPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('SOS'),
                        component: 'SosReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Speed Variation Report'),
                        component: 'SpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Stoppages Report'),
                        component: 'StoppagesRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Summary Report'),
                        component: 'DeviceSummaryRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Trip Report'),
                        component: 'TripReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('User Trip report'),
                        component: 'YourTripsPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Value Screen'),
                        component: 'DailyReportNewPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Working Hours Report'),
                        component: 'WorkingHoursReportPage'
                    }
                ]
            });
            // Load options with nested items (without icons)
            // -----------------------------------------------
            // this.options.push({
            //   displayText: this.translate.instant('Routes'),
            //   iconName: 'map',
            //   component: 'RoutePage'
            // });
            // Load special options
            // -----------------------------------------------
            this.options.push({
                displayText: this.translate.instant('Customer Support'),
                suboptions: [
                    {
                        iconName: 'star',
                        displayText: this.translate.instant('Rate Us'),
                        component: 'FeedbackPage'
                    },
                    {
                        iconName: 'mail',
                        displayText: this.translate.instant('Contact Us'),
                        component: 'ContactUsPage'
                    }
                ]
            });
            this.options.push({
                displayText: this.translate.instant('Profile'),
                iconName: 'person',
                component: 'ProfilePage'
            });
        }
        else {
            // if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == false) {
            if (this.islogin.isSuperAdmin === false && (this.islogin.isDealer === false || this.islogin.isDealer === undefined)) {
                this.options.push({
                    iconName: 'home',
                    displayText: this.translate.instant('Home'),
                    component: this.pageRoot,
                });
                this.options.push({
                    iconName: 'people',
                    displayText: this.translate.instant('Groups'),
                    component: 'GroupsPage'
                });
                this.options.push({
                    iconName: 'calendar',
                    displayText: this.translate.instant('My Bookings'),
                    component: 'MyBookingsPage'
                });
                this.options.push({
                    iconName: 'list',
                    displayText: this.translate.instant('POI List'),
                    component: 'PoiListPage'
                });
                this.options.push({
                    iconName: 'cash',
                    displayText: this.translate.instant('Expenses'),
                    component: 'ExpensesPage'
                });
                this.options.push({
                    iconName: 'custom-fastag',
                    displayText: this.translate.instant('Fastag'),
                    component: 'FastagListPage'
                });
                this.options.push({
                    displayText: this.translate.instant('Fuel'),
                    iconName: 'arrow-dropright',
                    suboptions: [
                        {
                            iconName: 'custom-fuel',
                            displayText: this.translate.instant('Fuel Entry'),
                            component: 'FuelConsumptionPage'
                        },
                        {
                            iconName: 'custom-fuel',
                            displayText: this.translate.instant('Fuel Consumption'),
                            component: 'FuelConsumptionReportPage'
                        },
                        {
                            iconName: 'custom-fuel',
                            displayText: this.translate.instant('Fuel Chart'),
                            component: 'FuelChartPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: this.translate.instant('Vehicle Maintenance'),
                    iconName: 'arrow-dropright',
                    suboptions: [
                        {
                            iconName: 'notifications',
                            displayText: this.translate.instant('Reminders'),
                            component: 'MaintenanceReminderPage'
                        }
                    ]
                });
                // Load options with nested items (with icons)
                // -----------------------------------------------
                this.options.push({
                    displayText: this.translate.instant('Reports'),
                    iconName: 'arrow-dropright',
                    suboptions: this.suboptions
                });
                // Load options with nested items (without icons)
                // -----------------------------------------------
                // this.options.push({
                //   displayText: this.translate.instant('Routes'),
                //   iconName: 'map',
                //   component: 'RoutePage'
                // });
                // Load special options
                // -----------------------------------------------
                this.options.push({
                    displayText: this.translate.instant('Customer Support'),
                    suboptions: [
                        {
                            iconName: 'star',
                            displayText: this.translate.instant('Rate Us'),
                            component: 'FeedbackPage'
                        },
                        {
                            iconName: 'mail',
                            displayText: this.translate.instant('Contact Us'),
                            component: 'ContactUsPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: this.translate.instant('Profile'),
                    iconName: 'person',
                    component: 'ProfilePage'
                });
            }
            else {
                // if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == true) {
                if (this.islogin.isSuperAdmin === false && this.islogin.isDealer === true) {
                    this.options.push({
                        iconName: 'home',
                        displayText: this.translate.instant('Home'),
                        component: this.pageRoot,
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: this.translate.instant('Groups'),
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'contacts',
                        displayText: this.translate.instant('Customers'),
                        component: 'CustomersPage'
                    });
                    this.options.push({
                        iconName: 'calendar',
                        displayText: this.translate.instant('My Bookings'),
                        component: 'MyBookingsPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: this.translate.instant('POI List'),
                        component: 'PoiListPage'
                    });
                    this.options.push({
                        iconName: 'cash',
                        displayText: this.translate.instant('Expenses'),
                        component: 'ExpensesPage'
                    });
                    this.options.push({
                        displayText: this.translate.instant('Fuel'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Entry'),
                                component: 'FuelConsumptionPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Consumption'),
                                component: 'FuelConsumptionReportPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Chart'),
                                component: 'FuelChartPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Vehicle Maintenance'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'notifications',
                                displayText: this.translate.instant('Reminders'),
                                component: 'MaintenanceReminderPage'
                            }
                        ]
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Reports'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('AC Report'),
                                component: 'AcReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Daily Report'),
                                component: 'DailyReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Daywise Report'),
                                component: 'DaywiseReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Distance Report'),
                                component: 'DistanceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Geofence Report'),
                                component: 'GeofenceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Idle Report',
                                component: 'IdleReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Ignition Report'),
                                component: 'IgnitionReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Loading Unloading Trip',
                                component: 'LoadingUnloadingTripPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Over Speed Report'),
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('POI Report'),
                                component: 'POIReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Route Violation Report'),
                                component: 'RouteVoilationsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('SOS'),
                                component: 'SosReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Speed Variation Report'),
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Stoppages Report'),
                                component: 'StoppagesRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Summary Report'),
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Trip Report'),
                                component: 'TripReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('User Trip report'),
                                component: 'YourTripsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Value Screen'),
                                component: 'DailyReportNewPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Working Hours Report'),
                                component: 'WorkingHoursReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    // this.options.push({
                    //   displayText: this.translate.instant('Routes'),
                    //   iconName: 'map',
                    //   component: 'RoutePage'
                    // });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Customer Support'),
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: this.translate.instant('Rate Us'),
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: this.translate.instant('Contact Us'),
                                component: 'ContactUsPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Profile'),
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
                else {
                    this.options.push({
                        iconName: 'home',
                        displayText: this.translate.instant('Home'),
                        component: this.pageRoot,
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: this.translate.instant('Groups'),
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'calendar',
                        displayText: this.translate.instant('My Bookings'),
                        component: 'MyBookingsPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: this.translate.instant('POI List'),
                        component: 'PoiListPage'
                    });
                    this.options.push({
                        iconName: 'cash',
                        displayText: this.translate.instant('Expenses'),
                        component: 'ExpensesPage'
                    });
                    this.options.push({
                        iconName: 'custom-fastag',
                        displayText: this.translate.instant('Fastag'),
                        component: 'FastagListPage'
                    });
                    this.options.push({
                        displayText: this.translate.instant('Fuel'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Entry'),
                                component: 'FuelConsumptionPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Consumption'),
                                component: 'FuelConsumptionReportPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Chart'),
                                component: 'FuelChartPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Vehicle Maintenance'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'notifications',
                                displayText: this.translate.instant('Reminders'),
                                component: 'MaintenanceReminderPage'
                            }
                        ]
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant(this.translate.instant('Reports')),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('AC Report'),
                                component: 'AcReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Daily Report'),
                                component: 'DailyReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Daywise Report'),
                                component: 'DaywiseReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Distance Report'),
                                component: 'DistanceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Geofence Report'),
                                component: 'GeofenceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Idle Report',
                                component: 'IdleReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Ignition Report'),
                                component: 'IgnitionReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Loading Unloading Trip',
                                component: 'LoadingUnloadingTripPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Over Speed Report'),
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('POI Report'),
                                component: 'POIReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Route Violation Report'),
                                component: 'RouteVoilationsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('SOS'),
                                component: 'SosReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Speed Variation Report'),
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Stoppages Report'),
                                component: 'StoppagesRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Summary Report'),
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Trip Report'),
                                component: 'TripReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('User Trip report'),
                                component: 'YourTripsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Value Screen'),
                                component: 'DailyReportNewPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Working Hours Report'),
                                component: 'WorkingHoursReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    // this.options.push({
                    //   displayText: this.translate.instant('Routes'),
                    //   iconName: 'map',
                    //   component: 'RoutePage'
                    // });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Customer Support'),
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: this.translate.instant('Rate Us'),
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: this.translate.instant('Contact Us'),
                                component: 'ContactUsPage'
                            },
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Profile'),
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
            }
        }
        console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"));
        var _DealerStat = localStorage.getItem("dealer_status");
        var _CustStat = localStorage.getItem("custumer_status");
        var onlyDeal = localStorage.getItem("OnlyDealer");
        if (_DealerStat != null || _CustStat != null) {
            if (_DealerStat == 'ON' && _CustStat == 'OFF') {
                this.options[2].displayText = this.translate.instant('Admin');
                this.options[2].iconName = 'person';
                this.options[2].component = 'DashboardPage';
                this.options[3].displayText = this.translate.instant('Customers');
                this.options[3].iconName = 'contacts';
                this.options[3].component = 'CustomersPage';
            }
            else {
                if (_DealerStat == 'OFF' && _CustStat == 'ON') {
                    this.options[2].displayText = this.translate.instant('Dealers');
                    this.options[2].iconName = 'person';
                    this.options[2].component = 'DashboardPage';
                }
                else {
                    if (_DealerStat == 'OFF' && _CustStat == 'OFF' && onlyDeal == null) {
                        this.options[2].displayText = this.translate.instant('Dealers');
                        this.options[2].iconName = 'person';
                        this.options[2].component = 'DealerPage';
                        this.options[3].displayText = this.translate.instant('Customers');
                        this.options[3].iconName = 'contacts';
                        this.options[3].component = 'CustomersPage';
                    }
                    else {
                        if (onlyDeal == 'true') {
                            this.options[2].displayText = this.translate.instant('Customers');
                            this.options[2].iconName = 'contacts';
                            this.options[2].component = 'CustomersPage';
                        }
                    }
                }
            }
        }
        this.events.subscribe("sidemenu:event", function (data) {
            console.log("sidemenu:event=> ", data);
            if (data) {
                _this.options[2].displayText = _this.translate.instant('Dealers');
                _this.options[2].iconName = 'person';
                _this.options[2].component = 'DashboardPage';
            }
        });
    };
    MyApp.prototype.getImgUrl = function () {
        var _this = this;
        // var url = this.apiCall.mainUrl + "users/shareProfileImage?uid=" + this.islogin._id;
        var url = "http://13.126.36.205/users/shareProfileImage?uid=" + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(function (resp) {
            // this.apiCall.stopLoading();
            console.log("server image url=> ", resp);
            debugger;
            // let imgStr = resp.imageDoc;
            if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
                // if (imgStr.length > 0) {
                var imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
                var str1 = imgUrl.split('public/');
                _this.fileUrl = "http://13.126.36.205/" + str1[1];
            }
        }, function (err) {
            // this.apiCall.stopLoading();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('headerTag'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MyApp.prototype, "headerTag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('scrollableTag'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MyApp.prototype, "scrollableTag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_6__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */])
    ], MyApp.prototype, "sideMenu", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/app/app.html"*/'<ion-menu [content]="content" [swipeEnabled]="false" (ionOpen)="chkCondition()">\n  <!-- <ion-menu [content]="content" [swipeEnabled]="false"> -->\n  <ion-header>\n    <!-- <ion-navbar>\n      <div class="headProf" #headerTag ion-fixed>\n        <img *ngIf="!fileUrl" src="assets/imgs/dummy-user-img.png" />\n        <img *ngIf="fileUrl" src="{{fileUrl}}" />\n        <div>\n          <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n          <p style="font-size: 12px">\n            <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}\n          </p>\n          <p style="font-size: 12px">\n            <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}\n          </p>\n        </div>\n      </div>\n    </ion-navbar> -->\n  </ion-header>\n  <ion-content id="outerNew">\n    <div class="headProf" #headerTag ion-fixed>\n      <img *ngIf="!fileUrl" src="assets/imgs/dummy-user-img.png" />\n      <img *ngIf="fileUrl" src="{{fileUrl}}" />\n      <div>\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n        <p style="font-size: 12px">\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}\n        </p>\n        <p style="font-size: 12px">\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}\n        </p>\n      </div>\n    </div>\n    <!-- <div #scrollableTag> -->\n    <div style="margin-top: 66%;">\n      <side-menu-content style="width:100%; height:100vh" [settings]="sideMenuSettings" [options]="options"\n        (change)="onOptionSelected($event)"></side-menu-content>\n    </div>\n\n  </ion-content>\n</ion-menu>\n<!-- <ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <div style="height:100%;">\n      <div style="height:20%;">\n        <ion-thumbnail class=\'mtitle\' item-start style="z-index:999999;margin-top: 5%; height:120px;width:100%;">\n          <img src="./assets/imgs/eliteadmin-logo.png" style="height: 100%;width: 100%;">\n        </ion-thumbnail>\n      </div>\n      <div style="height:80%;margin-top: 10%;background: #1370a7;">\n\n        <ion-list style="background: #1370a7;margin:0;" lines="full">\n          <button style="background: transparent;color: #fff;" menuClose ion-item\n            *ngFor="let p of mainmenus;let i=index; " (click)="openPage(p)">\n            <ion-icon name="{{p.icon}}" style="color:#fff;font-size: 2rem;" item-left></ion-icon>\n            {{p.title}}\n          </button>\n        </ion-list>\n\n        <ion-list style="background: #1370a7;margin:0;">\n          <ion-item text-wrap (click)="toggleGroup(i)" [ngClass]="{active: isGroupShown(i)}"\n            style="margin: 0;color: #fff;background-color: transparent;padding-right: 0px; margin: 0; ">\n            \n            <ion-row style="border-top: 1px solid;">\n              <ion-col col-2 style="line-height: 2;">\n                <ion-icon name="custom-fuel" style="color:#fff;font-size: 2rem;" item-left></ion-icon>\n              </ion-col>\n              <ion-col col-9 style="line-height: 2;font-size: 16px; padding-left: 11px;">\n                Fuel\n              </ion-col>\n              <ion-col col-1 *ngIf="sublevel" style="line-height: 2.5;">\n                <ion-icon style="color:#fff;margin-top: 0px;font-size: 2rem;" item-right\n                  [name]="isGroupShown(i) ? \'ios-arrow-down-outline\' : \'ios-arrow-forward\'"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf="isGroupShown(i)">\n              <ion-list style="background: #1370a7;width: 100%;">\n                <button style="background: transparent;color: #fff;" menuClose ion-item *ngFor="let p of submenus"\n                  (click)="openPage(p)">\n                  <ion-icon name="{{p.icon}}" style="color:#fff;font-size: 2rem;" item-left></ion-icon>\n                  {{p.title}}\n                </button>\n              </ion-list>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n\n        <ion-list style="background: #1370a7;margin:0;">\n          <button style="background: transparent;color: #fff;" menuClose ion-item *ngFor="let p of mainmenus1"\n            (click)="openPage(p)">\n            <ion-icon name="{{p.icon}}" style="color:#fff;font-size: 2rem;" item-left></ion-icon>\n\n            {{p.title}}\n          </button>\n        </ion-list>\n\n        <ion-list style="background: #1370a7;margin:0;">\n          <ion-item text-wrap (click)="toggleGroup1(i)" [ngClass]="{active: isGroupShown1(i)}"\n            style="margin: 0;color: #fff;background-color: transparent;padding-right: 0px; margin: 0; ">\n            <ion-row style="border-top: 1px solid;">\n              <ion-col col-11 style="line-height: 2;font-size: 16px;">Customer Support</ion-col>\n              <ion-col col-1 *ngIf="sublevel" style="line-height: 2.5;">\n                <ion-icon style="color:#fff;margin-top: 0px;font-size: 2rem;" item-right\n                  [name]="isGroupShown1(i) ? \'ios-arrow-down-outline\' : \'ios-arrow-forward\'"></ion-icon>\n              </ion-col>\n            </ion-row>\n            <ion-row *ngIf="isGroupShown1(i)">\n              <ion-list style="background: #1370a7;width: 100%;">\n                <button style="background: transparent;color: #fff;" menuClose ion-item *ngFor="let p of submenus1"\n                  (click)="openPage(p)">\n                  <ion-icon name="{{p.icon}}" style="color:#fff;font-size: 2rem;" item-left></ion-icon>\n\n                  {{p.title}}\n                </button>\n              </ion-list>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n      </div>\n    </div>\n  </ion-content>\n</ion-menu> -->\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_4__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_8__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_app_version__["a" /* AppVersion */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 565:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuOptionSelect; });
// SideMenuOptionSelect constant
var SideMenuOptionSelect = 'sideMenu:optionSelect';
//# sourceMappingURL=side-menu-option-select-event.js.map

/***/ }),

/***/ 572:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateGroup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateGroup = /** @class */ (function () {
    function UpdateGroup(navCtrl, navParams, apigroupupdatecall, alertCtrl, modalCtrl, formBuilder, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupupdatecall = apigroupupdatecall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        this.GroupStatus = [
            {
                name: "Active"
            },
            {
                name: "InActive"
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            grouptype: ['']
        });
    }
    UpdateGroup.prototype.ngOnInit = function () {
    };
    UpdateGroup.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateGroup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-model',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/groups/update-group/update-group.html"*/'<ion-header>\n\n        <ion-navbar>\n\n            <ion-title>Update Group</ion-title>\n\n            <ion-buttons end>\n\n                <button ion-button icon-only (click)="dismiss()">\n\n                    <ion-icon name="close-circle"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n        </ion-navbar>\n\n    </ion-header>\n\n    <ion-content>\n\n     \n\n        <form [formGroup]="groupForm">\n\n\n\n            <ion-item>\n\n                    <ion-label fixed fixed style="min-width: 50% !important;">Group Name</ion-label>\n\n                    <ion-input formControlName="group_name" type="text"></ion-input>\n\n           </ion-item>\n\n           \n\n\n\n           <ion-item>\n\n                <ion-label >Group Status*</ion-label>\n\n                <ion-select formControlName="status" style="min-width:50%;">\n\n                    <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.name" (ionSelect)="GroupStatusdata(statusname)">{{statusname.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n    \n\n      \n\n    </form>\n\n    <!-- <button ion-button block (click)="AddGroup()">ADD</button> -->\n\n        \n\n    </ion-content>\n\n    <ion-footer class="footSty">\n\n    \n\n            <ion-toolbar>\n\n                <ion-row no-padding>\n\n                    <ion-col width-50 style="text-align: center;">\n\n                        <button ion-button clear color="light" (click)="UpdateGroup()">UPDATE</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-toolbar>\n\n        </ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/ionic-tractive/src/pages/groups/update-group/update-group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], UpdateGroup);
    return UpdateGroup;
}());

//# sourceMappingURL=update-group.js.map

/***/ }),

/***/ 575:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ChatMessage */
/* unused harmony export UserInfo */
/* unused harmony export userAvatar */
/* unused harmony export toUserAvatar */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators_map__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(69);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChatMessage = /** @class */ (function () {
    function ChatMessage() {
    }
    return ChatMessage;
}());

var UserInfo = /** @class */ (function () {
    function UserInfo() {
    }
    return UserInfo;
}());

var userAvatar = 'https://github.com/HsuanXyz/ionic3-chat/blob/master/src/assets/user.jpg?raw=true';
var toUserAvatar = 'https://github.com/HsuanXyz/ionic3-chat/blob/master/src/assets/to-user.jpg?raw=true';
var ChatService = /** @class */ (function () {
    function ChatService(http, events) {
        this.http = http;
        this.events = events;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    ChatService.prototype.mockNewMsg = function (msg) {
        var _this = this;
        var mockMsg = {
            messageId: Date.now().toString(),
            userId: '210000198410281948',
            userName: 'Hancock',
            userAvatar: toUserAvatar,
            toUserId: '140000198202211138',
            time: Date.now(),
            message: msg.message,
            status: 'success'
        };
        setTimeout(function () {
            _this.events.publish('chat:received', mockMsg, Date.now());
        }, Math.random() * 1800);
    };
    ChatService.prototype.getMsgList = function () {
        var msgListUrl = 'https://raw.githubusercontent.com/HsuanXyz/ionic3-chat/master/src/assets/mock/msg-list.json';
        return this.http.get(msgListUrl)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators_map__["map"])(function (response) { return response.array.map(function (msg) { return (__assign({}, msg, { userAvatar: msg.userAvatar === './assets/user.jpg' ? userAvatar : toUserAvatar })); }); }));
    };
    ChatService.prototype.sendMsg = function (msg) {
        var _this = this;
        return new Promise(function (resolve) { return setTimeout(function () { return resolve(msg); }, Math.random() * 1000); })
            .then(function () { return _this.mockNewMsg(msg); });
    };
    ChatService.prototype.getUserInfo = function () {
        var userInfo = {
            id: this.islogin._id,
            name: 'Luff',
            avatar: userAvatar
        };
        return new Promise(function (resolve) { return resolve(userInfo); });
    };
    ChatService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], ChatService);
    return ChatService;
}());

//# sourceMappingURL=chat-service.js.map

/***/ }),

/***/ 576:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CordovaSqLiteProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(78);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CordovaSqLiteProvider = /** @class */ (function () {
    function CordovaSqLiteProvider(http, sqlite) {
        this.http = http;
        this.sqlite = sqlite;
        console.log('Hello CordovaSqLiteProvider Provider');
    }
    CordovaSqLiteProvider.prototype.creteSQLiteDB = function () {
        var _this = this;
        this.sqlite.create({
            name: 'oneqlik_vts.db',
            location: 'default'
        }).then(function (db) {
            return db.executeSql("\n      CREATE TABLE IF NOT EXISTS vehicle_list(\n        _id VARCHAR, \n        Device_Name TEXT,\n        Device_ID BIGINT,\n        supAdmin VARCHAR,\n        Dealer VARCHAR,\n        expiration_date VARCHAR, \n        status_updated_at VARCHAR, \n        fuel_percent INT, \n        currentFuel INT,\n        last_speed INT,\n        created_on VARCHAR, \n        today_odo FLOAT, \n        contact_number BIGINT, \n        iconType TEXT,\n        vehicleType TEXT,\n        status TEXT,\n        last_lat FLOAT,\n        last_lng FLOAT)", [])
                .then(function (res) {
                _this.sqldbObj = db;
                console.log('Executed SQL', res);
                // this.sqldbObj.executeSql('DROP TABLE vehicle_list;').then(res => {
                //   console.log('deleted data table: ', res)
                //   this.creteSQLiteDB();
                // }).catch(e => {
                //   this.creteSQLiteDB();
                //   // this.getVehicleList();
                //   console.log(e);
                // })
                // this.deleteSQLiteDB();
            })
                .catch(function (e) {
                // this.sqldbObj.executeSql('DROP TABLE vehicle_list;').then(res => {
                //   console.log('deleted data table: ', res)
                //   this.creteSQLiteDB();
                // }).catch(e => {
                //   this.creteSQLiteDB();
                //   // this.getVehicleList();
                //   console.log(e);
                // })
                console.log(e);
            });
        });
    };
    CordovaSqLiteProvider.prototype.deleteSQLiteDB = function () {
        this.sqldbObj.executeSql('DELETE FROM vehicle_list').then(function (res) {
            console.log('deleted data table: ', res);
            // this.getVehicleList();
        }).catch(function (e) {
            // this.creteSQLiteDB();
            // this.getVehicleList();
            console.log(e);
        });
    };
    CordovaSqLiteProvider.prototype.getVehicleDataFromSQLiteDB = function () {
        this.sqlite.create({
            name: 'oneqlik_vts.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql("\n      CREATE TABLE IF NOT EXISTS vehicle_list(\n        _id VARCHAR, \n        Device_Name TEXT,\n        Device_ID BIGINT,\n        supAdmin VARCHAR,\n        Dealer VARCHAR,\n        expiration_date VARCHAR, \n        status_updated_at VARCHAR, \n        fuel_percent INT, \n        currentFuel INT,\n        last_speed INT,\n        created_on VARCHAR, \n        today_odo FLOAT, \n        contact_number BIGINT, \n        iconType TEXT,\n        vehicleType TEXT,\n        status TEXT,\n        last_lat FLOAT,\n        last_lng FLOAT)", [])
                .then(function (res) {
                db.executeSql('SELECT * FROM vehicle_list', [])
                    .then(function (res) {
                    if (res.rows.length > 0) {
                        var temparray = [];
                        for (var i = 0; i < res.rows.length; i++) {
                            temparray.push(res.rows.item(i));
                        }
                    }
                    return temparray;
                })
                    .catch(function (e) { return console.log(e); });
            })
                .catch(function (e) {
                console.log(e);
            });
        });
    };
    CordovaSqLiteProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */]])
    ], CordovaSqLiteProvider);
    return CordovaSqLiteProvider;
}());

//# sourceMappingURL=cordova-sq-lite.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeocoderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(76);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GeocoderProvider = /** @class */ (function () {
    function GeocoderProvider(_GEOCODE) {
        this._GEOCODE = _GEOCODE;
        console.log('Hello GeocoderProvider Provider');
    }
    GeocoderProvider.prototype.reverseGeocode = function (lat, lng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._GEOCODE.reverseGeocode(lat, lng)
                .then(function (result) {
                var a = result[0].thoroughfare ? result[0].thoroughfare : null;
                var b = result[0].subThoroughfare ? result[0].subThoroughfare : null;
                var c = result[0].subLocality ? result[0].subLocality : null;
                var d = result[0].subAdministrativeArea ? result[0].subAdministrativeArea : null;
                var e = result[0].postalCode ? result[0].postalCode : null;
                var f = result[0].locality ? result[0].locality : null;
                var g = result[0].countryName ? result[0].countryName : null;
                var h = result[0].administrativeArea ? result[0].administrativeArea : null;
                var str = '';
                if (a != null && a != 'Unnamed Road')
                    str = a + ', ';
                if (b != null && b != 'Unnamed Road')
                    str = str + b + ', ';
                if (c != null && c != 'Unnamed Road')
                    str = str + c + ', ';
                if (d != null && d != 'Unnamed Road')
                    str = str + d + ', ';
                if (e != null && e != 'Unnamed Road')
                    str = str + e + ', ';
                if (f != null && f != 'Unnamed Road')
                    str = str + f + ', ';
                if (g != null && g != 'Unnamed Road')
                    str = str + g + ', ';
                if (h != null && h != 'Unnamed Road')
                    str = str + h + ', ';
                resolve(str);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider.prototype.geocoderResult = function (lat, lng) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                "position": {
                    lat: lat,
                    lng: lng
                }
            }).then(function (results) {
                var addr;
                if (results.length == 0) {
                    addr = 'N/A';
                    resolve(addr);
                }
                else {
                    addr = results[0].extra.lines[0];
                    resolve(addr);
                }
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__["a" /* NativeGeocoder */]])
    ], GeocoderProvider);
    return GeocoderProvider;
}());

//# sourceMappingURL=geocoder.js.map

/***/ })

},[431]);
//# sourceMappingURL=main.js.map